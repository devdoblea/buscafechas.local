const colors = require('tailwindcss/colors');

module.exports = {
  content: [
    './application/**/*.{html,js}',
    './application/views/**/*.{html,js}',
    './application/views/auth/auth_login.php',
    './assets/dist/js/**/*.{html,js}',
  ],
  theme: {
    screens: {
      '3xs': '320px',
      '2xs': '360px',
      xs: '420px',
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1440px',
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      red: colors.red,
      blue: colors.blue,
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
      emerald: colors.emerald,
      indigo: colors.indigo,
      yellow: colors.yellow,
    },
    fontFamily: {
      sans: ['Graphik', 'sans-serif'],
      serif: ['Merriweather', 'serif'],
      raleway: ['Raleway', 'sans-serif'],
    },
    extend: {
      spacing: {
        '128': '32rem',
        '144': '36rem',
      },
      borderRadius: {
        '4xl': '2rem',
      },
      colors: {
        'rojo-pollo': '#ea4e4e',
      }
    },
  },
  plugins: [],
}
