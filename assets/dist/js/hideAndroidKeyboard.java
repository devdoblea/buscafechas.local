/* Esto estaba en la pagina https://developer.android.com/guide/webapps/webview#java*/
//WebView myWebView = (WebView) findViewById(R.id.webview);
//WebSettings webSettings = myWebView.getSettings();
//webSettings.setJavaScriptEnabled(true);

/* esto estaba en la pagina https://stackoverflow.com/questions/1109022/how-do-you-close-hide-the-android-soft-keyboard-using-java*/ 
/**If you do not find a simple solution to do this, you could always just call java code from javascript. Tutorial and example here. Hide soft keyboard here. */
WebView webView = (WebView) findViewById(R.id.webview);
WebSettings webSettings = myWebView.getSettings();
webSettings.setJavaScriptEnabled(true);
webSettings.loadUrl("file:///../assets/js/hideAndroidKeyboard.java");
// webSettings.loadUrl("javascript:callJS()");
webSettings.setWebViewClient(new WebViewClient() {
    public void onPageFinished(WebView view, String url) {
        webView.loadUrl("javascript:callJS()");
    }
});
webSettings.addJavascriptInterface(new JavaScriptInterface(this), "Android");


public class JavaScriptInterface {
    Context mContext;

    /** Instantiate the interface and set the context */
    JavaScriptInterface(Context c) {
        mContext = c;
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    // public static void hideKeyboard(Activity activity) {
    //     InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
    //     //Find the currently focused view, so we can grab the correct window token from it.
    //     View view = activity.getCurrentFocus();
    //     //If no view currently has focus, create a new one, just so we can grab a window token from it
    //     if (view == null) {
    //         view = new View(activity);
    //     }
    //     imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    // }

    // public static void hideKeyboardFrom(Context context, View view) {
    //     InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
    //     imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    // }
}