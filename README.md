Sistema para la Compra y Venta de Productos, control de inventario y almacen. Ventas Online

#BASE DE DATOS
Primer paso para hacer funcionar el sistema es crear la base de datos, luego, se recomienda crear la tabla 
"ci_sessions" tal y como lo dice el manual de codeigniter. En el archivo sql de la base de datos incial se
puede encontrar el script para crear esta tabla. 
Posteriormente se deben ejecutar las migraciones tal y como se menciona a continuación:

	MIGRACIONES
	Es absolutamente imprescindible ejecutar las migraciones luego de crear la base de datos.
	Ellas se ejecutan primero, realizando un cambio en el archivo /application/config/migration.pgp
	en la linea:
		$config['migration_enabled'] = FALSE;
	cambiarlo por:
		$config['migration_enabled'] = TRUE;

	Luego, procedemos a escribir en la barra de direcciones url del navegador el nombre del dominio, 
	una barra "/", y por ultimo la palabra "migrate" asi como se describe a continuacion:

		http://tu_nombre_dominio.com/migrate

	De esta forma se crean o añaden los campos necesarios para continuar trabajando con la BD

Finalmente, si se desea llenar los datos de la tabla "Estados del Pais" se debe "subir" el archivo SQL 
"bdxxxx_INICIAL_yyyymmdd.sql" que se encuentra en el directorio "BACKUP" 
para que se actualicen todas las tablas necesarias ademas de la de los estados-municipios etc. 
Si no se crea la tabla "ci_sessions" el sistema dará error siempre que se intente acceder a èl.
Posterior a esto se deben ejecutar las migraciones como se dice a continuación.

NO SE DEBE OLVIDAR DE COLOCAR LA VARIABLE DE ENTORNO EN FALSE : "$config['migration_enabled'] = FALSE;"
esto evitarà que se reinicialicen las tablas de la base de datos.

#Ventas en Moneda Extranjera (USD)
El sistema esta programado para que calcule o no el costo de los productos en moneda extranjera siempre y cuando el administrador del sistema configure las opciones para ello.
Para que los productos muestren el valor en moneda local primero todos los productos deben tener el precio detal registrado en la moneda extranjera. En el caso de Venezuela, el precio al Detal del producto debe estar registrado en Dolares Norteamericanos y en la opción de Configuracion de Empresa, en el modulo Administrativo, debe seleccionar "Si" en la opción "Aplicar Tasa" ademas de escribir el monto por el cual será multiplicado el precio Detal de los productos. La "Tasa Dolar" debe estar expresada en Bolivares.
Si el usuario no registra una cantidad en la "Tasa Dolar" el sistema realizará igualmente la operación de multiplicar (si está seleccionada la opción "Aplicar Tasa" en "Si") pero, multiplicará por "1" para que el monto del precio Detal sea igual al registrado en el inventario.

#Modulo POS (Point of Sale).  
En este modulo, el usuario podrá crear una factura partiendo desde una página que se asemeja a un punto de venta de supermercado donde se pueden observar los productos con sus imagenes además de sus respectivas caracteristicas (código, precio, otros). 
Este modulo se puede iniciar desde el usuario vendedor así como tambien desde el usuario ADMINISTRADOR y usa practicamente las mismas "Vistas" para evitar confusiones.
Para agregar un producto a la factura de compra, el usuario solo tiene que hacer clic sobre la imagen del producto la cantidad de veces que sea necesario para igualar la cantidad de producto que desee llevarse el cliente. 
Todos los productos se agregan con la cantidad minima y el precio que está grabado en la base de datos; registro que se hace desde la opción "Productos" del Modulo Administrativo.

Para cambiar la cantidad de producto a agregar a la factura, el usuario debe primero escribir la cantidad con el teclado en pantalla dispuesto para ello, verificar que el botón que muestra la palabra "Qty" se encuentre de color amarillo (indicador de que está activo en esa funcion) y luego seleccionar el producto que desea agregar.

Un ejemplo de una venta de una fraccion de la cantidad minima es cuando se vende por ejemplo, algun producto de la chrcuteria. Por lo general, no se vende un kilo completo de queso, entonces para vender una porción, el usuario debe escribir la cantidad a vender anteponiendo el número "Cero" y despues escribiendo el simbolo "Punto" con lo se que escribirá un numero con decimales (algo asi como )
Para cambiar el precio del producto, el usuario debe primero hacer clic en el boton con la palabra "Price" hasta que este boton quede de color amarillo, luego procede a escribir el precio con el teclado numerico dispuesto para ello y por ultimo, seleccionar el producto a agregar haciendo clic sobre él. Si la cantidad de productos a agregar es mayor a uno primero de debe realizar el procedimiento de agregado de cantidad como se escribió enel parrafo anterior con la salvedad que se debe agregar la cantidad menos 1 (es decir si son 5 productos debe agregar 4) y para el ultimo, debe seleccionar el botón "Price", escribir el precio deseado con el teclado numerico dispuesto para ello y por ultimo seleccionar el producto a agregar haciendo clic sobre él.


# Modulo de descargo de Inventario
Se pueden crear facturas que no afecten los montos de ganancias en bolivares y en dolares pero si efectuan el respectivo descargo del inventario. Se crea esta opcion con el fin de poder realizar pagos a traves de la entrega de productos al costo a los empleados que asi lo requieran. Si se ejecuta esta acción via Point of Sale afectará o descargará la existencia del producto vendido pero el empleado no recibirá el beneficio de comprar con precio "al costo". 
Se agregó una opcion en la barra de menu lateral llamada "Descargo", Al hacer clic en esa opcion el usuario sera redirijido a una pagina donde podrá crear una factura seleccionando el tipo de venta, el precio al cual va a ser vendido cada uno de los productos que se seleeciones y del lado derecho de la pantalla podra ir seleccionando y agregando cada producto que desee adquirir con esta modalidad de "factura por descargo". Para finalizar la factura solo debe hacer clic en el boton azul "Grabar Descargo" y se guardará la factura. 
Estas facturas salen al final de reporte de "Libro de Ventas por Fecha" en donde se puede consultar tanto las facturas creadas por sesion, así como tambien las facturas creadas por descargo (saldrán al final del reporte). 



## Logica de pagos ( Estructura de los Metodos de Pago y como las usa el sistema)

En la vida real, los metodos de pago se clasifican en: 
	Pagos en Efectivo y 
	Pagos Digitales / Virtuales.

Dentro de los Pagos Digitales se encuentran los pagos via Tarjetas de Debito o Credito, Pagos via Electrónica dentro de los cuales tenemos los Pagos Móviles, Transferencias Bancarias, Pago via Blockchain y Pago via Pasarelas de Pago.

Este sistema viene configurado para ser usado con 2 metodos de pago de forma estandar: 
	Pago en Bolivares y 
	Pago en Dolares.

Estos dos metodos de Pago contienen en sí diferentes formas que incluyen los antes descritos, pero el sistema toma en cuenta los metodos básicos:
	Pago en Bolivares:
		Pagos en Efectivo y 
		Pagos Electrónicos.

	Pago en Dolares:
		Pagos en Efectivo y
		Pagos Electrónicos.

Teniendo claro que existen todas estas formas de pago, el sistema se centra en resolver los casos de uso mas generalizados que se puedan dar como por ejemplo:

	- Cuando se realiza un :

			- Pago en una sola transacción.

				- Efectivo, solo en Bolivares.
				- Efectivo, solo en Dolares.

			- Pago fraccionado en minimo 2 partes incluyendo la mezcla de Bolivares y Dolares:

				- Efectivo en Dolares y con Tarjeta de Debito en Bolivares.
				- Efectivo en Dolares y con Pago Móvil en Bolivares.
				- Efectivo en Dolares y con Transferencia Interbancaria en Bolivares.
				- Efectivo en Dolares y con Pasarelas de Pago en Dolares (Tipo Zelle, Paypal, C2P).

				- Efectivo en Dolares y con Pago Móvil en Dolares (Bancos Nacionales).
				- Efectivo en Dolares y con Transferencia Interbancaria en Dolares (Bancos Extranjeros).
				- Efectivo en Dolares y con Pasarelas de Pago en Dolares (Tipo Zelle, Paypal, C2P).

				- Efectivo en Bolivares y con Tarjeta de Debito en Bolivares.
				- Efectivo en Bolivares y con Pago Móvil en Bolivares.
				- Efectivo en Bolivares y con Transferencia Interbancaria en Bolivares.

El usuario del sistema solo necesita seleccionar el metodo de pago las veces que sea necesario para completar el pago total del monto de la factura.

Debido a que existen diferentes conbinaciones de metodos de pago, el sistema trata de acoplarse a todas lo mas perfecto posible, pero debido a lo complejo de los casos de uso, procederemos a explicar las formas mas complicadas de la forma mas sencilla posible.

Cuando se realizan los pagos convinando los metodos de pago en dolares y en bolivares, en efectivo y/o con pasarelas de pago. El sistema debe mostrar los calculos correctos para que se pueda finalizar la factura pero el usuario debe verificar visualmente; de esta forma, el usuario puede interpretar los montos escritos segun los metodos de pagos agregados, a su vez, esta acción permitirá que el sistema arroje los resultados correctos al momento de imprimir algun reporte de totales.

ANTES DE INICIAR LAS EXPLICACIONES SE HACE IMPRESCINDIBLE INDICAR QUE PARA PODER FINALIZAR LA FACTURA EL ULTIMO CAMPO "Vuelto/Resta" DEBE QUEDAR EN CERO "0". El usuario puede escribir en el campo :Entregado" una cifra mayor al total de la factura y el sistema mostrará el resultado de la respectiva resta pero no permitirá que se cierre la factura hasta que el vuelto sea el digito cero "0.00"

Expliquemos por partes.
	- Cuando se paga en Dolares en Efectivo y con Tarjeta de Debito en Bolivares:
			El usuario debe hacer click en el botón "Dolares" para agregar una fila de Metodo de Pago. En ella hay cinco columnas donde el usuario debe escribir, en la columna "Entregado" el monto en números. El sistema automaticamente calculará el monto que falta por pagar y lo mostrará en la columna "Vuelto/Resta". Éste monto será tomado como el valor que será mostrado en la columna "Debe" en la siguiente fila que se agrege cuando se vuelve a hacer click en el siguiente metodo de pago que decida el usuario para completar el pago de la factura. 
			En la columna "Vaucher" se observa la palabra "N/A" que significa "No Aplica" y esto es debido a que no hace falta escribir un numero de vaucher cuando se paga en efectivo. 
			La última columna describe el metodo de pago que se está agregando en la nueva fila. 
			Hasta que en la columna "Vuelto/Resta" no se muestre el valor cero "0" no se podrá finalizar la factura y por ende no se podrá realizar una nueva venta.
			Nótese que cuando se selecciona el Pago en Dolares y ésta es la primera fila de los Metodo de Pago, el monto que se muestra en la columna "Debe" será el monto total de la factura expresado en Dolares. Si el usuario debe fraccionar el pago en varias partes, el monto "Debe" restante será mostrado dependiendo de el siguiente metodo de pago seleccionado. Si el siguiente metodo de pago seleccionado es diferente a "Dolares", el monto expresado en la columna "Debe" será expresado en Bolivares por el sistema y será el resultado restante calculado en base a la Tasa del Dolar registrada en la configuración del sistema.  


## Devolucion de Productos Vendidos
	El sistema esta preparado para devolver productos que fueron devueltos por el cliente despues de haberlos comprado. En esta situación, el sistema simplemente devuelve la cantidad vendida mostrada en la factura en cuestion, luego procede a modificar la cantidad y el monto por el cual fue vendido el producto llevandolos a "CERO" y le cambiará el status (statusprod) de "VENDIDO" a "DEVUELTO" en la base de datos. Esto permitirá llevar un mejor control de las ganancias ya que el producto aunque aparezca en la factura ya no tiene precio de venta ni cantidad vendida, no se descuadrará el inventario porque se devuelve la cantidad de producto vendido y se reportará que se ha realizado una devolución cuando se consulte dicha factura. 


## Devolucion de Facturas Procesadas
	Esta en proceso...




## BASES DE DATOS DE OTROS CLIENTES

bdpelrey  -> Pollos a la Brasa El Rey
bdpapollo -> Pollos a la Broster PapáPollo
bdapetit  -> Restaurante Apetitos Gourmet


=====================================================================================================

#Ion Auth 2
###The future of authentication
by [Ben Edmunds](http://benedmunds.com)

Redux Auth 2 had a lot of potential.  It's lightweight, simple, and clean,
but had a ton of bugs and was missing some key features.  So we refactored
the code and added new features.

This version drops any backwards compatibility and makes things even more
awesome then you could expect.


##Support
If you use this to further your career, or put money in your pocket, and would like to support the project please consider a [moral license](https://www.morallicense.com/benedmunds/ion-auth).


##Documentation
Documentation is located at http://benedmunds.com/ion_auth/

##Installation
Just copy the files from this package to the corresponding folder in your
application folder.  For example, copy Ion_auth/config/ion_auth.php to
application/config/ion_auth.php

You can also copy the entire directory structure into your third_party/ folder.  For example, copy everything to /application/third_party/ion_auth/

###CodeIgniter Version 2 Compatibility
CodeIgniter v2 requires the class file names to be lowercase.  In order to support this follow the standard installation procedures and then either rename the following files or create symlinks

	models/Ion_auth_model.php         =>   models/ion_auth_model.php
	controllers/Auth.php              =>   controllers/auth.php

###Relational DB Setup
Then just run the appropriate SQL file (if you're using migrations you can
get the migrations from JD here:
https://github.com/iamfiscus/codeigniter-ion-auth-migration).

##Usage
In the package you will find example usage code in the controllers and views
folders.  The example code isn't the most beautiful code you'll ever see but
it'll show you how to use the library and it's nice and generic so it doesn't
require a MY_controller or anything else.

###Default Login
Username: admin@admin.com
Password: password


###Important
It is highly recommended that you use encrypted database sessions for security!


###Optimization
It is recommended that you add your identity column as a unique index.



Feel free to send me an email if you have any problems.


Thanks,
-Ben Edmunds
 ben.edmunds@gmail.com
 @benedmunds

===============================================================================================
###################
What is CodeIgniter
###################

CodeIgniter is an Application Development Framework - a toolkit - for people
who build web sites using PHP. Its goal is to enable you to develop projects
much faster than you could if you were writing code from scratch, by providing
a rich set of libraries for commonly needed tasks, as well as a simple
interface and logical structure to access these libraries. CodeIgniter lets
you creatively focus on your project by minimizing the amount of code needed
for a given task.

*******************
Release Information
*******************

This repo contains in-development code for future releases. To download the
latest stable release please visit the `CodeIgniter Downloads
<https://codeigniter.com/download>`_ page.

**************************
Changelog and New Features
**************************

You can find a list of all changes for each release in the `user
guide change log <https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/changelog.rst>`
*******************
Server Requirements
*******************

PHP version 5.6 or newer is recommended.

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

************
Installation
************

Please see the `installation section <https://codeigniter.com/user_guide/installation/index.html>`_
of the CodeIgniter User Guide.

*******
License
*******

Please see the `license
agreement <https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/license.rst>`_

*********
Resources
*********

-  `User Guide <https://codeigniter.com/docs>`_
-  `Language File Translations <https://github.com/bcit-ci/codeigniter3-translations>`_
-  `Community Forums <http://forum.codeigniter.com/>`_
-  `Community Wiki <https://github.com/bcit-ci/CodeIgniter/wiki>`_
-  `Community Slack Channel <https://codeigniterchat.slack.com>`_

Report security issues to our `Security Panel <mailto:security@codeigniter.com>`_
or via our `page on HackerOne <https://hackerone.com/codeigniter>`_, thank you'_

***************
Acknowledgement
***************
The CodeIgniter team would like to thank EllisLab, all the
contributors to the CodeIgniter project and you, the CodeIgniter user.