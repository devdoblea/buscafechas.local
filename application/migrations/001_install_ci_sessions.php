<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install_ci_sessions extends CI_Migration {

	public function up()
	{
		// Drop table 'ci_sessions' if it exists
		//$this->dbforge->drop_table('ci_sessions', TRUE);

		// Table structure for table 'ci_sessions'
		// $this->dbforge->add_field(array(
		// 	'id' => array(
		// 		'type' => 'VARCHAR',
		// 		'constraint' => '128',
		// 	),
		// 	'ip_address' => array(
		// 		'type' => 'VARCHAR',
		// 		'constraint' => '45',
		// 		'default' => '0'
		// 	),
		// 	'user_agent' => array(
		// 		'type' => 'VARCHAR',
		// 		'constraint' => '150',
		// 		'null' => TRUE,
		// 		'default' => '0'
		// 	),
		// 	'last_activity' => array(
		// 		'type' => 'MEDIUMINT',
		// 		'constraint' => '10',
		// 		'unsigned' => TRUE,
		// 		'null' => TRUE,
		// 		'default' => '0'
		// 	),
		// 	'user_data' => array(
		// 		'type' => 'TEXT',
		// 		'null' => TRUE,
		// 	),
		// 	'timestamp' => array(
		// 		'type' => 'MEDIUMINT',
		// 		'constraint' => '10',
		// 		'unsigned' => TRUE,
		// 	),
		// 	'data' => array(
		// 		'type' => 'BLOB'
		// 	)
		// ));
		// $this->dbforge->add_key('timestamp', TRUE);
		// $this->dbforge->create_table('ci_sessions');

	}

	public function down()
	{
		// $this->dbforge->drop_table('ci_sessions', TRUE);
	}
}
