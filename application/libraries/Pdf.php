<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	// Incluimos el archivo fpdf
	require_once APPPATH."/third_party/fpdf/fpdf.php";

//Extendemos la clase Pdf de la clase fpdf para que herede todas sus variables y funciones
class Pdf extends FPDF {
	// Variables para la funcion WriteTag
	protected $wLine; // Maximum width of the line
	protected $hLine; // Height of the line
	protected $Text; // Text to display
	protected $border;
	protected $align; // Justification of the text
	protected $fill;
	protected $Padding;
	protected $lPadding;
	protected $tPadding;
	protected $bPadding;
	protected $rPadding;
	protected $TagStyle; // Style for each tag
	protected $Indent;
	protected $Space; // Minimum space between words
	protected $PileStyle;
	protected $Line2Print; // Line to display
	protected $NextLineBegin; // Buffer between lines
	protected $TagName;
	protected $Delta; // Maximum width minus width
	protected $StringLength;
	protected $LineLength;
	protected $wTextLine; // Width minus paddings
	protected $nbSpace; // Number of spaces in the line
	protected $Xini; // Initial position
	protected $href; // Current URL
	protected $TagHref; // URL for a cell
	// Variables para WritHTML
	protected $B;
  protected $I;
  protected $U;
  protected $HREF;
  protected $ALIGN;
  protected $fontList;
  protected $issetfont;
  protected $issetcolor;

	public function __construct($_debug=false) {
	 	parent::__construct();
	 	$this->STRONG=0;
	 	$this->B=0;
    $this->I=0;
    $this->U=0;
    $this->LI='';
    $this->HREF='';
    $this->ALIGN='';
    $this->PRE=false;
    $this->fontlist=array('Times','Courier');
    $this->issetfont=false;
    $this->issetcolor=false;
    $this->debug=$_debug;
  }

	//funciones para cambiar los encabezados de los reportes
	function SetDatosHeader($datosH){
		$this->datosHeader = $datosH;
	}

	//funciones para cambiar los encabezados de los reportes
	function SetDatosFooter($datosF){
		$this->datosFooter = $datosF;
	}

	//funciones para cambiar los encabezados de los reportes
	function GetDatosHeader(){
		return $this->datosHeader;
	}

	//funciones para cambiar los encabezados de los reportes
	function GetDatosFooter(){
		return $this->datosFooter;
	}

	// El encabezado del PDF
	function Header() {
		if ($this->header == 0) {
			if ($this->PageNo()==1) {
				//$this->Image('assets/images/logo/logotipo.png',10,5,50);
				$this->SetFont('Timesi','',11);
				$datosHeader = $this->GetDatosHeader();
				$titu = $datosHeader['Titulo'];
				$sti1 = $datosHeader['Subtit1'];
				$sti2 = $datosHeader['Subtit2'];
				// Setear la fecha y la hora para que
		    // sea la del sistema operativo y no la del servidor
		    $timezone = "America/Caracas";
		    date_default_timezone_set($timezone);
		    $date = new DateTime("now",new DateTimeZone($timezone));
				$fhoy = $date->format('d-m-Y H:i:s');
				$this->Cell(25);// separar la siguiente celda del margen izquierdo
				$this->Cell(230,8,iconv("UTF-8","ISO-8859-1","$titu"),0,0,'L');
				$this->Ln(4);
				$this->Cell(25);// separar la siguiente celda del margen izquierdo
				$this->Cell(230,8,iconv("UTF-8","ISO-8859-1","$sti1"),0,0,'L');
				$this->Ln(4);
				$this->SetFont('Timesi','',8);
				$this->Cell(190,8,iconv("UTF-8","ISO-8859-1","FECHA: $fhoy"),0,0,'R');
				$this->Ln();
				$this->SetFont('Timesi','',11);
				$this->Cell(25);// separar la siguiente celda del margen izquierdo
				$this->Cell(150,4,iconv("UTF-8","ISO-8859-1","$sti2"),0,0,'C');
			} elseif($this->PageNo() >=1 ) {
				// el mismo encabezado de la primera pagina
				$this->Ln(7);
				$this->Cell(8,7,'NUM','TBL',0,'C','1');
				$this->Cell(22,7,'CEDULA','TB',0,'L','1');
				$this->Cell(50,7,'NOMBRES Y APELLIDOS','TB',0,'L','1');
				$this->Cell(40,7,'EMAIL','TB',0,'C','1');
				$this->Cell(25,7,'TLFNO CEL','TB',0,'L','1');
				$this->Cell(21,7,'Cliente desde','TB',0,'L','1');
				$this->Cell(22,7,'STATUS','TBR',0,'C','1');
				$this->Ln(7);
			}
	  } elseif ($this->header == 1) {
			//$this->Image('assets/images/logos/logoNew-solutec.jpg',10,5,25);

			$datosHeader = $this->GetDatosHeader();
			$titu = $datosHeader['Titulo'];
			$sti1 = $datosHeader['nrorden'];
			$sti2 = $datosHeader['nombcli'];
			$sti3 = $datosHeader['fserv'];
			$sti4 = $datosHeader['dirref'];
			$sti5 = $datosHeader['dircli'];
			$sti6 = $datosHeader['tlfnoca'];
			$sti7 = $datosHeader['tlfnoce'];
			$sti8 = $datosHeader['mailcli'];

			// Setear la fecha y la hora para que
		  // sea la del sistema operativo y no la del servidor
		  $timezone = "America/Caracas";
		  date_default_timezone_set($timezone);
		  $date = new DateTime("now",new DateTimeZone($timezone));
			$fhoy = $date->format('d-m-Y H:i:s');
			// empiezo a imprimir linea por linea
			$this->Image('assets/images/logo/logotipo.png',10,5,45);
			$this->SetFont('Arial','B',8);
			$this->Cell(198,4,iconv("UTF-8","ISO-8859-1","FECHA: $fhoy"),0,0,'R');
			$this->Ln();
			$this->SetFont('Arial','B',12);
			$this->Cell(202,6,iconv("UTF-8","ISO-8859-1","$titu "),0,0,'C');
			$this->Ln();
			$this->SetFont('Arial', 'B', 9);
			$this->Cell(152);
			$this->Cell(40,8, iconv("UTF-8","ISO-8859-1","Nro Orden:"),0,0,'C');
			$this->Ln();
			$this->SetFont('Arial', 'B', 13);
			$this->Cell(157);
			$this->Cell(40,10, iconv("UTF-8","ISO-8859-1","$sti1"),1,0,'C');
			$this->Ln(4);
			$this->SetFont('Arial', 'B', 9);
			$this->Cell(40,5, iconv("UTF-8","ISO-8859-1","Cliente: $sti2"),0,0,'L');
			$this->Ln(4);
			$this->SetFont('Arial', 'B', 9);
			$this->Cell(80,5, iconv("UTF-8","ISO-8859-1","Direccion: $sti5 - $sti4"),0,0,'L');
			$this->Ln(5);

			// titulo de las columnas
			$this->Cell(12,8,"CANT.",1,0,'C','0');
			$this->Cell(100,8,"DESCRIP. DEL SERVICIO",1,0,'L','0');
			$this->SetFont('Arial', 'B', 8);
			$this->MultiCell(24,4,"FECHA SERVICIO",1,0,'C','0');
			$this->Cell(25,8,"GARANTIA",1,0,'C','0');
			$this->Cell(36,8,"PRECIO UNITARIO",1,0,'C','0');
			$this->Ln(8);
		} elseif ($this->header == 2) {
			if ($this->PageNo()==1) {
				//$this->Image('assets/images/logos/logoNew-solutec.jpg',10,5,15);
				$this->SetFont('Arial','B',11);
				$datosHeader = $this->GetDatosHeader();
				$titu = $datosHeader['Titulo'];
				$sti1 = $datosHeader['Subtit1'];
				$sti2 = $datosHeader['Subtit2'];
				// Setear la fecha y la hora para que
		    // sea la del sistema operativo y no la del servidor
		    $timezone = "America/Caracas";
		    date_default_timezone_set($timezone);
		    $date = new DateTime("now",new DateTimeZone($timezone));
				$fhoy = $date->format('d-m-Y H:i:s');
				$this->SetFont('','B');
				$this->SetFont('Arial','B',8);
				$this->Cell(45);// separar la siguiente celda del margen izquierdo
				$this->Cell(237,8,iconv("UTF-8","ISO-8859-1","FECHA: $fhoy"),0,0,'R');
				$this->Ln(4);
				$this->SetFont('Arial','B',11);
				$this->Cell(45);// separar la siguiente celda del margen izquierdo
				$this->Cell(210,8,iconv("UTF-8","ISO-8859-1","$titu"),0,0,'C');
				$this->Ln(4);
				$this->Cell(45);// separar la siguiente celda del margen izquierdo
				$this->Cell(210,8,iconv("UTF-8","ISO-8859-1","DESDE: $sti1  HASTA: $sti2"),0,0,'C');
				$this->Ln(2);

			} elseif($this->PageNo() >=1 ) {
				// el mismo encabezado de la primera pagina
				$this->Ln(7);
				$this->SetFont('Arial','B',5);
				$this->SetFillColor(229, 229, 229);
				$this->Cell(8,7,'Nro.','TBL',0,'C','1');
				$this->Cell(13,7,'Ctrl Int.','TBL',0,'C','1');
				$this->Cell(38,7,'CED. - NOMBRE CLIENTE','TBL',0,'C','1');
				$this->MultiCell(14,3.5,'FECHA SERVICIO','TBL',0,'C','1');
				$this->Cell(40,7,'DIRECCION','TBL',0,'C','1');
				$this->Cell(26,7,'CORREO','TBL',0,'C','1');
				$this->Cell(18,7,'TELEFONOS','TBL',0,'C','1');
				$this->Cell(20,7,'OBSERVACION','TBL',0,'C','1');
				$this->SetFont('Arial', 'B', 4);
				$this->MultiCell(10,3.5,'CANT. SOLICIT.','TBL',0,'C','1');
				$this->Cell(55,7,'DESCRIPCION DEL SERVICIO','TBL',0,'C','1');
				$this->Cell(16,7,'GARANTIA','TBL',0,'C','1');
				$this->SetFont('Arial', 'B', 4);
				$this->MultiCell(12,3.5,'PRECIO UNITARIO','TBLR',0,'C','1');
				$this->Ln(7);
			}
		} elseif ($this->header == 3) {
			if ($this->PageNo()==1) {
				//$this->Image('assets/images/logos/logoNew-solutec.jpg',10,5,15);
				$this->SetFont('Arial','B',11);
				$datosHeader = $this->GetDatosHeader();
				$titu = $datosHeader['Titulo'];
				$sti1 = $datosHeader['Subtit1'];
				$sti2 = $datosHeader['Subtit2'];
				$sti3 = $datosHeader['Subtit3'];
				$tasa = number_format($datosHeader['tasa'],2,'.',',');
				$tinv = $datosHeader['totivnt'];
				$tprd = $datosHeader['totprds'];
				// Setear la fecha y la hora para que
		    // sea la del sistema operativo y no la del servidor
		    $timezone = "America/Caracas";
		    date_default_timezone_set($timezone);
		    $date = new DateTime("now",new DateTimeZone($timezone));
				$fhoy = $date->format('d-m-Y H:i:s');
				$this->Ln(-5);
				$this->SetFont('Arial','B',11);
				$this->Cell(35);// separar la siguiente celda del margen izquierdo
				$this->Cell(237,4,iconv("UTF-8","ISO-8859-1","FECHA: $fhoy"),0,0,'R');
				$this->Ln();
				$this->SetFont('Arial','B',11);
				$this->Cell(35);// separar la siguiente celda del margen izquierdo
				$this->Cell(237,4,"Cantidad Productos en Inventario: ".number_format($tprd),0,0,'R');
				$this->Ln();
				$this->SetFont('Arial','B',11);
				$this->Cell(35);// separar la siguiente celda del margen izquierdo
				$this->Cell(212,4,"Costo Inventario: ".number_format($tinv,2,'.',','),0,0,'R');
				$this->Cell(25,4,iconv("UTF-8","ISO-8859-1","($tasa Bs x $)"),0,0,'R');
				$this->Ln(-8);
				$this->SetFont('Arial','B',11);
				$this->Cell(45);// separar la siguiente celda del margen izquierdo
				$this->Cell(182,4,iconv("UTF-8","ISO-8859-1","$sti2"),0,0,'C');
				$this->Ln();
				$this->SetFont('Arial','B',11);
				$this->Cell(45);// separar la siguiente celda del margen izquierdo
				$this->Cell(182,4,iconv("UTF-8","ISO-8859-1","$sti3"),0,0,'C');
				$this->Ln(5);

			} elseif($this->PageNo() >=1 ) {
				// el mismo encabezado de la primera pagina
				$this->SetFont('Arial', 'B', 9);
				$this->Cell(40,12,'REFERENCIA','TBL',0,'C','1');
				$this->Cell(100,12,'DESCRIPCION DEL PRODUCTO','TB',0,'C','1');
				$this->Cell(25,12,'TIPO','TB',0,'C','1');
				$this->MultiCell(25,6,'Precio Proveedor','TB',0,'C','1');
				$this->Cell(25,12,'Precio Detal','TB',0,'C','1');
				$this->MultiCell(25,6,'Precio Mnda Local','TB',0,'C','1');
				$this->MultiCell(20,6,'Existencia Minima','TB',0,'C','1');
				$this->MultiCell(20,12,'Existencia','TBR',0,'C','1');
				$this->Ln(11);
			}
		} elseif ($this->header == 4) {
			if ($this->PageNo()==1) {
				//$this->Image('assets/images/logos/logoNew-solutec.jpg',10,5,15);
				$this->SetFont('Arial','B',9);
				$datosHeader = $this->GetDatosHeader();
				$titu = $datosHeader['Titulo'];
				$sti1 = $datosHeader['Subtit1'];
				$sti2 = $datosHeader['Subtit2'];
				$tasa = number_format($datosHeader['tasa'],2,'.',',');
				// Setear la fecha y la hora para que
		    // sea la del sistema operativo y no la del servidor
		    $timezone = "America/Caracas";
		    date_default_timezone_set($timezone);
		    $date = new DateTime("now",new DateTimeZone($timezone));
				$fhoy = $date->format('d-m-Y H:i:s');
				$this->SetFont('','B');
				$this->SetFont('Arial','B',8);
				$this->Cell(30);// separar la siguiente celda del margen izquierdo
				$this->Cell(164,4,iconv("UTF-8","ISO-8859-1","FECHA: $fhoy"),0,0,'R');
				$this->Ln();
				$this->SetFont('Arial','B',9);
				$this->Cell(35);// separar la siguiente celda del margen izquierdo
				$this->Cell(122,4,iconv("UTF-8","ISO-8859-1","$sti2"),0,0,'C');
				$this->Cell(37,4,iconv("UTF-8","ISO-8859-1","TASA: $tasa"),0,0,'R');
				$this->Ln(2);

			} elseif($this->PageNo() >=1 ) {
				// el mismo encabezado de la primera pagina
				$this->SetFont('Arial', 'B', 7);
				$this->Cell(40,12,'REFERENCIA','TBL',0,'C','1');
				$this->Cell(116,12,'DESCRIPCION DEL PRODUCTO','TB',0,'C','1');
				$this->MultiCell(20,6,'Existencia Minima','TB',0,'C','1');
				$this->MultiCell(20,12,'Existencia','TBR',0,'C','1');
				$this->Ln(12);
			}
		} elseif ($this->header == 6) {
			$this->Image('assets/images/logo/cropped-logotipo-1.png',10,4,25);
			// recibo las variables para el header
			$datosHeader = $this->GetDatosHeader();
			$emp = $datosHeader['Titulo'];
			$rif = $datosHeader['Subtit1'];
			$des = cambfecha($datosHeader['fdesde']);
			$has = cambfecha($datosHeader['fhasta']);
			$nmvend = $datosHeader['nvende'];

			/*ENCABEZADO*/
			// preparar primera linea del encabezado
			$this->SetFont('Arial','B',9);
			//$this->Cell(31);
			//$this->Cell(60,10,"$emp",0,0,'L');
			$this->Ln(7);
			$this->Cell(33,10,"RIF $rif",0,0,'R');
			$this->Ln(8);
			// preparar la segunda linea del encabezado
			$this->SetFont('Arial','B',11);
			$this->Cell(213,4,"REPORTE DE SERVICIOS REALIZADOS POR FECHA",0,0,'C');
			$this->Ln(4);
			$this->Cell(213,4,"DESDE: $des HASTA: $has ",0,0,'C');
			$this->Ln(8);

			if ($nmvend != 'TODOS') {
				$this->SetFont('Arial', 'B', 13);
				$this->Cell(8,10,  iconv("UTF-8","ISO-8859-1","TÉCNICO: $nmvend"),0,0,'L');
				$this->Ln(10);
			}

			/*
			 * TITULOS DE COLUMNAS
			 *
			 * $this->Cell(Ancho, Alto,texto,borde,posición,alineación,relleno);
			 */
			// el mismo encabezado de la primera pagina
			$this->SetFont('Arial','B',9);
			$this->SetFillColor(229, 229, 229);
			$this->Cell(8,10,'Nro.',1,0,'C','1');
			$this->MultiCell(18,5,'Nro SERVICIO',1,0,'C','1');
			$this->Cell(20,10,'FECHA',1,0,'C','1');
			$this->Cell(50,10,'CLIENTE',1,0,'C','1');
			$this->Cell(40,10,'STATUS DEL SERV.',1,0,'C','1');
			$this->MultiCell(25,5,'MONTO NETO FACTURA',1,0,'C','1');
			$this->Cell(41,10,iconv("UTF-8","ISO-8859-1","TÉCNICO"),1,0,'C','1');
			$this->Ln(10);//Se agrega un salto de linea
		} elseif ($this->header == 7) {
			// recibo las variables para el header
			$datosHeader = $this->GetDatosHeader();
			$emp  = $datosHeader['Titulo'];
			$rif  = $datosHeader['Subtit1'];
			$sti2 = $datosHeader['Subtit2'];
			$fdes = $datosHeader['fdesde'];
			$fhas = $datosHeader['fhasta'];
			$this->Image('assets/images/logo-text.png',10,10,40);
			$this->SetFont('Arial','B',9);
			$this->Ln(15);
			$this->Cell(30,4,"RIF: $rif",0,0,'R'); // Aqui va pero no se ve en el reporte porque la linea esta siendo usada por un foreach
			// preparar la segunda linea del encabezado
			$this->Ln(-15);
			$this->SetFont('Arial','B',9);
			$this->Cell(40);
			$this->Cell(200,4,"REPORTE LIBRO DE VENTAS POR FECHA",0,0,'C');
			$this->Ln();
			$this->SetFont('Arial','B',7);
			$this->Cell(40);
			$this->Cell(200,4,"(NO INCLUYE VENTAS EFECTUADAS EN SESIONES ABIERTAS)",0,0,'C');
			$this->Ln();
			$this->SetFont('Arial','B',11);
			$this->Cell(40);
			$this->Cell(200,4,"DESDE: $fdes  HASTA: $fhas",0,0,'C');
			$this->Ln();
			$this->Cell(40);
			$this->Cell(200,4,"$sti2",0,0,'C');
			$this->Ln(6);
			if ($this->PageNo() != 1) {
				// recibo las variables para el header
				$datosHeader = $this->GetDatosHeader();
				$emp  = $datosHeader['Titulo'];
				$rif  = $datosHeader['Subtit1'];
				$sti2 = $datosHeader['Subtit2'];
				$fdes = $datosHeader['fdesde'];
				$fhas = $datosHeader['fhasta'];
				$this->Image('assets/images/logo-text.png',10,10,40);
				$this->SetFont('Arial','B',9);
				// preparar la segunda linea del encabezado
				$this->Ln(10);
		    // $this->SetFont('Arial','B',8);
		    // $this->SetFillColor(229, 229, 229);
		    // $this->Cell(8,10,'Nro.',1,0,'C','1');
		    // $this->MultiCell(18,5,'NRO FACTURA',1,0,'C','1');
		    // $this->Cell(60,10,'CLIENTE.',1,0,'C','1');
		    // $this->MultiCell(22,5,'CANT. PRODUCTOS',1,0,'C','1');
		    // $this->Cell(30,10,'NETO FACTURA',1,0,'C','1');
		    // $this->Cell(16,10,'I.V.A.',1,0,'C','1');
		    // $this->Cell(30,10,'TOTAL FACTURA',1,0,'C','1');
		    // $this->MultiCell(20,5,'TOT. FACT.    ( $ )',1,0,'C','1');
		    // $this->MultiCell(20,5,'TASA DEL DIA',1,0,'C','1');
		    // $this->Cell(20,10,'PROFIT',1,0,'C','1');
		    // $this->Cell(30,10,'GANANCIA',1,0,'C','1');
		    // $this->Ln(10);//Se agrega un salto de linea
			}
		} elseif ($this->header == 8) {
			//$this->Image('assets/img/logos/logo_hastor.png',10,8,25);
			// recibo las variables para el header
			$datosHeader = $this->GetDatosHeader();
			$titu = $datosHeader['Titulo'];
			$rife = $datosHeader['rif'];
			$empr = $datosHeader['empresa'];
			$dire = $datosHeader['diremp'];
			$edoe = strtoupper($datosHeader['estado']);
			$tlfp = $datosHeader['tlfnop'];
			$tlfs = $datosHeader['tlfnos'];
			$tlfc = $datosHeader['tlfnoc'];
			$emae = $datosHeader['email'];
			$nsrv = $datosHeader['nrosrv'];
			$fsrv = cambfecha($datosHeader['fservc']);
			$nacc = $datosHeader['nac'];
			$apen = $datosHeader['nombcli'];
			$cedc = $datosHeader['cedcli'];
			$tlcc = $datosHeader['tlfclic'];
			$tlco = $datosHeader['tlfclio'];
			$dirc = $datosHeader['dircli'];
			$tecn = $datosHeader['tecnico'];
			$hora = $datosHeader['horapag'];

			/*ENCABEZADO*/
			// preparar el encabezado
			$this->Ln();
			$this->SetFont('Arial','B',9);
			$this->Cell(74,3,iconv("UTF-8","ISO-8859-1","$titu"),0,0,'C');
			$this->Ln(3);
			$this->SetFont('Arial','B',9);
			$this->Cell(74,3,"$rife",0,0,'C');
			$this->Ln(3);
			$this->Cell(74,3,iconv("UTF-8","ISO-8859-1","$empr"),0,0,'C');
			$this->Ln(3);
			$this->SetFont('Arial','B',6);
			$this->MultiCell(74,2.5,iconv("UTF-8","ISO-8859-1","$dire"),0,0,'C');
			$this->Ln(5);
			$this->Cell(74,3,iconv("UTF-8","ISO-8859-1","$edoe"),0,0,'C');
			$this->Ln(3);
			$this->SetFont('Arial','B',8);
			$this->Cell(74,3,"TLFNO $tlfp / $tlfs",0,0,'C');
			$this->Ln(5);
			$this->Cell(36,3,iconv("UTF-8","ISO-8859-1","RECIBO: $nsrv"),0,0,'L');
			$this->Cell(36,3,iconv("UTF-8","ISO-8859-1","FECHA: $fsrv"),0,0,'R');
			$this->Ln(4);
			$this->Cell(74,2,'','T',0,'C',0);
			$this->Ln();
			$this->Cell(74,3,iconv("UTF-8","ISO-8859-1","CLIENTE: $apen"),0,0,'L');
			$this->Ln(3);
			$this->Cell(74,3,iconv("UTF-8","ISO-8859-1","RIF/CED: $nacc - $cedc"),0,0,'L');
			$this->Ln(3);
			$this->Cell(74,3,iconv("UTF-8","ISO-8859-1","TELÉFONO: $tlcc"),0,0,'L');
			$this->Ln(3);
			$this->MultiCell(74,2.5,iconv("UTF-8","ISO-8859-1","DIRECCIÓN: $dirc"),0,0,'L');
			$this->Ln(8);
			$this->SetFont('Arial','B',8);
			$this->Cell(34,3,iconv("UTF-8","ISO-8859-1","VENDEDOR: $tecn"),0,0,'L');
			$this->Ln(4);
			$this->Cell(74,2,'','T',0,'C',0);
			$this->Ln();

			/* TITULOS DE COLUMNAS */
			// preparar primera linea del encabezado del cuerpo
			$this->SetFont('Arial', 'B', 8);// Se define el formato de fuente: Arial, negritas
			$this->Cell(7,4,'CANT.','B',0,'C',0);
			$this->Cell(47,4,'DESCRIPCION','B',0,'C',0);
			$this->Cell(20,4,'PRECIO','B',0,'C',0);
			$this->Ln(6);// Salto de linea antes de empesar a imprimir los conceptor
		} elseif ($this->header == 9) {
			//$this->Image('assets/img/logos/logo_hastor.png',10,8,25);
			// recibo las variables para el header
			$datosHeader = $this->GetDatosHeader();
			$titu = $datosHeader['Titulo'];
			$rife = $datosHeader['rif'];
			$empr = $datosHeader['empresa'];
			$dire = $datosHeader['diremp'];
			$edoe = strtoupper($datosHeader['estado']);
			$tlfp = $datosHeader['tlfnop'];
			$tlfs = $datosHeader['tlfnos'];
			$tlfc = $datosHeader['tlfnoc'];
			$emae = $datosHeader['email'];
			$nsrv = $datosHeader['nrosrv'];
			$fsrv = cambfecha($datosHeader['fservc']);
			$nacc = $datosHeader['nac'];
			$apen = $datosHeader['nombcli'];
			$cedc = $datosHeader['cedcli'];
			$tlcc = $datosHeader['tlfclic'];
			$tlco = $datosHeader['tlfclio'];
			$dirc = $datosHeader['dircli'];
			$tecn = $datosHeader['tecnico'];
			$hora = $datosHeader['horapag'];

			/*ENCABEZADO*/
			// preparar el encabezado
			$this->Ln();
			$this->SetFont('Arial','B',9);
			$this->Cell(74,3,iconv("UTF-8","ISO-8859-1","$titu"),0,0,'C');
			$this->Ln(3);
			$this->SetFont('Arial','B',9);
			$this->Cell(74,3,"$rife",0,0,'C');
			$this->Ln(3);
			$this->Cell(74,3,iconv("UTF-8","ISO-8859-1","$empr"),0,0,'C');
			$this->Ln(3);
			$this->SetFont('Arial','B',6);
			$this->MultiCell(74,2.5,iconv("UTF-8","ISO-8859-1","$dire"),0,0,'C');
			$this->Ln(5);
			$this->Cell(74,3,iconv("UTF-8","ISO-8859-1","ESTADO $edoe"),0,0,'C');
			$this->Ln(3);
			$this->SetFont('Arial','B',8);
			$this->Cell(74,3,"TELF $tlfp / $tlfs",0,0,'C');
			$this->Ln(5);
			$this->Cell(74,3,iconv("UTF-8","ISO-8859-1","RECIBO: $nsrv"),0,0,'L');
			$this->Ln(2);
			$this->Cell(72,3,iconv("UTF-8","ISO-8859-1","FECHA: $fsrv"),0,0,'R');
			$this->Ln(4);
			$this->Cell(74,3,'','T',0,'C',0);
			$this->Ln();
			$this->Cell(74,3,iconv("UTF-8","ISO-8859-1","CLIENTE: $apen"),0,0,'L');
			$this->Ln(3);
			$this->Cell(74,3,iconv("UTF-8","ISO-8859-1","RIF/CED: $nacc - $cedc"),0,0,'L');
			$this->Ln(3);
			$this->Cell(74,3,iconv("UTF-8","ISO-8859-1","TELÉFONO: $tlcc"),0,0,'L');
			$this->Ln(3);
			$this->MultiCell(74,2.5,iconv("UTF-8","ISO-8859-1","DIRECCIÓN: $dirc"),0,0,'L');
			$this->Ln(8);
			$this->SetFont('Arial','B',8);
			$this->Cell(74,3,iconv("UTF-8","ISO-8859-1","TÉCNICO: $tecn"),0,0,'L');
			$this->Ln(4);
			$this->Cell(74,2,'','T',0,'C',0);
			$this->Ln(4);

			/* TITULOS DE COLUMNAS */
			// preparar primera linea del encabezado del cuerpo
			$this->SetFont('Arial', 'B', 8);// Se define el formato de fuente: Arial, negritas
			$this->Cell(6,4,'CANT.','B',0,'C',0);
			$this->Cell(43,4,'DESCRIPCION','B',0,'C',0);
			$this->Cell(8,4,'GARNT.','B',0,'C',0);
			$this->Cell(17,4,'PRECIO','B',0,'C',0);
			$this->Ln(6);// Salto de linea antes de empesar a imprimir los conceptor
		} elseif ($this->header == 10) {
			if ($this->PageNo()==1) {
				$this->SetFont('Arial','B',11);
				$datosHeader = $this->GetDatosHeader();
				$titu = $datosHeader['Titulo'];
				$sti1 = $datosHeader['Subtit1'];
				$sti2 = $datosHeader['Subtit2'];
				$logo = $datosHeader['logo'];
				$this->Image('assets/images/'.$logo,10,5,25);
				// Setear la fecha y la hora para que
		    // sea la del sistema operativo y no la del servidor
		    $timezone = "America/Caracas";
		    date_default_timezone_set($timezone);
		    $date = new DateTime("now",new DateTimeZone($timezone));
				$fhoy = $date->format('d-m-Y H:i:s');
				$this->Cell(25);// separar la siguiente celda del margen izquierdo
				$this->Cell(230,8,iconv("UTF-8","ISO-8859-1","$titu"),0,0,'L');
				$this->Ln(4);
				$this->Cell(25);// separar la siguiente celda del margen izquierdo
				$this->Cell(230,8,iconv("UTF-8","ISO-8859-1","$sti1"),0,0,'L');
				$this->Ln(4);
				$this->SetFont('','',8);
				$this->Cell(190,8,iconv("UTF-8","ISO-8859-1","FECHA: $fhoy"),0,0,'R');
				$this->Ln();
				$this->SetFont('','',11);
				$this->Cell(25);// separar la siguiente celda del margen izquierdo
				$this->Cell(150,4,iconv("UTF-8","ISO-8859-1","$sti2"),0,0,'C');
			} elseif($this->PageNo() >=1 ) {
				// el mismo encabezado de la primera pagina
				$this->Ln(7);
				$this->Cell(8,7,'NUM','TBL',0,'C','1');
				$this->Cell(22,7,'CEDULA','TB',0,'L','1');
				$this->Cell(50,7,'NOMBRES Y APELLIDOS','TB',0,'L','1');
				$this->Cell(40,7,'EMAIL','TB',0,'C','1');
				$this->Cell(25,7,'TLFNO CEL','TB',0,'L','1');
				$this->Cell(21,7,'Cliente desde','TB',0,'L','1');
				$this->Cell(22,7,'STATUS','TBR',0,'C','1');
				$this->Ln(7);
			}
	  } elseif ($this->header == 11) {
			if ($this->PageNo()==1) {
				//$this->Image('assets/images/logo/logotipo.png',10,5,50);
				$this->SetFont('Times','I',11);
				$datosHeader = $this->GetDatosHeader();
				$titu = $datosHeader['titulo'];
				$fech = fecha_larga_sin_dia_semana(cambfecha($datosHeader['fecha']));
				$aten = $datosHeader['atencion'];
				$asun = $datosHeader['asunto'];
				$this->Ln(14);
				$this->SetFont('Times','I',12);
				$this->Cell(175,8,iconv("UTF-8","ISO-8859-1","Caracas, $fech"),0,0,'R');
				$this->Ln(14);
				$this->SetFont('Times','BIU',12);
				$this->Cell(20,4,iconv("UTF-8","ISO-8859-1","Atención:"),0,0,'L');
				$this->SetFont('Times','I',12);
				$this->Cell(130,4,iconv("UTF-8","ISO-8859-1","$aten"),0,0,'L');
				if($asun != ''){
					$this->Ln(7);
					$this->SetFont('Times','BIU',12);
					$this->Cell(20,4,iconv("UTF-8","ISO-8859-1","Asunto:"),0,0,'L');
					$this->SetFont('Times','I',12);
					$this->Cell(130,4,iconv("UTF-8","ISO-8859-1","$asun"),0,0,'L');
				}
				if($titu != ''){
					$this->Ln(14);
					$this->SetFont('Times','I',14);
					$this->Cell(190,4,iconv("UTF-8","ISO-8859-1","$titu"),0,0,'C');
				} else {
					$this->Ln(5);
				}

			} elseif($this->PageNo() >=1 ) {
				// el mismo encabezado de la primera pagina
				$this->Ln(14);
			}
	  } elseif ($this->header == 13) {
			if ($this->PageNo()==1) {
				$this->Image('assets/images/logo/logo-presup-inconet.png',8,8,55);

				$datosHeader = $this->GetDatosHeader();
				$titu = $datosHeader['Titulo'];
				$sti1 = $datosHeader['nrorden'];
				$sti2 = $datosHeader['nombcli'];
				$sti3 = $datosHeader['fserv'];
				$sti4 = $datosHeader['fvenc'];
				$sti5 = $datosHeader['dircli'];
				$sti6 = $datosHeader['tlfnoca'];
				$sti7 = $datosHeader['rifcli'];
				$sti8 = $datosHeader['mailcli'];
				$sti9 = $datosHeader['condic'];
				$st10 = $datosHeader['transp'];
				$vend = $datosHeader['vendedor'];
				// Setear la fecha y la hora para que
		    // sea la del sistema operativo y no la del servidor
		    $timezone = "America/Caracas";
		    date_default_timezone_set($timezone);
		    $date = new DateTime("now",new DateTimeZone($timezone));
				$fhoy = $date->format('d-m-Y H:i:s');
				// empiezo a imprimir linea por linea
				//$this->Ln(7);
				// $this->SetFont('Arial','B',8);
				// $this->Cell(194,8,iconv("UTF-8","ISO-8859-1","FECHA: $fhoy"),0,0,'R');
				$this->Ln(14);
				$this->SetFont('Arial','B',18);
				$this->Cell(202,6,iconv("UTF-8","ISO-8859-1","$titu "),0,0,'C');
				$this->Ln();
				$this->SetFont('Arial', 'B', 9);
				$this->Cell(142);
				$this->Cell(32,8, iconv("UTF-8","ISO-8859-1","PRESUPUESTO Nro.:"),0,0,'R');
				$this->SetFont('Arial', 'B', 12);
				$this->Cell(20,8, iconv("UTF-8","ISO-8859-1","$sti1"),0,0,'C');
				$this->Ln(5);
				$this->SetFont('Arial', 'B', 9);
				$this->Cell(142);
				$this->Cell(32,8, iconv("UTF-8","ISO-8859-1","Fecha:"),0,0,'R');
				$this->Cell(20,8, iconv("UTF-8","ISO-8859-1","$sti3"),0,0,'C');
				$this->Ln(5);
				$this->SetFont('Arial', 'B', 9);
				$this->Cell(142);
				$this->Cell(32,8, iconv("UTF-8","ISO-8859-1","Vence:"),0,0,'R');
				$this->Cell(20,8, iconv("UTF-8","ISO-8859-1","$sti4"),0,0,'C');
				$this->Ln(5);

				// titulo de las columnas
				$this->SetFont('Arial', '', 9);
				$this->Cell(30,8,iconv("UTF-8","ISO-8859-1","Razón Social:"),0,0,'L','0');
				$this->Cell(163,8,$sti2,0,0,'L','0');
				$this->Ln(5);
				$this->Cell(30,8,iconv("UTF-8","ISO-8859-1","RIF:"),0,0,'L','0');
				$this->Cell(163,8,$sti7,0,0,'L','0');
				$this->Ln(5);
				$this->Cell(30,8,iconv("UTF-8","ISO-8859-1","Teléfono:"),0,0,'L','0');
				$this->Cell(40,8,$sti6,0,0,'L','0');
				$this->Cell(45);
				$this->Cell(35,8,iconv("UTF-8","ISO-8859-1","Transporte:"),0,0,'R','0');
				$this->SetFont('Arial', 'B', 8);
				$this->Cell(50,8,iconv("UTF-8","ISO-8859-1","$st10"),0,0,'L','0');
				$this->Ln(5);
				$this->SetFont('Arial', '', 8);
				$this->Cell(30,8,iconv("UTF-8","ISO-8859-1","Dirección:"),0,0,'L','0');
				$this->Cell(40,8,$sti5,0,0,'L','0');
				$this->Cell(45);
				$this->SetFont('Arial', '', 8);
				$this->Cell(35,8,iconv("UTF-8","ISO-8859-1","Vendedor:"),0,0,'R','0');
				$this->SetFont('Arial', 'B', 8);
				$this->Cell(50,8,iconv("UTF-8","ISO-8859-1","$vend"),0,0,'L','0');
				$this->Ln(5);
				$this->SetFont('Arial', '', 8);
				$this->Cell(30,8,"Email:",0,0,'L','0');
				$this->Cell(40,8,"$sti8",0,0,'L','0');
				$this->Cell(45);
				$this->Cell(35,8,iconv("UTF-8","ISO-8859-1","Condición de Compra:"),0,0,'R','0');
				$this->SetFont('Arial', 'B', 8);
				$this->Cell(40,8,iconv("UTF-8","ISO-8859-1","$sti9"),0,0,'L','0');
				$this->Ln(12);

				// titulo de las columnas
				$this->SetFont('Arial', 'B', 8);
				$this->Cell(20,8,"CANTIDAD",1,0,'C','0');
				$this->Cell(100,8,iconv("UTF-8","ISO-8859-1","DESCRIPCIÓN"),1,0,'L','0');
				$this->Cell(16,8,"GARANTIA",1,0,'C','0');
				$this->Cell(28,8,"PRECIO SIN IVA",1,0,'C','0');
				$this->Cell(29,8,"TOTAL RENG.",1,0,'C','0');
				$this->Ln(8);
			} elseif($this->PageNo() >=1 ) {
				$datosHeader = $this->GetDatosHeader();
				$titu = $datosHeader['Titulo'];
				$sti1 = $datosHeader['nrorden'];
				// Setear la fecha y la hora para que
		    // sea la del sistema operativo y no la del servidor
		    $timezone = "America/Caracas";
		    date_default_timezone_set($timezone);
		    $date = new DateTime("now",new DateTimeZone($timezone));
				$fhoy = $date->format('d-m-Y H:i:s');
				// el mismo encabezado de la primera pagina
				$this->Ln(7);
				$this->SetFont('Arial','B',8);
				$this->Cell(194,8,iconv("UTF-8","ISO-8859-1","FECHA: $fhoy"),0,0,'R');
				$this->Ln();
				$this->SetFont('Arial','B',18);
				$this->Cell(172,6,iconv("UTF-8","ISO-8859-1","$titu "),0,0,'C');
				$this->SetFont('Arial', 'B', 9);
				$this->Cell(12,8, iconv("UTF-8","ISO-8859-1","PRESUPUESTO Nro.:"),0,0,'L');
				$this->Ln(8);
				$this->SetFont('Arial', 'B', 13);
				$this->Cell(153);
				$this->Cell(40,10, iconv("UTF-8","ISO-8859-1","$sti1"),1,0,'C');


				$this->Ln(12);
				$this->SetFont('Arial', 'B', 8);
				$this->Cell(20,8,"CANTIDAD",1,0,'C','0');
				$this->Cell(100,8,iconv("UTF-8","ISO-8859-1","DESCRIPCIÓN"),1,0,'L','0');
				$this->Cell(16,8,"GARANTIA",1,0,'C','0');
				$this->Cell(28,8,"PRECIO SIN IVA",1,0,'C','0');
				$this->Cell(29,8,"TOTAL RENG.",1,0,'C','0');
				$this->Ln(8);
			}
		} elseif ($this->header == 14) {
			// recibo las variables para el header
			$datosHeader = $this->GetDatosHeader();
			$emp  = $datosHeader['Titulo'];
			$rif  = $datosHeader['Subtit1'];
			$sti2 = $datosHeader['Subtit2'];
			$fdes = $datosHeader['fdesde'];
			$fhas = $datosHeader['fhasta'];
			$this->Image('assets/images/logo-text.png',7,6,40);
			$this->SetFont('Arial','B',9);
			$this->Ln(15);
			$this->Cell(85,4,"RIF: $rif",0,0,'L'); 
			$this->Cell(200,4,"Fecha: ".date('d-m-Y H:i:s'),0,0,'R');
			// preparar la segunda linea del encabezado
			$this->Ln(-10);
			$this->SetFont('Arial','B',12);
			$this->Cell(40);
			$this->Cell(200,4,"$sti2",0,0,'C');
			$this->Ln();
			$this->Cell(40);
			$this->Cell(200,4,"DESDE: $fdes",0,0,'C');
			$this->Ln();
			$this->Cell(40);
			$this->Cell(200,4,"HASTA: $fhas",0,0,'C');
			$this->Ln(5);
			if ($this->PageNo() != 1) {
				// recibo las variables para el header
				$datosHeader = $this->GetDatosHeader();
				$emp  = $datosHeader['Titulo'];
				$rif  = $datosHeader['Subtit1'];
				$sti2 = $datosHeader['Subtit2'];
				$fdes = $datosHeader['fdesde'];
				$fhas = $datosHeader['fhasta'];
				// $this->Image('assets/images/logo-text.png',10,10,40);
				$this->Ln(2);
				$this->SetFont('Arial','B',8);
        $this->SetFillColor(229, 229, 229);
        $this->Cell( 8,10,'Nro.',1,0,'C','1');
        $this->MultiCell(20, 5,'FECHA VENTA',1,0,'C','1');
        $this->Cell(22,10,'COD. PROD.',1,0,'C','1');
        $this->Cell(98,10,'NOMBRE PRODUCTO.',1,0,'C','1');
        $this->MultiCell(18,5,'CANT. VENDIDA',1,0,'C','1');
        $this->MultiCell(20,5,'ST.VENTA USD( $ ) *(A)',1,0,'C','1');
        $this->MultiCell(20,5,'PRECIO PROVEED',1,0,'C','1');
        $this->MultiCell(20,5,'STOT PREC. PROV. *(B)',1,0,'C','1');
        $this->MultiCell(20,5,'GANANCIA *(A - B)',1,0,'C','1');
        $this->MultiCell(20,5,'ST.VENTA VES( BsD )',1,0,'C','1');
        $this->MultiCell(18,5,'Existencia Actual',1,0,'C','1');
				// preparar la segunda linea del encabezado
				$this->Ln(10);
			}
		} 
	}

	// El pie del pdf
	function Footer(){
		if ($this->footer == 0) {
			$this->SetY(-15);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'',0,0,'C');
		} elseif ($this->footer == 1) {
			$this->SetY(-12);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		} elseif ($this->footer == 2) {
			$this->SetY(-39);
			$this->SetFont('Arial','B',7);
			$this->Cell(100,5,iconv("UTF-8","ISO-8859-1","VI. Fecha de Remisión:"),'TLR',0,'C','0');
			$this->Cell(4);
			$this->Cell(100,5,iconv("UTF-8","ISO-8859-1","VII. Fecha de Recepción:"),'TLR',0,'C','0');
			$this->Ln(5);
			$this->Cell(45,5,iconv("UTF-8","ISO-8859-1","Director(a)"),'TLR',0,'C','0');
			$this->Cell(55,5,iconv("UTF-8","ISO-8859-1",""),'TLR',0,'C','0');
			$this->Cell(4);
			$this->Cell(45,5,iconv("UTF-8","ISO-8859-1","Funcionario(a) Receptor(a)"),'TLR',0,'C','0');
			$this->Cell(55,5,iconv("UTF-8","ISO-8859-1",""),'TLR',0,'C','0');
			$this->Ln(5);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1","Apellidos y Nombres:"),'TLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'LR',0,'C','0');
			$this->Cell(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1","Apellidos y Nombres:"),'TLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'LR',0,'C','0');
			$this->Ln(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1",""),'BLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'LR',0,'C','0');
			$this->Cell(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1",""),'BLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'LR',0,'C','0');
			$this->Ln(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1","Número de C.I.:"),'TLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1","SELLO DEL PLANTEL"),'LR',0,'C','0');
			$this->Cell(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1","Número de C.I.:"),'TLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1","SELLO DE LA ZONA EDUCATIVA"),'LR',0,'C','0');
			$this->Ln(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1",""),'BLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'LR',0,'C','0');
			$this->Cell(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1",""),'BLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'LR',0,'C','0');
			$this->Ln(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1","Firma:"),'TLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'LR',0,'C','0');
			$this->Cell(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1","Firma"),'TLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'LR',0,'C','0');
			$this->Ln(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1",""),'BLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'BLR',0,'C','0');
			$this->Cell(4);
			$this->Cell(45,4,iconv("UTF-8","ISO-8859-1",""),'BLR',0,'C','0');
			$this->Cell(55,4,iconv("UTF-8","ISO-8859-1",""),'BLR',0,'C','0');
		} elseif ($this->footer == 3) {
			$this->SetY(-12);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		} elseif ($this->footer == 4) {
			$this->SetY(-10);
			$this->SetFont('Arial','I',5);
			$this->Cell(15);
			$this->Cell(260,4,iconv("UTF-8","ISO-8859-1","NOTA: ESTOS PRECIOS SON DE LOS COMERCIOS EN MAICAO A LA FECHA ACTUAL, Y ESTÁN SUJETOS A CAMBIOS SEGÚN EL COMPORTAMIENTO DE LA MONEDA."),0,0,'C');
			$this->Ln(4);
			$this->Cell(0,4,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		} elseif ($this->footer == 5) {
			$this->SetY(-60);
			$datosFooter = $this->GetDatosFooter();
			$observ = $datosFooter['observ'];
			$usuario= $datosFooter['usuario'];
			$tlfnoca= $datosFooter['tlfnoca'];
			$tlfnoof= $datosFooter['tlfnoof'];
			$tlfnoce= $datosFooter['tlfnoce'];
			////////////////////////////////////////////////////////////////////////////////////////
			// esta consulta es para saber de que tamaño tendrà que ser la linea a imprimir
			// Darle un tamaño adecuado al ancho de linea segun la descripcion del producto
			$height = $this->GetMulticellHeight(180,$observ);
			if( $height == 0 ) {
				$h = 8;
				$m = 8;
				$line = 8;
			} elseif( $height <= 1 ) {
				$h = 8;
				$m = 4;
				$line = 8;
			} elseif( $height <= 3 ) {
				$h = 12;
				$m = 4;
				$line = 12;
			}
			// Imprimir las Observacions
			$this->Ln(10);//Se agrega un salto de linea
			$this->SetFont('Arial','', 8);
			$this->MultiCell(193,4, iconv("UTF-8","ISO-8859-1","NOTA IMPORTANTE:"),'TLR',0,'J','0');
			$this->Ln();//Se agrega un salto de linea
			//$this->MultiCell(190,24, iconv("UTF-8","ISO-8859-1","  $observ"),'LRB',0,'L','0');
			$this->MultiCell(193,$m,"$observ",'LRB',0,'L','0');
			$this->Ln($line);//Se agrega un salto de linea
			$this->SetFont('Arial', '', 6);
			$this->Cell(193,4, iconv("UTF-8","ISO-8859-1","PRESUPUESTO POR: $usuario"),0,0,'L','0');

			/* linea para los totales
			$iva = 0;
			$this->Ln(8);
			$this->SetFont('Arial','B',10);
			$this->Cell(110,8,'RECIBIDO POR:','TLR',0,'L','0');
			$this->Cell(32,8,'',0,0,'R','0');
			$this->Cell(20,8,'SubTotal:',0,0,'R','0');
			$this->Cell(36,8,number_format($tot,2,',','.'),1,0,'C','0');
			$this->Ln(8);
			$this->Cell(110,8,'','LR',0,'R','0');
			$this->Cell(32,8,'',0,0,'R','0');
			$this->Cell(20,8,'IVA:',0,0,'R','0');
			$this->Cell(36,8,number_format($iva,2,',','.'),1,0,'C','0');
			$this->Ln(8);
			$this->Cell(110,8,'','BLR',0,'R','0');
			$this->Cell(32,8,'',0,0,'R','0');
			$this->Cell(20,8,'TOTAL:',0,0,'R','0');
			$this->Cell(36,8,number_format(($tot+$iva),2,',','.'),1,0,'C','0');
			$this->Ln(8);*/

			$this->Ln();
			//Pie de pagina en media pagina
			$this->SetFont('Arial','B',6);
			$this->Cell(193,4,'Original',0,0,'R','0');
			$this->Ln();

			$this->SetFont('Arial','I',6);
			$this->Cell(193,4,iconv("UTF-8","ISO-8859-1","NOTA: SIVASE INFORMAR SOBRE CUALQUIER NOVEDAD O FALLA DE LOS PRODUCTOS A LOS TELEFONOS $tlfnoca, $tlfnoof O AL $tlfnoce."),0,0,'C');
			$this->Ln();
			$this->Cell(193,4,iconv("UTF-8","ISO-8859-1","Gracias por su Compra, Visitenos a www.Inconetsystems.co.ve"),0,0,'C');
			$this->Ln();

			$this->SetFont('Arial','I',8);
			$this->Cell(0,5,'Pag.: '.$this->PageNo().'/{nb}',0,0,'C');

			/*$this->Ln(4);
			$this->Cell(0,4,'Page '.$this->PageNo().'/{nb}',0,0,'C');*/
		} elseif ($this->footer == 8) {
			$datosFooter = $this->GetDatosFooter();
			//$info = $datosFooter['info'];
			$sety = $datosFooter['sety'];
			//$this->SetY(-124);
			$this->SetY($sety);
			$this->Cell(74,2,'','T',0,'C',0);
			$this->Ln();
			// $this->SetFont('Arial','B',10);
			// $this->Cell(74,4,'INFORMACION IMPORTANTE','B',0,'C',0);
			// $this->Ln(5);
			// $this->SetFont('Arial','B',5);
			// $this->MultiCell(72,2.5,iconv("UTF-8","ISO-8859-1","$info"),0,0,'C',0);
			// $this->Ln(90);//Se agrega un salto de linea
			// $this->Cell(28,4,'CONFORME CLIENTE FIRMA:',0,0,'R','0');
			// $this->Cell(40,4,'','B',0,'R','0');
		} elseif ($this->footer == 9) {

			$this->SetY(-12);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'',0,0,'C');

			// Ya no se le va a colocar esta info en el pie de pagina
			// $datosFooter = $this->GetDatosFooter();
			// $info = $datosFooter['info'];
			// $sety = $datosFooter['sety'];
			// //$this->SetY(-124);
			// $this->SetY($sety);
			// $this->Cell(74,2,'','T',0,'C',0);
			// $this->Ln();
			// $this->SetFont('Arial','B',10);
			// $this->Cell(74,4,'INFORMACION IMPORTANTE','B',0,'C',0);
			// $this->Ln(5);
			// $this->SetFont('Arial','B',4);
			// $this->MultiCell(74,2.5,iconv("UTF-8","ISO-8859-1","$info"),0,0,'C',0);
			// $this->Ln(90);//Se agrega un salto de linea
			// $this->Cell(28,4,'CONFORME CLIENTE FIRMA:',0,0,'R','0');
			// $this->Cell(40,4,'','B',0,'R','0');
		} elseif ($this->footer == 10) {
			$datosFooter = $this->GetDatosFooter();
			$idinfo  = $datosFooter['idinfo'];
			$admin   = $datosFooter['admin'];
			$empresa = $datosFooter['empresa'];
			//$this->SetY(-80);
			// $this->Ln(5);
			// $this->Cell(190,5, iconv("UTF-8", "ISO-8859-1", "$admin"), '', 0, 'C', '0');
			// $this->Ln(9);
	    // $this->Cell(190,5, iconv("UTF-8", "ISO-8859-1", "$empresa"), '', 0, 'C', '0');
	    // $this->Ln(9);
	    // $this->Cell(190,5, iconv("UTF-8", "ISO-8859-1", '"TECNOLOGÍA A SU ALCANCE"'), '', 0, 'C', '0');
	    // $this->Ln(5);
	    $this->SetY(-10);
			$this->SetFont('Arial','B',5);
			$this->Cell(72,2.5,iconv("UTF-8","ISO-8859-1","Informe Nro: $idinfo"),0,0,'L',0);
		}
	}


	// para usar el multicell sin el salto de pagina que viene predeterminado
	/**
	* MultiCell with alignment as in Cell.
	* @param float $w
	* @param float $h
	* @param string $text
	* @param mixed $border
	* @param int $ln
	* @param string $align
	* @param boolean $fill
	*/
	function MultiAlignCell($w,$h,$text,$border=0,$ln=0,$align='L',$fill=false)	{
	    // Store reset values for (x,y) positions
	    $x = $this->GetX() + $w;
	    $y = $this->GetY();

	    // Make a call to FPDF's MultiCell
	    $this->MultiCell($w,$h,$text,$border,$align,$fill);

	    // Reset the line position to the right, like in Cell
	    if( $ln==0 ) {
	        $this->SetXY($x,$y);
	    }
	}

	// Para conocer cual es el height de una multicel dinamica
	// y con esto no se rompa el reporte cada vez que haya una
	// descripcion de articulo con mas de 4 lineas
	function GetMulticellHeight($w, $txt) {

		$height = 0;
		$strlen = strlen($txt);
		$width  = 0;
		for ($i = 0; $i <= $strlen; $i++) {
			$char = substr($txt, $i, 1);
			$width += $this->GetStringWidth($char);
			if($char == "\n") {
				$height++;
				$width = 0;
			}
			if($width >= $w) {
				$height++;
				$width = 0;
			}
		}

		return $height;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// MDOIFICADO POR ANGEL ANTUNEZ
	// ESTA ES LA FUNCION ORIGINAL PERO SE SUSTITUYE POR OTRA FUNCION CELL QUE PERMITA ALINEAR JUSTIFICADO
	function Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
	{
	    $k=$this->k;
	    if($this->y+$h>$this->PageBreakTrigger && !$this->InHeader && !$this->InFooter && $this->AcceptPageBreak())
	    {
	        $x=$this->x;
	        $ws=$this->ws;
	        if($ws>0)
	        {
	            $this->ws=0;
	            $this->_out('0 Tw');
	        }
	        $this->AddPage($this->CurOrientation);
	        $this->x=$x;
	        if($ws>0)
	        {
	            $this->ws=$ws;
	            $this->_out(sprintf('%.3F Tw',$ws*$k));
	        }
	    }
	    if($w==0)
	        $w=$this->w-$this->rMargin-$this->x;
	    $s='';
	    if($fill || $border==1)
	    {
	        if($fill)
	            $op=($border==1) ? 'B' : 'f';
	        else
	            $op='S';
	        $s=sprintf('%.2F %.2F %.2F %.2F re %s ',$this->x*$k,($this->h-$this->y)*$k,$w*$k,-$h*$k,$op);
	    }
	    if(is_string($border))
	    {
	        $x=$this->x;
	        $y=$this->y;
	        if(is_int(strpos($border,'L')))
	            $s.=sprintf('%.2F %.2F m %.2F %.2F l S ',$x*$k,($this->h-$y)*$k,$x*$k,($this->h-($y+$h))*$k);
	        if(is_int(strpos($border,'T')))
	            $s.=sprintf('%.2F %.2F m %.2F %.2F l S ',$x*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-$y)*$k);
	        if(is_int(strpos($border,'R')))
	            $s.=sprintf('%.2F %.2F m %.2F %.2F l S ',($x+$w)*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
	        if(is_int(strpos($border,'B')))
	            $s.=sprintf('%.2F %.2F m %.2F %.2F l S ',$x*$k,($this->h-($y+$h))*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
	    }
	    if($txt!='')
	    {
	        if($align=='R')
	            $dx=$w-$this->cMargin-$this->GetStringWidth($txt);
	        elseif($align=='C')
	            $dx=($w-$this->GetStringWidth($txt))/2;
	        elseif($align=='FJ')
	        {
	            //Set word spacing
	            $wmax=($w-2*$this->cMargin);
	            //echo 'wmax:'.$wmax.'<br>';
	            $subs_count = (substr_count($txt,' ') == 0) ? 1 : substr_count($txt,' ');
	            //$this->ws = ($wmax-$this->GetStringWidth($txt)) / substr_count($txt,' ');
	            $this->ws = ($wmax-$this->GetStringWidth($txt)) / $subs_count;
	            //echo 'subStr: '.substr_count($txt,' ').'<br>';
	            //echo 'ws: '.$this->ws.'<br>';
	            $this->_out(sprintf('%.3F Tw',$this->ws * $this->k));
	            $dx=$this->cMargin;
	        }
	        else
	            $dx=$this->cMargin;
	        $txt=str_replace(')','\\)',str_replace('(','\\(',str_replace('\\','\\\\',$txt)));
	        if($this->ColorFlag)
	            $s.='q '.$this->TextColor.' ';
	        $s.=sprintf('BT %.2F %.2F Td (%s) Tj ET',($this->x+$dx)*$k,($this->h-($this->y+.5*$h+.3*$this->FontSize))*$k,$txt);
	        if($this->underline)
	            $s.=' '.$this->_dounderline($this->x+$dx,$this->y+.5*$h+.3*$this->FontSize,$txt);
	        if($this->ColorFlag)
	            $s.=' Q';
	        if($link)
	        {
	            if($align=='FJ')
	                $wlink=$wmax;
	            else
	                $wlink=$this->GetStringWidth($txt);
	            $this->Link($this->x+$dx,$this->y+.5*$h-.5*$this->FontSize,$wlink,$this->FontSize,$link);
	        }
	    }
	    if($s)
	        $this->_out($s);
	    if($align=='FJ')
	    {
	        //Remove word spacing
	        $this->_out('0 Tw');
	        $this->ws=0;
	    }
	    $this->lasth=$h;
	    if($ln>0)
	    {
	        $this->y+=$h;
	        if($ln==1)
	            $this->x=$this->lMargin;
	    }
	    else
	        $this->x+=$w;
	}

	// // funcion para evitar el salto de linea que hace el MultiCELL original
	function MultiCell($w, $h, $txt, $border=0, $ln=0, $align='J', $fill=false) {
	  // Custom Tomaz Ahlin
	  if($ln == 0) {
	      $current_y = $this->GetY();
	      $current_x = $this->GetX();
	  }

	  // Output text with automatic or explicit line breaks
	  $cw = &$this->CurrentFont['cw'];
	  if($w==0)
	      $w = $this->w-$this->rMargin-$this->x;
	  $wmax = ($w-2*$this->cMargin)*1000/$this->FontSize;
	  $s = str_replace("\r",'',$txt);
	  $nb = strlen($s);
	  if($nb>0 && $s[$nb-1]=="\n")
	      $nb--;
	  $b = 0;
	  if($border)
	  {
	      if($border==1)
	      {
	          $border = 'LTRB';
	          $b = 'LRT';
	          $b2 = 'LR';
	      }
	      else
	      {
	          $b2 = '';
	          if(strpos($border,'L')!==false)
	              $b2 .= 'L';
	          if(strpos($border,'R')!==false)
	              $b2 .= 'R';
	          $b = (strpos($border,'T')!==false) ? $b2.'T' : $b2;
	      }
	  }
	  $sep = -1;
	  $i = 0;
	  $j = 0;
	  $l = 0;
	  $ns = 0;
	  $nl = 1;
	  while($i<$nb)
	  {
	      // Get next character
	      $c = $s[$i];
	      if($c=="\n")
	      {
	          // Explicit line break
	          if($this->ws>0)
	          {
	              $this->ws = 0;
	              $this->_out('0 Tw');
	          }
	          $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
	          $i++;
	          $sep = -1;
	          $j = $i;
	          $l = 0;
	          $ns = 0;
	          $nl++;
	          if($border && $nl==2)
	              $b = $b2;
	          continue;
	      }
	      if($c==' ')
	      {
	          $sep = $i;
	          $ls = $l;
	          $ns++;
	      }
	      $l += $cw[$c];
	      if($l>$wmax)
	      {
	          // Automatic line break
	          if($sep==-1)
	          {
	              if($i==$j)
	                  $i++;
	              if($this->ws>0)
	              {
	                  $this->ws = 0;
	                  $this->_out('0 Tw');
	              }
	              $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
	          }
	          else
	          {
	              if($align=='J')
	              {
	                  $this->ws = ($ns>1) ?     ($wmax-$ls)/1000*$this->FontSize/($ns-1) : 0;
	                  $this->_out(sprintf('%.3F Tw',$this->ws*$this->k));
	              }
	              $this->Cell($w,$h,substr($s,$j,$sep-$j),$b,2,$align,$fill);
	              $i = $sep+1;
	          }
	          $sep = -1;
	          $j = $i;
	          $l = 0;
	          $ns = 0;
	          $nl++;
	          if($border && $nl==2)
	              $b = $b2;
	      }
	      else
	          $i++;
	  }
	  // Last chunk
	  if($this->ws>0)
	  {
	      $this->ws = 0;
	      $this->_out('0 Tw');
	  }
	  if($border && strpos($border,'B')!==false)
	      $b .= 'B';
	  $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
	  $this->x = $this->lMargin;

	  // Custom Tomaz Ahlin
	  if($ln == 0) {
	      $this->SetXY($current_x + $w, $current_y);
	  }
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	/* FUNCION PARA CONVERTIR LOS TEXTOS INSERTADOS
	 * EN LOS INFORMES
	*/
	function hex2dec($color = "#000000"){
	    $tbl_color = array();
	    $tbl_color['R']=hexdec(substr($color, 1, 2));
	    $tbl_color['G']=hexdec(substr($color, 3, 2));
	    $tbl_color['B']=hexdec(substr($color, 5, 2));
	    return $tbl_color;
	}

	function px2mm($px) {
		//
	  return $px*25.4/72;
	}

	function txtentities($html){
	    $trans = get_html_translation_table(HTML_ENTITIES);
	    $trans = array_flip($trans);
	    return strtr($html, $trans);
	}

	/* convertir html en PDF con algunas modificaciones
	*/
	function WriteHTML($ancho,$m,$html,$align='')
	{
		//
    $html=strip_tags($html,"<b><i><u><a><img><p><br><strong><em><font><tr><blockquote><ol><ul><li><hr><h1><h2><h3><h4><h5><h6><pre><red><blue>"); //supprime

    // tous les tags sauf ceux reconnus
    // $html=str_replace("\n",' ',$html); // reemplazar salto de línea con espacio
    $html=trim(preg_replace('/\s\s+/', "\n", $html)); // reemplazar salto de línea con espacio
    $html=str_replace("  "," ",$html); // reemplazar salto de línea con espacio
    $html=str_replace("&#39;","'",$html); // reemplazar salto de línea con espacio
    $html=str_replace("<li>","<li>»",$html); // reemplazar salto de línea con espacio
    $html=str_replace('<p style="text-align:left">','<p style="L">',$html); // alineacion parrafo "right"
    $html=str_replace('<p style="text-align:right">','<p style="R">',$html); // alineacion parrafo "right"
    $html=str_replace('<p style="text-align:center">','<p style="C">',$html); // alineacion parrafo al center"
    $html=str_replace('<p style="text-align:justify">','<p style="J">',$html);
    $html=str_replace('<p style="text-align: left;">','<p style="L">',$html); // alineacion parrafo "right"
		$html=str_replace('<p style="text-align: right;">','<p style="R">',$html);
    $html=str_replace('<p style="text-align: center;">','<p style="C">',$html);
    $html=str_replace('<p style="text-align: justify;">','<p style="J">',$html);

    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE); // rompe la cadena en etiquetas
    $skip=false;

    foreach($a as $i=>$e)
    {
    	//print("<pre>i: ".print_r($i%2,true)."</pre>");
    	//print("<pre>E: ".print_r($e,true)."</pre>");
    	// modificacion
    	if (!$skip) {
    		if($this->HREF){
    			$e=str_replace("\n","",str_replace("\r","",$e));
    		}
    		// Original
	    	if($i%2==0) {
	    		// new line
	        if($this->PRE){
	        	$e=str_replace("\r","\n",$e);
	        } else {
	        	$e=str_replace("\r","",$e);
	        }
			    //Text
	    		if($this->HREF){
	    			$this->PutLink($this->HREF,$e);
	    			$skip=true;
	    		} else {
	    			 /* Con esta linea mando a alinear el parrafo haciendo uso de la funcion
    			  * Cell MODIFICADA de FPDF que permite alinear con justificado forzado FJ
    			  * Esta funcion esta en este archivo Pdf.php y en la funcion original dentro
    			  * del archivo FPDF.php lo que hice fue comentar esa funcion para que no entorpezca
    			  * el funcionamiento de la funcion Cell que está aqui... ¿¿Complicado no?*/
    			  //print("<pre>i: ".print_r($i%2,true)."</pre>");
  			 		//print("<pre>e: ".print_r($e,true)."</pre>");
    			  $firstCharacter = substr($e, 0, 1) . substr($e, 1, 1);
    			  //print("<pre>e: ".print_r($firstCharacter,true)."</pre>");
    			  if ($firstCharacter == '»'){
    			  	// Para cuando se busca imprimir una viñeta
    			  	////////////////////////////////////////////////////////////////////////////////////
							// esta consulta es para saber de que tamaño tendrà que ser la linea a imprimir
							// Darle un tamaño adecuado al ancho de linea segun la descripcion del producto
							$e = explode('»', $e);
							$height = $this->GetMulticellHeight(160,$e[1]);
							if( $height == 0 ) {
								$h = $height;
								$line = 0;
							} else if( $height <= 1 ) {
								$h = $height;
								$line = 5;
							} else if( $height <= 2 ) {
								$h = $height;
								$line = 7 + $height;
							} else if( $height <= 3 ) {
								$h = $height;
								$line = 11 + $height + $m;
							} else {
								$h = $height;
								$w = 170 / $height;
								//$line = $height * $w;
								$line = (170 / $height) + $m ;
							}
    			  	$this->Cell(1);
    			  	//$this->MultiCell(160,$m,stripslashes(self::txtentities($e[1])).$height.' - '.$h.' - '.$line,0,0,'L');
    			  	$this->MultiCell(160,$m,stripslashes(self::txtentities($e[1])),0,0,'J');
    			  	$this->Ln($line);
    			  } else if($this->ALIGN){
	  			  	if($this->ALIGN == "L") {
	  			  		// Para cuando la alineacion es a la izquierda
	  			  		$this->Write($m,stripslashes(self::txtentities($e)),'',"L");
	  			  	} else if($this->ALIGN == "R") {
	  			  		// Para cuando la alineacion es a la derecha
	  			  		$this->Write($m,stripslashes(self::txtentities($e)),'',"R");
	  			  	} else if($this->ALIGN == "C") {
	  			  		// Para cuando la alineacion es al centro
	  			  		$this->Write($m,stripslashes(self::txtentities($e)),'',"C");
	  			  	} else if($this->ALIGN == "J") {
	  			  		// Para cuando la alineacion es justificado
	  			  		$this->Write($m,stripslashes(self::txtentities($e)),'',"FJ");
	  			  	}

		    		} else {
    			 		// ESTA ALINEACON ES LA QUE QUEDA POR DEFECTO
    			 		$this->Write($m,stripslashes(self::txtentities($e)),'',"L");
    			  }
	    		}
	    	}	else {
	        //Tag
	    		if($e[0]=='/') {
	    			$this->CloseTag(strtoupper(substr($e,1)));
	    		}	else {
	          //Extract attributes
	    			$a2=explode(' ',$e);
	    			//print("<pre>e: ".print_r($e,true)."</pre>");
	    			//print("<pre>a2: ".print_r($a2,true)."</pre>");
	    			$tag=strtoupper(array_shift($a2));
	    			$attr=array();
	    			foreach ($a2 as $v) {
	    				// if (ereg('^([^=]*)=["\']?([^"\']*)["\']?$', $v, $a3)) {
	    				// 	$attr[strtoupper($a3[1])] = $a3[2];
	    				// }
	    				if (preg_match('/([^=]*)=["\']?([^"\']*)/', $v, $a3)) {
	    					$attr[strtoupper($a3[1])] = $a3[2];
	    				}
	    			}
	    			//print("<pre>tag: ".print_r($tag,true)."</pre>");
	    			//print("<pre>attr: ".print_r($attr,true)."</pre>");
    				$this->OpenTag($tag, $attr);
	    		}
	    	}
	    } else {
	    	$this->HREF='';
	    	$this->ALIGN='';
	    	$skip=false;
	    }
    }
  }

  function OpenTag($tag, $attr)
  {
    //Opening tag
  	switch($tag){
  		case 'H1':
        $this->Ln(2);
        $this->SetTextColor(0,0,0);
        $this->SetFontSize(22);
        $this->SetStyle('B',true);
        break;
      case 'H2':
        $this->Ln(2);
        $this->SetTextColor(0,0,0);
        $this->SetFontSize(18);
        $this->SetStyle('B',true);
        break;
      case 'H3':
        $this->Ln(2);
        $this->SetTextColor(0,0,0);
        $this->SetFontSize(16);
        $this->SetStyle('B',true);
        break;
      case 'H4':
        $this->Ln(2);
        $this->SetTextColor(0,0,0);
        $this->SetFontSize(14);
        $this->SetStyle('B',true);
        break;
      case 'H5':
        $this->Ln(2);
        $this->SetTextColor(0,0,0);
        $this->SetFontSize(12);
        $this->SetStyle('B',true);
        break;
      case 'H6':
        $this->Ln(2);
        $this->SetTextColor(0,0,0);
        $this->SetFontSize(10);
        $this->SetStyle('B',true);
        break;
      case 'HR':
      	if( !empty($attr['WIDTH']) )
            $Width = $attr['WIDTH'];
        else
            $Width = $this->w - $this->lMargin-$this->rMargin;
        $this->Ln(2);
        $x = $this->GetX();
        $y = $this->GetY();
        $this->SetLineWidth(0.4);
        $this->Line($x,$y,$x+$Width,$y);
        $this->SetLineWidth(0.2);
        $this->Ln(2);
  		case 'STRONG':
	  		$this->SetStyle('B',true);
	  		break;
  		case 'EM':
	  		$this->SetStyle('I',true);
	  		break;
  		case 'B':
  		case 'I':
  		case 'U':
	  		$this->SetStyle($tag,true);
	  		break;
  		case 'A':
  			$this->HREF=$attr['HREF'];
  			break;
  		case 'IMG':
	  		if(isset($attr['SRC']) && (isset($attr['WIDTH']) || isset($attr['HEIGHT']))) {
	  			if(!isset($attr['WIDTH']))
	  				$attr['WIDTH'] = 0;
	  			if(!isset($attr['HEIGHT']))
	  				$attr['HEIGHT'] = 0;
	  			$this->Image($attr['SRC'], $this->GetX(), $this->GetY(), px2mm($attr['WIDTH']), px2mm($attr['HEIGHT']));
	  		}
  			break;
  		case 'TR':
  		case 'BLOCKQUOTE':
  		case 'BR':
  			$this->Ln(5);
  			break;
  		case 'LI':
        $this->Ln(2);
        $this->SetTextColor(190,0,0);
        $this->Write(4,iconv("UTF-8", "ISO-8859-1", "     » "));
        $this->mySetTextColor(-1);
        break;
      case 'TR':
        $this->Ln(5);
        $this->PutLine();
        break;
  		case 'FONT':
	  		if (isset($attr['COLOR']) && $attr['COLOR']!='') {
	  			$coul=hex2dec($attr['COLOR']);
	  			$this->SetTextColor($coul['R'],$coul['V'],$coul['B']);
	  			$this->issetcolor=true;
	  		}
	  		if (isset($attr['FACE']) && in_array(strtolower($attr['FACE']), $this->fontlist)) {
	  			$this->SetFont(strtolower($attr['FACE']));
	  			$this->issetfont=true;
	  		}
	  		break;
	  	case 'P':
	  		if (isset($attr['STYLE']) && $attr['STYLE']!='') {
	  			$this->ALIGN=$attr['STYLE'];
	  		}
	  		$this->SetFont('Times','I');
	  		$this->Ln(2);
  		break;
  	}
  }

  function CloseTag($tag)
  {
    //Closing tag
    if ($tag=='H1' || $tag=='H2' || $tag=='H3' || $tag=='H4' || $tag=='H5' || $tag=='H6'){
    	$this->Ln(2);
      $this->SetFontSize(12);
      $this->SetStyle('B',false);
    }
    if ($tag=='PRE'){
    	$this->SetFont('Times','',12);
    	$this->SetFontSize(12);
    	$this->PRE=false;
    }
    if ($tag=='BLOCKQUOTE'){
    	$this->mySetTextColor(0,0,0);
    	$this->Ln(3);
    }
  	if($tag=='STRONG')
  		$tag='B';
  	if($tag=='EM')
  		$tag='I';
  	if($tag=='B' || $tag=='I' || $tag=='U' || $tag=='STRONG')
  		$this->SetStyle($tag,false);
  	if($tag=='P')
  		$this->ALIGN='';
  	if($tag=='A')
  		$this->HREF='';
  	if($tag=='FONT'){
  		if ($this->issetcolor==true) {
  			$this->SetTextColor(0);
  		}
  		if ($this->issetfont) {
  			$this->SetFont('Times');
  			$this->issetfont=false;
  		}
  	}
  	if($tag =='TR' || $tag == 'BLOCKQUOTE' || $tag == 'BR') {
  		$this->Ln(3);
  	}
  }

  function SetStyle($tag, $enable)
  {
    //Modify style and select corresponding font
  	$this->$tag+=($enable ? 1 : -1);
  	$style='';
  	foreach(array('B','I','U') as $s)
  	{
  		if( $this->$s > 0 ) {
  			$style.=$s;
  		}
  	}
  	$this->SetFont('',$style.'I');
  }

  function PutLink($URL, $txt)
  {
    //Put a hyperlink
  	$this->SetTextColor(0,0,255);
  	$this->SetStyle('U',true);
  	$this->Write(5,$txt,$URL);
  	$this->SetStyle('U',false);
  	$this->SetTextColor(0);
  }

  function mySetTextColor($r,$g=0,$b=0){
    static $_r=0, $_g=0, $_b=0;

    if ($r==-1)
        $this->SetTextColor($_r,$_g,$_b);
    else {
        $this->SetTextColor($r,$g,$b);
        $_r=$r;
        $_g=$g;
        $_b=$b;
    }
	}


}