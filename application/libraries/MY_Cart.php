<?php

/**
 * Extension de la Shopping Cart Class
 *
 * Esta clase es solo para extender lo bueno que tiene la clase CART
 * por ahora solo necesito que permita cargar los nombres del producto
 * con caracteres especiales como la Ñ el #, y hasta la tecla enter
 * o salto de parrafo
 * 
 */

class MY_Cart extends CI_Cart {

	public function __construct() {
		
		parent::__construct();
		
		$this->product_name_rules = '\,\(\)\/\"\.\:\- _a-z0-9\d\D';
	}
}