<?php

	/**
	* Site Name
	*
	* Returns the "site_name" item from your config file
	*
	* @access    public
	* @return    string
	*/
	function site_name()
	{
	    $CI =& get_instance();
	    return $CI->config->item('site_name');
	}

	function format_phone($country, $phone) {
	  	$function = 'format_phone_' . $country;
		  if(function_exists($function)) {
		    return $function($phone);
		  }
		return $phone;
	}

	function format_phone_us($phone) {
	  // note: making sure we have something
	  if(!isset($phone[3])) { return ''; }
	  // note: strip out everything but numbers
	  $phone = preg_replace("/[^0-9]/", "", $phone);
	  $length = strlen($phone);
		switch($length) {
			case 7:
				return preg_replace("/([0-9]{4})([0-9]{4})/", "$1-$2", $phone);
			 	break;
			case 10:
				return preg_replace("/([0-9]{4})([0-9]{3})([0-9]{2})([0-9]{2})/", "($1) $2-$3", $phone);
				break;
			case 11:
				return preg_replace("/([0-9]{0})([0-9]{4})([0-9]{3})([0-9]{4})/", "$1($2) $3-$4", $phone);
				break;
			default:
			    return $phone;
			break;
		}
	}

	// funcion que intercambia la fecha a formato mysql o humano
	function cambfecha($fecha) {
		if ($fecha == ''){
			$fech = '';
			$fnac = '';
			return false;
		} else {
			$fech = multiexplode(array("/","-"),$fecha);
			$fnac = $fech[2].'-'.$fech[1].'-'.$fech[0];

			return $fnac;
		}
	}

	// para dividir strings segun los parametros enviados
	function multiexplode ($delimiters,$data) {
		$MakeReady = str_replace($delimiters, $delimiters[0], $data);
		$Return    = explode($delimiters[0], $MakeReady);
		return  $Return;
	}

	function zerofill($valor, $longitud){
		$res = str_pad($valor, $longitud, '0', STR_PAD_LEFT);

		return $res;
	}

	// funcion que calcula la edad segun fecha dada
	function calculaEdad($fnac){
		// calcular edad
		list($Y,$m,$d) = explode("-",$fnac);
		$edad = date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y;

		return $edad;

	}

	// funcion para convertir fecha en fecha larga
	// Parámetros: string $data:  Fecha en formato dd/mm/aaaa o timestamp
	//             int    $tipus: Tipo de fecha (0-timestamp, 1-dd/mm/aaaa)
	//// Retorna:    string  Fecha en formato largo (x, dd mm de yyyy)
	function fecha_larga($data, $tipus=1){
		if ($data != '' && $tipus == 0 || $tipus == 1){
			$setmana = array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
			$mes     = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
			if ($tipus == 1){
				preg_match('/([0-9]{1,2})-([0-9]{1,2})-([0-9]{2,4})/', $data, $data);
				$data = mktime(0,0,0,$data[2],$data[1],$data[3]);
			}

			return $setmana[date('w', $data)].', '.date('d', $data).' '.$mes[date('m',$data)-1].' de '.date('Y', $data);
		} else {
			return 0;
		}
	}

	// funcion para convertir fecha en fecha larga
	// Parámetros: string $data:  Fecha en formato dd/mm/aaaa o timestamp
	//             int    $tipus: Tipo de fecha (0-timestamp, 1-dd/mm/aaaa)
	//// Retorna:    string  Fecha en formato largo (x, dd mm de yyyy)
	function fecha_larga_sin_dia_semana($data, $tipus=1){
		if ($data != '' && $tipus == 0 || $tipus == 1){
			   $mes = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
			if ($tipus == 1){
				preg_match('/([0-9]{1,2})-([0-9]{1,2})-([0-9]{2,4})/', $data, $data);
				$data = mktime(0,0,0,$data[2],$data[1],$data[3]);
			}

			return date('d', $data).' de '.$mes[date('m',$data)-1].' de '.date('Y', $data);
		} else {
			return 0;
		}
	}


	// funcion para saber cuanto tiempo pasó desde una fech y hora determinada
	/**
    * (desde[, hasta])
    *
    * Devuelve la diferencia textual entre dos fechas.
    */
	function time_ago($date) {
	   $timestamp = strtotime($date);

	   $strTime = array("segundo", "minutos", "horas", "dia", "mes", "año");
	   $length = array("60","60","24","30","12","10");

	   $currentTime = time();
	   if($currentTime >= $timestamp) {
			$diff     = time()- $timestamp;
			for($i = 0; $diff >= $length[$i] && $i < count($length)-1; $i++) {
			$diff = $diff / $length[$i];
			}

			$diff = round($diff);
			return 'Hace '.$diff . " " . $strTime[$i] . "(s) atras";
	   }
	}

	// funcion para separar
	function partir_texto($texto) {
		$split = explode('-',$texto);
		$parte = $split[0];

		return $parte;
	}

	// funcion para separar un texto y extraer solo el que nos interesa
	// tambien permite al usuario seleccionar cual es el caracter que divide el texto
	function partir_texto_param($texto,$explode,$param) {
		$split = explode($explode,$texto);
		$parte = $split[$param];

		return $parte;
	}

	// para acomodar el resultado de los telefonos
	function camelize($word) {
		$vowels = array("/", "-", "(", ")");
	   return str_replace($vowels, '', $word);
	}

	// verificar si estoy en localhost o en un hosting
	/**
	* Retrieves the best guess of the client's actual IP address.
	* Takes into account numerous HTTP proxy headers due to variations
	* in how different ISPs handle IP addresses in headers between hops.
	*/
	function get_ip_address() {
		// Check for shared internet/ISP IP
	  if (!empty($_SERVER['HTTP_CLIENT_IP']) && validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
	  	return $_SERVER['HTTP_CLIENT_IP'];
	  }
	  // Check for IPs passing through proxies
	  if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	   // Check if multiple IP addresses exist in var
	    $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
	    foreach ($iplist as $ip) {
	     	if (validate_ip($ip)) {
	      	return $ip;
	     	}
	   	}
	  }
	  if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED'])) {
	  	return $_SERVER['HTTP_X_FORWARDED'];
	  }
	  if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
	  	return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
	  }
	  if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR'])) {
	  	return $_SERVER['HTTP_FORWARDED_FOR'];
	  }
	  if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED'])) {
	  	return $_SERVER['HTTP_FORWARDED'];
	  }

	  // Return unreliable IP address since all else failed
	  return $_SERVER['REMOTE_ADDR'];
	}

	/**
	* Ensures an IP address is both a valid IP address and does not fall within
	* a private network range.
	*
	* @param string $ip
	*/
	function validate_ip($ip) {
	  if (filter_var($ip, FILTER_VALIDATE_IP,
	                      FILTER_FLAG_IPV4 |
	                      FILTER_FLAG_IPV6 |
	                      FILTER_FLAG_NO_PRIV_RANGE |
	                      FILTER_FLAG_NO_RES_RANGE) === false) {
	     	return false;
	  } else {
	  	//self::$ip = $ip;
	  	return true;
		}
	}

	/*
	* Funcion para calcular la fecha de vencimiento de una factura
	* Recibe el parametro de la fecha de la factura
	* le resta la cantidad de dias que indique el registro condcomercio que serias los dias de credito
	* y devuelve el resultado en formato fecha para ser mostrado en la vista.
	*
	*/
	function fecha_vencimiento($fechafact,$condcomercio) {
		$result = date("d-m-Y",strtotime($fechafact."+".$condcomercio." days"));
		return $result;
	}

		if (! function_exists('clean_data')) {

		function clean_data($string) {
			return trim(strip_tags(stripslashes($string)));
		}
	}

	// sacar solo el texto que exista entre etiquetas html
	function getTextBetweenTags($string, $tagname) {

		$pattern = "/<$tagname>(.*?)<\/$tagname>/";
		preg_match($pattern, $string, $matches);
		return $matches[1];
	}

	/* *************
	* funcion para quitar los puntos y las comas de los montos
	* y que se puedan grabar en la base de datos
	*/
	function convierte_num($monto) {
		$tt = str_replace('.', '',$monto);
		$num = str_replace(',', '.',$tt);

		return $num;
	}

	/* *************
	* funcion para quitar los puntos de los montos
	* y que se puedan mostrar correctamente en español
	*/
	function muestra_num($monto) {
		$num = str_replace(',', '.',$monto);

		return $num;
	}

	/* *************
	* funcion para quitar las tildes o los acentos
	* para que se pueda guardar en la base de datos
	*/
	function eliminar_tildes($cadena){

	  $cade = utf8_decode($cadena);
	  $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ","à","è","ì","ò","ù");

	  $permitidas   = array ("a","e","i","o","u","A","E","I","O","U","n","N","a","e","i","o","u");

	  $texto = str_replace($no_permitidas, $permitidas ,$cadena);

	  return $texto;
	}

	/* *************
	* funcion para convertir los montos tipo MONEDA del
	* formato americano a un formato que se pueda
	* grabar en la base de datos
	*/
	function convierte_num_gringo($monto) {
		$num = str_replace(',', '',$monto);
		return $num;
	}

/**
	 * Convierte un número en una cadena de letras, para el idioma
	 * castellano, pero puede funcionar para español de mexico, de
	 * españa, colombia, argentina, etc.
	 *
	 * Máxima cifra soportada: 18 dígitos con 2 decimales
	 * 999,999,999,999,999,999.99
	 * NOVECIENTOS NOVENTA Y NUEVE MIL NOVECIENTOS NOVENTA Y NUEVE BILLONES
	 * NOVECIENTOS NOVENTA Y NUEVE MIL NOVECIENTOS NOVENTA Y NUEVE MILLONES
	 * NOVECIENTOS NOVENTA Y NUEVE MIL NOVECIENTOS NOVENTA Y NUEVE PESOS 99/100 M.N.
	 *
	 * @author Ultiminio Ramos Galán <contacto@ultiminioramos.com>
	 * @param string $numero La cantidad numérica a convertir
	 * @param string $moneda La moneda local de tu país
	 * @param string $subfijo Una cadena adicional para el subfijo
	 *
	 * @return string La cantidad convertida a letras
	 */
	function num_to_letras($numero, $moneda = 'Pesos', $subfijo = 'cents')
	{
	    $xarray = array(
	        0 => 'Cero'
	        , 1 => 'UN', 'DOS', 'TRES', 'CUATRO', 'CINCO', 'SEIS', 'SIETE', 'OCHO', 'NUEVE'
	        , 'DIEZ', 'ONCE', 'DOCE', 'TRECE', 'CATORCE', 'QUINCE', 'DIECISEIS', 'DIECISIETE', 'DIECIOCHO', 'DIECINUEVE'
	        , 'VEINTI', 30 => 'TREINTA', 40 => 'CUARENTA', 50 => 'CINCUENTA'
	        , 60 => 'SESENTA', 70 => 'SETENTA', 80 => 'OCHENTA', 90 => 'NOVENTA'
	        , 100 => 'CIENTO', 200 => 'DOSCIENTOS', 300 => 'TRESCIENTOS', 400 => 'CUATROCIENTOS', 500 => 'QUINIENTOS'
	        , 600 => 'SEISCIENTOS', 700 => 'SETECIENTOS', 800 => 'OCHOCIENTOS', 900 => 'NOVECIENTOS'
	    );

	    $numero = trim($numero);
	    $xpos_punto = strpos($numero, '.');
	    $xaux_int = $numero;
	    $xdecimales = '00';
	    if (!($xpos_punto === false)) {
	        if ($xpos_punto == 0) {
	            $numero = '0' . $numero;
	            $xpos_punto = strpos($numero, '.');
	        }
	        $xaux_int = substr($numero, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
	        $xdecimales = substr($numero . '00', $xpos_punto + 1, 2); // obtengo los valores decimales
	    }

	    $XAUX = str_pad($xaux_int, 18, ' ', STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
	    $xcadena = '';
	    for ($xz = 0; $xz < 3; $xz++) {
	        $xaux = substr($XAUX, $xz * 6, 6);
	        $xi = 0;
	        $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
	        $xexit = true; // bandera para controlar el ciclo del While
	        while ($xexit) {
	            if ($xi == $xlimite) { // si ya llegó al límite máximo de enteros
	                break; // termina el ciclo
	            }

	            $x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
	            $xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
	            for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
	                switch ($xy) {
	                    case 1: // checa las centenas
	                        $key = (int) substr($xaux, 0, 3);
	                        if (100 > $key) { // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
	                            /* do nothing */
	                        } else {
	                            if (TRUE === array_key_exists($key, $xarray)) {  // busco si la centena es número redondo (100, 200, 300, 400, etc..)
	                                $xseek = $xarray[$key];
	                                $xsub = subfijo($xaux); // devuelve el subfijo correspondiente (Millón, Millones, Mil o nada)
	                                if (100 == $key) {
	                                    $xcadena = ' ' . $xcadena . ' CIEN ' . $xsub;
	                                } else {
	                                    $xcadena = ' ' . $xcadena . ' ' . $xseek . ' ' . $xsub;
	                                }
	                                $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
	                            } else { // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
	                                $key = (int) substr($xaux, 0, 1) * 100;
	                                $xseek = $xarray[$key]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
	                                $xcadena = ' ' . $xcadena . ' ' . $xseek;
	                            } // ENDIF ($xseek)
	                        } // ENDIF (substr($xaux, 0, 3) < 100)
	                        break;
	                    case 2: // checa las decenas (con la misma lógica que las centenas)
	                        $key = (int) substr($xaux, 1, 2);
	                        if (10 > $key) {
	                            /* do nothing */
	                        } else {
	                            if (TRUE === array_key_exists($key, $xarray)) {
	                                $xseek = $xarray[$key];
	                                $xsub = subfijo($xaux);
	                                if (20 == $key) {
	                                    $xcadena = ' ' . $xcadena . ' VEINTE ' . $xsub;
	                                } else {
	                                    $xcadena = ' ' . $xcadena . ' ' . $xseek . ' ' . $xsub;
	                                }
	                                $xy = 3;
	                            } else {
	                                $key = (int) substr($xaux, 1, 1) * 10;
	                                $xseek = $xarray[$key];
	                                if (20 == $key)
	                                    $xcadena = ' ' . $xcadena . ' ' . $xseek;
	                                else
	                                    $xcadena = ' ' . $xcadena . ' ' . $xseek . ' Y ';
	                            } // ENDIF ($xseek)
	                        } // ENDIF (substr($xaux, 1, 2) < 10)
	                        break;
	                    case 3: // checa las unidades
	                        $key = (int) substr($xaux, 2, 1);
	                        if (1 > $key) { // si la unidad es cero, ya no hace nada
	                            /* do nothing */
	                        } else {
	                            $xseek = $xarray[$key]; // obtengo directamente el valor de la unidad (del uno al nueve)
	                            $xsub = subfijo($xaux);
	                            $xcadena = ' ' . $xcadena . ' ' . $xseek . ' ' . $xsub;
	                        } // ENDIF (substr($xaux, 2, 1) < 1)
	                        break;
	                } // END SWITCH
	            } // END FOR
	            $xi = $xi + 3;
	        } // ENDDO
	        # si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
	        if ('ILLON' == substr(trim($xcadena), -5, 5)) {
	            $xcadena.= ' DE';
	        }

	        # si la cadena obtenida en MILLONES o BILLONES, entonces le agrega al final la conjuncion DE
	        if ('ILLONES' == substr(trim($xcadena), -7, 7)) {
	            $xcadena.= ' DE';
	        }

	        # depurar leyendas finales
	        if ('' != trim($xaux)) {
	            switch ($xz) {
	                case 0:
	                    if ('1' == trim(substr($XAUX, $xz * 6, 6))) {
	                        $xcadena.= 'UN BILLON ';
	                    } else {
	                        $xcadena.= ' BILLONES ';
	                    }
	                    break;
	                case 1:
	                    if ('1' == trim(substr($XAUX, $xz * 6, 6))) {
	                        $xcadena.= 'UN MILLON ';
	                    } else {
	                        $xcadena.= ' MILLONES ';
	                    }
	                    break;
	                case 2:
	                    if (1 > $numero) {
	                        $xcadena = "CERO {$moneda}S {$xdecimales}/100 {$subfijo}";
	                    }
	                    if ($numero >= 1 && $numero < 2) {
	                        $xcadena = "UN {$moneda} {$xdecimales}/100 {$subfijo}";
	                    }
	                    if ($numero >= 2) {
	                        $xcadena.= " {$moneda} con {$xdecimales}/100 {$subfijo}"; //
	                    }
	                    break;
	            } // endswitch ($xz)
	        } // ENDIF (trim($xaux) != "")

	        $xcadena = str_replace('VEINTI ', 'VEINTI', $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
	        $xcadena = str_replace('  ', ' ', $xcadena); // quito espacios dobles
	        $xcadena = str_replace('UN UN', 'UN', $xcadena); // quito la duplicidad
	        $xcadena = str_replace('  ', ' ', $xcadena); // quito espacios dobles
	        $xcadena = str_replace('BILLON DE MILLONES', 'BILLON DE', $xcadena); // corrigo la leyenda
	        $xcadena = str_replace('BILLONES DE MILLONES', 'BILLONES DE', $xcadena); // corrigo la leyenda
	        $xcadena = str_replace('DE UN', 'UN', $xcadena); // corrigo la leyenda
	    } // ENDFOR ($xz)
	    return trim($xcadena);
	}

	/**
	 * Esta función regresa un subfijo para la cifra
	 *
	 * @author Ultiminio Ramos Galán <contacto@ultiminioramos.com>
	 * @param string $cifras La cifra a medir su longitud
	 */
	function subfijo($cifras)
	{
	    $cifras = trim($cifras);
	    $strlen = strlen($cifras);
	    $_sub = '';
	    if (4 <= $strlen && 6 >= $strlen) {
	        $_sub = 'MIL';
	    }

	    return $_sub;
	}


/* **********************
	 * Funcion para conseguir cuantos dias hay
	 * entre una fecha dada y la fecha de hoy
	 * Retorna un numero entero.
	*/
function dias_entre_fechas($fvenc)
{

	$date = new DateTime($fvenc);
	$date->sub(new DateInterval('P10D'));
	$res = $date->format('d');

	return $res;
}


/* **********************************
   * Funcion para encontrar cuantos dias hay entre
   * una fecha inicial y una fecha final.
   * Puede usarse para encontrar cuanto falta para
   * llegar a una fecha de vencimieto. Es decir, a partir
   * de la fecha de hoy, ¿cuantos dias faltan para llegar
   * a la fecha final?
   * Retorna un numero entero.
   */
function dateDifference($start_date, $end_date)
{
	// // calulating the difference in timestamps
	// $diff = strtotime($start_date) - strtotime($end_date);

	// // 1 day = 24 hours
	// // 24 * 60 * 60 = 86400 seconds
	// return ceil(abs($diff / 86400));
	// NUEVA FORMA DE CALCULAR. LA ANTERIR ESTA BIEN PERO NO DEVUELVE LOS NUMEROS EN NEGATIVO
	$dStart = new DateTime($start_date);
	$dEnd  = new DateTime($end_date);
	$dDiff = $dStart->diff($dEnd);
	return $dDiff->format('%r%a'); // use for point out relation: smaller/greater
}

/* *************
  ** Calcular la edad de un estudiante
  */
// funcion para calcular edad
function calcular_edad($fecha)
{
	$fec = time() - strtotime($fecha);
	$edad = floor((($fec / 3600) / 24) / 360);
	return $edad;
}


/**
 * Funcion para setear fecha segun el formato recibido.
 * Retorna una fecha formateada
 */
function setear_fecha($date, $formato)
{
	// Arreglo de dias en ingles para pasarlo a español
	$findDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
	$replaceDays = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];
	// Arreglo de meses en ingles para pasarlo a español
	$findMonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	$replaceMonths = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre", "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];
	/* Setear la fecha y la hora para que sea la del sistema 
	 * operativo y no la del servidor. el $date recibido debe ser en formato gringo */
	$timezone = "America/Caracas";
	date_default_timezone_set($timezone);
	$date = new DateTime($date, new DateTimeZone($timezone));
	$fec = $date->format($formato);
	$fecha = str_replace($findDays, $replaceDays, $fec);
	$fecha = str_replace($findMonths, $replaceMonths, $fecha);
	return $fecha;
}

/***************************************************************************
 * Setear la fecha y la hora para que
 * $timezone debe ser el nombre del timeZone por ejemplo : 'America/Mexico_City'
 * $formato debe ser igual al formato que se debe retornar. Ej.: 'Y-m-d'  
 */
function fecha_formateada_segun_timezone($timezone, $formato)
{
	// $timezone = $this->config->item("timezone");
	setlocale(LC_TIME, 'es_ES.UTF-8');
	date_default_timezone_set($timezone);
	$date = new DateTime("now", new DateTimeZone($timezone));
	return $date->format($formato);
}
