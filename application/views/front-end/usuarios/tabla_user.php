<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid d-flex h-100 flex-column">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-heading">
						<?php echo form_open(base_url().'usuarios/validar','class="formulario"') ?>
						<div class="row">
            	<div class="col-12 form-inline">
              	<div class="col-md-6 col-sm-12 col-xs-12 input-group mb-3 mt-3 float-left">
									<input type="text" class="form-control" id="buscando" name="buscando" value="<?php echo set_value('buscando')?>" placeholder="Buscar por..." accept-charset="utf-8" placeholder="Introducir búsqueda" autofocus="true" autocomplete="off" autocapitalize="off" autocorrect="off" spellcheck="false" maxlength="45">
									<div class="input-group-append">
										<button class="btn btn-danger" type="submit"><i class="ti-search"></i></button>
									</div>
								</div>
								<div class="col-md-6 col-sm-12 col-xs-12 mt-3">
	                <!--  -->
	                <?php echo anchor('usuarios/reguser','<i class="mdi mdi-cash-register d-xs-block d-sm-block d-md-none d-lg-none"></i> <span class="d-xs-none d-sm-none d-md-block d-lg-block">Agregar Usuario</span>','id="agregar" class="btn btn-lg btn-success col-md-4 col-sm-12 col-xs-12 float-right margin-5" data-toggle="modal" data-target="#ventanaAgregar" accesskey="g"');?>
								</div>
							</div>
			  		</div>
						<?php echo form_close() ?>
					</div>

					<div class="card-body" style="min-height: 650px;">
						
						<?php
							if( ! $resultados ) {
								echo '
									<div class="card-body border-top height-box">
										<div class="alert alert-danger" role="alert">
											<h4 class="alert-heading">No Hay Resultados</h4>
											<p>Es posible que aùn no se haya registrado algun Cliente.</p>
											<hr>
											<p class="mb-0"><strong>Por favor verifica y en caso que creas que esto no es correcto, llama al Administrador/Desarrollador del Sistema</strong></p>
										</div>
									</div>
								';
							} else {
								echo '<div class="row el-element-overlay">';
										foreach ( $resultados as $r ):
											// boton para la edicion
											echo '
												<div class="col-lg-3 col-md-6">
													<div class="card">
														<div class="el-card-item">
															<div class="el-card-avatar el-overlay-1"> 
																<img src="'.base_url().'assets/images/users/user.png" alt="user" />
																<div class="el-overlay">
																	<ul class="list-style-none el-info">
																		<li class="el-item">
																			<button type="button" class="btn default btn-outline image-popup-vertical-fit el-link" id="editar" data-editar='.$r->id.' data-toggle="modal" data-target="#ventanaEditar"><i class="mdi mdi-lead-pencil"></i></button>
																		</li>
																		<li class="el-item">
																			<a class="btn danger btn-outline el-link" href="javascript:void(0);"><i class="mdi mdi-close"></i></a>
																		</li>
																	</ul>
																</div>
															</div>
															<div class="el-card-content">
																<h4 class="m-b-0">'.$r->first_name.'</h4>
																<h5 class="m-b-0"> <i class="mdi mdi-cellphone-iphone"></i> '.$r->phone.'</h5>
																<label> Pertenece a: </lable>';
																	$grupos = $this->Usuarios_model->get_users_group($r->id);
																	foreach($grupos as $grp):
								                  	echo '<span class="text-muted">*'.$grp->name.'</span>';    
								                  endforeach;
																echo '
															</div>
															<div class="col-md-12">
																<button type="button" class="btn ntn-lg btn-block btn-secundary" id="editar" data-editar='.$r->id.' data-toggle="modal" data-target="#ventanaEditar">Editar</button>
															</div>
														</div>
													</div>
												</div>
											';
										endforeach;
								echo '</div><!-- Fin del row el-element-overlay -->';/// final de la comprobacion de si hay datos o no	
							}
						?>
					
					</div> <!-- fin del card-body-->

					<?php // para mostrar los enlaces de la paginacion 
							echo '
							<div class="col-md-12"><nav class="float-right" aria-label="..."> '.$this->pagination->create_links().'</nav></div>';
							$this->session->unset_userdata('buscando');?>
					
				</div>

			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Right sidebar -->
		<!-- ============================================================== -->
		<!-- .right-sidebar -->
		<!-- ============================================================== -->
		<!-- End Right sidebar -->
		<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- footer -->
<!-- ============================================================== -->
<!-- Con este grupo de divs se abre la ventana modal que traera la info a editar del estudiante-->
<div class="modal fade in" id="ventanaEditar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content ct">
		</div>
	</div>
</div>
<!-- Con este grupo de divs se abre la ventana modal que traera la info a editar del estudiante-->
<div class="modal fade in" id="ventanaAgregar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content ct">
		</div>
	</div>
</div>
<script>
	// cuando haga clic en el boton argegar
	$('#agregar').click(function() {
		var seguro = '<?php echo $this->security->get_csrf_hash();?>';
		var modal = $("#ventanaAgregar").on("show.bs.modal");
		var dataString = { token:seguro };
		$.ajax({
				url: "<?php echo base_url()?>usuarios/reguser",
				type: "POST",
				data: dataString,
				cache: false,
				success: function (data) {
					//console.log(data);
					modal.find('.ct').html(data);
					$('#ccedula').focus();
				},
				error: function(err) {
					toastr.options.timeOut = 4000; // 3s
          toastr.error('agregar: '+JSON.stringify(err['statusText']));
				}
		});
	});
</script>
<script>
	// cuando haga clic en el boton argegar
	$('#ventanaEditar').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		var id = button.data('editar'); // Extract info from data-* attributes
		var seguro = '<?php echo $this->security->get_csrf_hash();?>';
		var modal = $(this);
		var dataString = { id:id, token:seguro };
		$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>usuarios/editarusuario",
				data: dataString,
				cache: false,
				success: function (data) {
						//console.log(data);
						modal.find('.ct').html(data);
				},
				error: function(err) {
					toastr.options.timeOut = 4000; // 3s
          toastr.error('ventanaEditar: '+JSON.stringify(err['statusText']));
				}
		});
	});
</script>
<script>
	 var form = document.createElement("form"); // crear un form
		with(form) {
		setAttribute("name", "myform"); //nombre del form
		setAttribute("action", ""); // action por defecto
		setAttribute("method", "post"); // method POST }
		}
		var input = document.createElement("input"); // Crea un elemento input
		with(input) {
		setAttribute("name", "idimpo"); //nombre del input que va a pasar el valor a la otra pagina
		setAttribute("type", "hidden"); // tipo hidden
		setAttribute("value", ""); // valor por defecto
		}

		form.appendChild(input); // añade el input al formulario
		document.getElementsByTagName("body")[0].appendChild(form); // añade el formulario al documento

		window.onload=function(){
			var my_links = document.getElementsByTagName("a");
			for (var a = 0; a < my_links.length; a++) {
				if (my_links[a].name=="perfil") my_links[a].onclick = function() {
					document.myform.action=this.href;
					document.myform.idimpo.value=this.rel;
					document.myform.submit();
					return false;
				}
			}
		}
</script>
