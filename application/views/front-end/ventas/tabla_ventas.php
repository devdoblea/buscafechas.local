<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid d-flex h-100 flex-column">
  <!-- ============================================================== -->
  <!-- Start Page Content -->
  <!-- ============================================================== -->
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-heading">
          <?php echo form_open(base_url() . 'ventas/validar', 'class="formulario"') ?>
          <div class="row">
            <label class="pt-2">Fecha de Sesión:&nbsp;&nbsp;</label>
            <div class="col-12 form-inline">
              <div class="col-md-6 col-sm-12 col-xs-12 input-group mb-3 mt-3 float-left">
                <input type="text" name="fec_sess" id="fec_sess" class="form-control col-md-3 outcome text-center datepicker" autocomplete="off" placeholder="Seleccione Fecha" />
                <div class="input-group-append">
                  <button class="btn btn-danger" type="submit"><i class="ti-search"></i></button>
                </div>
              </div>
            </div>
            <!-- <div class="col-12 form-inline">
              <div class="col-md-6 col-sm-12 col-xs-12 input-group mb-3 mt-3 float-left">
                <input type="text" class="form-control col-md-6" id="buscando" name="buscando" value="<?php //echo set_value('buscando') ?>" placeholder="Buscar por..." accept-charset="utf-8" placeholder="Introducir búsqueda" autocomplete="off" autocapitalize="off" autocorrect="off" spellcheck="false" maxlength="45">
                <div class="input-group-append">
                  <button class="btn btn-danger" type="submit"><i class="ti-search"></i></button>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 col-xs-12 mt-3">
                <a href="/ventas/pos" class="btn btn-lg btn-success col-md-4 col-sm-12 col-xs-12 float-right margin-5" id="agregar"> <i class="mdi mdi-cash-register d-xs-block d-sm-block d-md-none d-lg-none"></i> <span class="d-xs-none d-sm-none d-md-block d-lg-block">Agregar Venta</span> </a>
              </div>
            </div> -->
          </div>
          <?php echo form_close() ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <?php
    if (!$resultados || $resultados == 'nada') :
      echo '
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body border-top" style="min-height: 550px;">
                  <div class="alert alert-danger" role="alert">
                  <h4 class="alert-heading">No Hay Resultados</h4>
                  <p>Es posible que aùn no se haya registrado algun Servicio.</p>
                  <hr>
                  <p class="mb-0"><strong>Por favor verifica y en caso que creas que esto no es correcto, llama al Administrador/Desarrollador del Sistema</strong></p>
                </div>
             </div>
            </div>
          </div>
        ';
    elseif ($resultados) : ?>
      <!-- AREA DE SESIONES  -->
      <!-- AREA DE SESIONES ABIERTAS  -->
      <div class="col-12">
        <div class="card">
          <!-- ENCABEZADO  -->
          <div class="card-body">
            <h3 class="card-title m-b-0">Sesiones Abiertas</h3>
            <hr>
          </div>
          <section class="card-container">
            <?php
            foreach ($resultados as $r) :
              $st = $r->status;
              $idusr = 1;
              $idu = $r->iduser; ?>
              <?php if ($st == 'ABIERTA') : ?>
                <article class="card card__article">
                  <header class="card__title">
                    <h3><span class="m-b-15 d-block"><?php echo fecha_larga(date('d-m-Y', strtotime($r->tstampini))); ?></span></h3>
                  </header>
                  <div class="card__infodata" style="width:22rem;">
                    <div class="d-none d-xs-none d-sm-none d-md-block d-lg-block">
                      <!-- <figure class="card__thumbnail"> -->
                      <img class="img_sess_open" src="<?php echo base_url(); ?>assets/images/icono_presup400x400.png">
                      <!-- </figure> -->
                    </div>
                    <div class="card__datasess col-md-6 col-sm-12 col-xs-12">
                      <?php $dlsCaja = $this->Ventas_model->get_dolares_en_caja($r->idses, date('Y-m-d', strtotime($r->tstampini)), '=', 'Dolares (USD)', 'ABIERTA'); ?>
                      <?php $blvCaja = $this->Ventas_model->get_dolares_en_caja($r->idses, date('Y-m-d', strtotime($r->tstampini)), '!=', 'Dolares (USD)', 'ABIERTA'); ?>
                      <h4 class="font-medium">Dolares en Caja:&nbsp;<?php echo number_format($dlsCaja->entregado, 2, '.', ','); ?></h4>
                      <h4 class="font-medium">Dinero en PDV:&nbsp;&nbsp;<?php echo number_format($blvCaja->entregado, 2, '.', ','); ?></h4>
                      <h5 class="font-medium">
                        Status:&nbsp;ABIERTA&nbsp;-&nbsp;
                        Sesion #:&nbsp;<?php echo $r->nrosesion; ?>
                      </h5>
                      <h5 class="font-medium">Operador: <?php echo $r->first_name; ?></h5>
                    </div>
                  </div>
                  <!-- BOTONES -->
                  <div class="row">
                    <div class="col-12">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <?php if ($idu == $idusr) : ?>
                          <?php $tstampini = date('Y-m-d', strtotime($r->tstampini));
                          $diadeHoy  = date('Y-m-d', strtotime(date('Y-m-d'))); ?>
                          <?php if (strtotime($tstampini) <= strtotime($diadeHoy)) : ?>
                            <a href="/ventas/pos" class="btn btn-success button_card">REANUDAR</a>
                          <?php else : ?>
                            <a href="#" class="btn btn-success button_card disabled" style="pointer-events:none; ">REANUDAR</a>
                          <?php endif; ?>
                          <a href="#" class="btn btn-danger button_card cerrar" id="cerrar" name="cerrar" data-idsess="<?php echo $r->idses ?>" data-nroses="<?php echo $r->nrosesion ?>" data-diases="<?php echo date('Y-m-d', strtotime($r->tstampini)); ?>" data-iduser="<?php echo $r->iduser; ?>" title="Cierra ésta sesión para ver el resumen">CERRAR</a>
                        <?php else : ?>
                          <a href="#" class="btn btn-success button_card disabled" title="Esta Sesión NO ha sido abierta por el usuario activo" style="pointer-events:none; ">REANUDAR</a>
                          <a href="#" class="btn btn-danger button_card disabled" title="Esta Sesión NO ha sido abierta por el usuario activo" style="pointer-events:none; ">CERRAR</a>
                        <?php endif; ?>
                      </div>
                    </div>
                  </div>
                  <!--  -->
                  <hr>
                  <!--  -->
                  <div class="card__description">
                    <!-- <a href="#" class="button_card">Call to Action</a> -->
                    <button class="btn btn-lg btn-block btn-outline-primary consultsess" id="consultsess" title="Consultar esta Sesión" data-idsess="<?php echo $r->idses; ?>" data-nroses="<?php echo $r->nrosesion; ?>" data-fecini="<?php echo $r->tstampini; ?>" data-fecfin="<?php echo date('Y-m-d', strtotime($r->tstampini)) . ' 23:59:59'; ?>" data-iduser="<?php echo $r->iduser; ?>">
                      <h1 class="font-light pt-2"><i class="mdi mdi-help-circle-outline"></i></h1>
                    </button>
                  </div>

                </article>
              <?php endif; ?>
            <?php
            endforeach;
            ?>
          </section>
        </div>
      </div>
      <!-- FIN AREA DE SESIONES ABIERTAS  -->
      <!-- AREA DE SESIONES CERRADAS  -->
      <div class="col-12">
        <div class="card">
          <!-- ENCABEZADO  -->
          <div class="card-body">
            <h3 class="card-title m-b-0">Sesiones Cerradas</h3>
            <hr>
          </div>
          <section class="card-container">
            <?php
            foreach ($resultados as $r) :
              $st = $r->status;
              if ($st == 'CERRADA') :
                $tot_sesion = $this->Ventas_model->get_totfact_por_sesion($r->iduser, $r->nrosesion, $r->tstampini, $r->tstampfin);
                echo '
                    <article class="card card__article">
                      <header class="card__title">
                        <h3><span class="totsess">Tot.: ' . number_format($tot_sesion->totfact, 2, '.', ',') . '</span></h3>
                        <hr>
                      </header>
                      <div class="card__infodata">
                        <div class="d-none d-xs-none d-sm-none d-md-block d-lg-block" style="width: 10rem;">
                          <!-- <figure class="card__thumbnail_cerrada"> -->
                            <img class="img_sess_open" src="' . base_url() . 'assets/images/icono_presup400x400.png">
                          <!-- </figure> -->
                        </div>
                        <div class="card__datasess_cerrada" style="flex-basis: 60%;">
                          <div class="price" style="font-size: 0.8rem;">
                            <span class="m-b-15 d-block" style="margin-bottom: 5px !important;">' . fecha_larga(date('d-m-Y', strtotime($r->tstampini))) . '</span>
                            <br />
                            <span class="opername" style="font-size: 0.8rem;">' . $r->first_name . '</span>
                          </div>
                          <span class="span_status">Status: ' . $r->status . '<br/> Sess #: ' . $r->nrosesion . '</span>
                          <div class="stats_cerrada">
                            <span><i class="far fa-play-circle"></i>&nbsp;&nbsp;' . fecha_larga_sin_dia_semana(date('d-m-Y', strtotime($r->tstampini))) . '</span>
                            <br />
                            <span><i class="far fa-stop-circle"></i>&nbsp;&nbsp;' . fecha_larga_sin_dia_semana(date('d-m-Y', strtotime($r->tstampfin))) . '</span>
                          </div>
                          <div class="datesess">INI: ' . date('h:i a', strtotime($r->tstampini)) . ', FIN: ' . date('h:i a', strtotime($r->tstampfin)) . '</div>
                        </div>
                      </div>
                      <main class="card__description">
                      <hr>
                        <button
                            class="btn btn-lg btn-block btn-outline-primary consultsess"
                            id="consultsess"
                            title="Consultar esta Sesión"
                            data-idsess="' . $r->idses . '"
                            data-nroses="' . $r->nrosesion . '"
                            data-fecini="' . $r->tstampini . '"
                            data-fecfin="' . $r->tstampfin . '"
                            data-iduser="' . $r->iduser . '">
                              <h1 class="font-light pt-2"><i class="mdi mdi-help-circle-outline"></i></h1>
                        </button>
                      </main>
                      <!-- <a href="#" class="button_card">Call to Action</a> -->

                    </article>
                    ';
              endif;
            endforeach;
            ?>
          </section>
        </div>
        <!-- FIN AREA DE SESIONES CERRADAS  -->
        <?php // para mostrar los enlaces de la paginacion
        echo '<div class="col-md-12"><nav class="float-right" aria-label="..."> ' . $this->pagination->create_links() . '</nav></div>';
        $this->session->unset_userdata('buscando'); ?>
      <?php
    endif;
      ?>
      </div>
  </div>
  <!-- ============================================================== -->
  <!-- End Container fluid  -->
  <!-- ============================================================== -->
  <script>
    // para acomodar las tablas en monitores de menos de 1024px
    $('#zero_config').stacktable();
  </script>
  <script src="<?php echo base_url() ?>assets/libs/jquery.redirect/jquery.redirect.js"></script>
  <script>
    $('.cerrar').on('click', function() {
      //
      var seguro = '<?php echo $this->security->get_csrf_hash(); ?>';
      var idsess = this.getAttribute("data-idsess");
      var nroses = this.getAttribute("data-nroses");
      var diases = this.getAttribute("data-diases");
      var iduser = this.getAttribute("data-iduser");
      //alert('codped: '+codped+' - '+'emailc: '+emailc);
      $.redirect("/ventas/cerrar_session", {
        idsess: idsess,
        nroses: nroses,
        diases: diases,
        iduser: iduser,
        token: seguro
      }, "POST");
    });
  </script>
  <script>
    // cuando haga clic en el boton editar
    $('.consultsess').on('click', function(event) {
      var idsess = this.getAttribute("data-idsess"); // Extract info from data-* attributes
      var nroses = this.getAttribute("data-nroses"); // Extract info from data-* attributes
      var fecini = this.getAttribute("data-fecini"); // Extract info from data-* attributes
      var fecfin = this.getAttribute("data-fecfin"); // Extract info from data-* attributes
      var iduser = this.getAttribute("data-iduser"); // Extract info from data-* attributes
      var seguro = '<?php echo $this->security->get_csrf_hash(); ?>';
      $.redirect("/ventas/validar_sesion", {
        idsess: idsess,
        nroses: nroses,
        fecini: fecini,
        fecfin: fecfin,
        iduser: iduser,
        token: seguro
      }, "POST");
    });
  </script>
  <!-- Datepicker de Bootstrap-->
  <link href="<?php echo base_url() ?>assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
  <script src="<?php echo base_url() ?>assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="<?php echo base_url() ?>assets/libs/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js"></script>
  <script>
    //Date range as a button
    $(".datepicker").datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      language: 'es',
    });
  </script>