<form id="form-modificar" action="<?php echo base_url();?>ventas/devolver_producto" method="post" role="form">
	<input type="hidden" name="token" value="<?php echo $this->security->get_csrf_hash();?>">
	<div class="modal-header bg-danger">
			<h4 class="modal-title text-center" id="myModalLabel" style="color:#FFF">
				DEVOLVER PRODUCTO
			</h4>
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
	</div>

	<div class="modal-body">

		<div class="form-group col-md-12 text-center">
			<h3><?php echo $desc?></h3>
			<h3>Cantidad: <?php echo $cant?></h3>
			<h3>Precio de venta: <?php echo number_format($pvpd,2,'.',',')?></h3>
			<h2>Desea DEVOLVER éste <br>PRODUCTO<br>
					Si: <input type="radio" id="optionsRadios" name="optionsRadios" value="Si"><br>
					<input type="hidden" name="idfact" id="idfact" value="<?php echo $idfact;?>" />
					<input type="hidden" name="nfact" id="nfact" value="<?php echo $nfact;?>" />
					<input type="hidden" name="idinv" id="idinv" value="<?php echo $idinv;?>" />
					<input type="hidden" name="cantp" id="cantp" value="<?php echo $cant;?>" />
					<input type="hidden" name="pvpd" id="pvpd" value="<?php echo $pvpd;?>" />
					<input type="hidden" name="tasa" id="tasa" value="<?php echo $tasa;?>" />
			</h2>
			<p>Este PRODUCTO <b>SERÁ DEVUELTO AL INVENTARIO EN LA BASE DE DATOS</b>,<br>No se podrá modificar despues. Tome las debidas precauciones.</p>
		</div>

	</div>

	<div class="modal-footer col-md-12">
		<button type="button" class="btn btn-default" data-dismiss="modal">No Devolver</button>
		<button type="submit" name="Borrar" class="btn btn-danger" value="Borrar" title="Devolver" >DEVOLVER</button>
	</div>

</form>