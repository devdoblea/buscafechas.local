<?php
	//echo form_open('path/to/controller/update/method');
	$empresa = $this->Empresa_model->datos_empresa_row();
	// Ubicar la tasa dolar
  if ( $empresa->aplicar_tasa == 'Si') :
    $tasa = $empresa->tasa_dolar;
  elseif ( $empresa->aplicar_tasa == 'No') :
    $tasa = 1;
  endif;
	// Calcular IVA
	if ( $empresa->aplicar_iva == 'Si') :
		$iva = ( $empresa->iva / 100 );
	elseif ( $empresa->aplicar_iva == 'No') :
		$iva = 0;
	endif;
	if($this->cart->contents()) {
		$i = 1;
		$stpriceBs = 0;
		foreach ($this->cart->contents($newest_first = TRUE) as $items) :
			$priceBs = $items['price'] * $tasa;
			echo '<tr data-row="'.$i.form_hidden($i.'[rowid]', $items['rowid']).'">';
			if ($this->cart->has_options($items['rowid']) == TRUE):
				foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value):
					echo '<td class="text-center">'.$i.'</td>';
					echo '<td class="text-center">'.number_format($items['qty'],3,'.',',').'</td>';
					echo '<td class="text-left;" >'.$items['name'].'</td>';
					echo '<td class="text-center" >'.number_format($items['price'],2,'.',',').'</td>';
					echo '<td class="text-center" >'.number_format($priceBs,2,'.',',').'</td>';
					$subtot = $items['qty'] * $items['price'];
					echo '<td class="text-right" >'.number_format($subtot,2,'.',',').'</td>';
				endforeach;
			endif;
					$bborrar = array (
						'name'          => 'borrar',
						'id'            => 'borrar',
						'type'          => 'button',
						'content'       => '<i class="fas fa-minus"></i>',
						'class'         => 'btn btn-xs btn-danger',
						'title'         => 'Borrar este Producto',
						'data-toggle' 	=> 'modal',
						'data-borrar' 	=> $items['rowid'],
						'data-cantidad' => $items['qty'],
						'data-target' 	=> '#ventanaBorrar',
					);
					echo '<td>'.form_button($bborrar).'</td>';
					echo '</tr>';
			$i++;
			$stpriceBs = $stpriceBs + $priceBs;
		endforeach;
		// llena de lineas hasta el final
		while ($i<=6) {
			echo "
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td class='text-center'>
						<button class='btn btn-xs btn-disable disabled' ><i class='fa fa-minus-square'></i></button>
					</td>
				</tr>
			";
			$i++;
		} // fin del while
		//
		$subtdolar = $tasa / $this->cart->total();
		$total_des = 0;
		$total_iva = ( $this->cart->total() - $total_des ) * $iva;
		$total_ped = ( $this->cart->total() - $total_des ) + $total_iva;
		echo '
				<tr>
					<td class="text-right" colspan="4">
						<input type="hidden" name="total_items" id="total_items" value="'.$this->cart->total_items().'"/>
						<strong>SubTotal Pedido Precio Detal:</strong>
					</td>
					<td colspan="2" class="right">
						<span id="valor_cart">'.number_format($this->cart->total(),2,'.',',').'</span>
					</td>
					<td colspan="1" class="right">
						<input type="hidden" name="count_items" id="count_items" value="'.count($this->cart->contents()).'"/>
					</td>
				</tr>
				<tr>
					<td class="text-right" colspan="4">
						<input type="hidden" name="valor_desc" id="valor_desc" value=""/>
						<strong>Total Bs.D:</strong>
					</td>
					<td colspan="2" class="right"><span id="total_boli">'.number_format($stpriceBs,2,'.',',').'</span></td>
					<td colspan="1"></td>
				</tr>
				<tr>
					<td class="text-right" colspan="4">
						<strong>IVA:('.number_format($empresa->iva,2,'.',',').')</strong>
						<input type="hidden" name="iva_pct" id="iva_pct" value="'.$iva.'"/>
					</td>
					<td colspan="2" class="right"><span id="total_iva">'.number_format($total_iva,2,'.',',').'</span></td>
					<td colspan="1"></td>
				</tr>
				<tr>
					<td class="text-right" colspan="4"><strong>Total Pedido al Detal:</strong></td>
					<td colspan="2" class="right"><span id="total_ped">'.number_format($total_ped,2,'.',',').'</span></td>
					<td colspan="1"></td>
				</tr>
		';
	} else {
		// si no hay registros
		// llena de lineas hasta el final
		$i = 1;
		while ($i<=6) {
			echo "
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td class='text-center'>
						<button class='btn btn-xs btn-disable disabled' ><i class='fa fa-minus-square'></i></button>
					</td>
				</tr>
			";
			$i++;
		} // fin del while
		echo '
				<tr>
					<td class="text-right" colspan="4">
						<input type="hidden" name="total_items" id="total_items" value=""/>
						<strong>SubTotal Pedido Precio Detal:</strong>
					</td>
					<td colspan="2" class="right"><span id="valor_cart"></span></td>
					<td colspan="1" class="right"></td>
				</tr>
				<tr>
					<td class="text-right" colspan="4">
						<input type="hidden" name="valor_desc" id="valor_desc" value=""/>
						<strong>Total Bs.D:</strong>
					</td>
					<td colspan="2" class="right"><span id="total_boli"></span></td>
					<td colspan="1"></td>
				</tr>
				<tr>
					<td class="text-right" colspan="4">
						<strong>IVA:('.number_format($empresa->iva,2,'.',',').')</strong>
						<input type="hidden" name="iva_pct" id="iva_pct" value="'.$iva.'"/>
					</td>
					<td colspan="2" class="right"><span id="total_iva"></span></td>
					<td colspan="1"></td>
				</tr>
				<tr>
					<td class="text-right" colspan="4"><strong>Total Pedido al Detal:</strong></td>
					<td colspan="2" class="right"><span id="total_ped"></span></td>
					<td colspan="1"></td>
				</tr>
		';
	}
?>