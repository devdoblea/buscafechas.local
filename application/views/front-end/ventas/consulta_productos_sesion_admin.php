<div class="modal-header bg-success"style="color:#FFFFFF">
  <h5 class="modal-title" id="exampleModalLabel">Consulta Venta Nro.: <?php echo $nrofac?></h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
  <div class="row mb-3">
    <div class="col-12">
      <!--  -->
      <div class="row">
        <div class="col-md-4 col-sm-12">
          <label for="nrofactc" class="control-label col-form-label">Nro. Factura Venta:</label>
          <input type="text" name="nrofact" id="nrofact" class="form-control text-center input_material" value="<?php echo $ventaencab->nrofact?>" disabled />
        </div>
        <div class="col-md-4 col-sm-12">
          <label for="fechafact" class="control-label col-form-label pl-4">Fecha Venta:</label>
          <input type="text" name="fechafact" id="fechafact" class="form-control text-center input_material" value="<?php echo cambfecha($ventaencab->fechafact)?>" disabled />
        </div>
        <div class="col-md-4 col-sm-12">
          <label for="tasaventa" class="control-label col-form-label pl-3">Tasa Venta:</label>
          <input type="text" name="tasaventa" id="tasaventa" class="form-control text-center input_material" value="<?php echo number_format($ventaencab->tasaDolar,2,'.',',');?>" disabled />
        </div>
      </div>
      <!--  -->
      <div class="row">
        <div class="col-md-2 col-sm-12">
          <label for="ccedula" class="control-label col-form-label">Ced/Rif Cliente:</label>
          <input type="text" name="ccedula" id="ccedula" class="form-control text-center input_material"  value="<?php echo $ventaencab->nac.'-'.$ventaencab->ccedula;?>" disabled />
        </div>
        <!--  -->
        <div class="col-md-5 col-sm-12">
          <label for="apenombre" class="control-label col-form-label" style="margin-left: 4vw;">Cliente:</label>
          <input type="text" name="apenombre" id="apenombre" class="form-control text-center input_material" value="<?php echo $ventaencab->apenombre?>" disabled />
        </div>
        <!--  -->
        <div class="col-md-5 col-sm-12">
          <label for="ecorreo" class="control-label col-form-label">Email:</label>
          <input type="text" name="ecorreo" id="ecorreo" class="form-control text-center input_material" value="<?php echo $ventaencab->ecorreo?>" disabled />
        </div>
      </div>
      <!--  -->
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <label for="tlfnocelp" class="control-label col-form-label ml-2">Celular:</label>
          <input type="text" name="tlfnocel" id="tlfnocel" class="form-control text-center input_material" value="<?php echo $ventaencab->tlfnocel?>" data-mask-tel disabled />
        </div>
        <div class="col-md-6 col-sm-12">
          <label for="tlfnocasap" class="control-label col-form-label ml-2">Casa:</label>
          <input type="text" name="tlfnocasa" id="tlfnocasa" class="form-control text-center input_material" value="<?php echo $ventaencab->tlfnocasa?>" data-mask-tel disabled />
        </div>
      </div>
      <!--  -->
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <label for="dircli" class="control-label col-form-label">Dirección:</label>
          <textarea style="text-align:justify;" name="dircli" id="dircli" rows="3" class="form-control border border-success" spellcheck="true" maxlength="1000" onkeyup="if(this.value.length >= 1000){ toastr.options.timeOut = 4000; toastr.warning('Has superado el m&aacute;ximo de texto permitido'); return false; }" onchange="javascript:this.value=this.value.toUpperCase();" disabled ><?php echo $ventaencab->dircli?></textarea>
        </div>
        <div class="col-md-6 col-sm-12">
          <label for="observacion" class="control-label col-form-label mr-1">Observacion:</label>
          <textarea style="text-align:justify;" name="observacion" id="observacion" rows="3" class="form-control border border-success" spellcheck="true" maxlength="1000" onkeyup="if(this.value.length >= 1000){ toastr.options.timeOut = 4000; toastr.warning('Has superado el m&aacute;ximo de texto permitido'); return false; }" onchange="javascript:this.value=this.value.toUpperCase();" disabled ><?php echo $ventaencab->observacion?></textarea>
        </div>
      </div>
      <hr>
      <!--  -->
    </div>
  </div>
  <div class="row mb-3">
    <div class="col-12">
      <div class="col-md-12 col-sm-12">
        <div class="table-responsive">
          <table id="zero_config" class="table table-striped table-bordered">
            <thead>
              <tr class="bg-success">
                <th class="column-title text-center" style="color:#FFFFFF;width: 7vw;"> Cant.           </th>
                <th class="column-title text-center" style="color:#FFFFFF;width: 24vw;"> Articulo despachado</th>
                <th class="column-title text-center" style="color:#FFFFFF;width: 8vw;"> Sub-Total <br>$  </th>
                <th class="column-title text-center" style="color:#FFFFFF;width: 12vw;"> Sub-Total <br><?php echo $empresa->simbol_moneda;?></th>
                <th class="column-title text-center" style="color:#FFFFFF;width: 4vw;"> DEVOLVER</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $empresa = $this->Empresa_model->datos_empresa_row();
                if ( $empresa->aplicar_iva == 'Si') :
                  $aplicar_iva = 'SI APLICA';
                  $iva = ( $empresa->iva / 100 );
                elseif ( $empresa->aplicar_iva == 'No') :
                  $aplicar_iva = 'NO APLICA';
                  $iva = 0;
                endif;
                $tot_dolar = 0;
                if ( $ventacuerp != 'nada') :
                  foreach ( $ventacuerp as $r ) :
                    $st = $r->statusprod;
                    if( $r->cantpedido != 0 ) :
                      $pvent = $r->stotlVenta / $r->cantpedido;
                      $punit = $r->precioDetal  / $r->cantpedido;
                      $subtot = $r->cantpedido * $r->precioDetal;
                      $stotdl = $r->cantpedido * ($r->precioDetal / $ventaencab->tasaDolar);
                    else :
                      $pvent = 0.00;
                      $punit = 0.00;
                      $subtot = 0.00;
                      $stotdl = 0.00;
                    endif;
                    if ($st=='DEVUELTO') {$bg = 'alert alert-danger';} else {$bg = '';}
                    echo '<tr class="'.$bg.'" title="'.$st.'">';
                    echo '<td class="text-center">'.$r->cantpedido.'</td>';
                    echo '<td class="text-left;">'.$r->descriprod.'<br>
                            P. Venta (desde BD): '.number_format($r->precioDetal,2,'.',',').'</td>';
                    echo '<td class="text-center">'.number_format($r->stdolVenta,2,'.',',').'</td>';
                    echo '<td class="text-center">'.number_format($r->stotlVenta,2,'.',',').'</td>';
                    echo '<td class="text-center">';
                    if ($r->statusprod == "DEVUELTO") :
                      echo '<button type="button" class="btn btn-danger" style="cursor: default;" title="Devolver Producto" disabled><i class="fas fa-undo-alt"></i></button>';
                    else :
                      echo '<button type="button" class="btn btn-danger" id="borrar" name="borrar" data-nfact="'.$nrofac.'" data-idfact="'.$r->idfact.'" data-idinv="'.$r->idinventa.'" data-desc="'.$r->descriprod.'" data-cant="'.$r->cantpedido.'" data-pvpd="'.$r->precioDetal.'" data-tasa="'.$ventaencab->tasaDolar.'" data-toggle="modal" data-target="#ventanaDevolverProd" style="cursor: pointer;" title="Devolver Producto"><i class="fas fa-undo-alt"></i></button>';
                    endif;
                    echo    '</td>';
                    echo '</tr>';
                    $tot_dolar = $tot_dolar + $stotdl;
                  endforeach;
                else :
                  echo '
                    <tr>
                      <td colspan="6" class="text-center"><strong>ERROR: NO HAY PRODUCTOS PARA MOSTRAR (PUDO HABER UNA DEVOLUCIÓN DE PRODUCTOS???)</strong></td>
                    </tr>
                  ';
                endif;
                echo '
                  <tr>
                    <td class="text-right" colspan="3"><strong>SubTotal Precio Venta:</strong></td>
                    <td colspan="2" class="text-center">'.number_format($ventaencab->netofact,2,'.',',').'</td>
                  </tr>
                  <tr>
                    <td class="text-right" colspan="3"><strong>SubTotal Venta Precio Dolar:</strong></td>
                    <td colspan="2" class="text-center">'.number_format($ventaencab->totusd,2,'.',',').'</td>
                  </tr>
                  <tr>
                    <td class="text-right" colspan="3"><strong title="Consulta la Configuración de tu Sistema para cambiar la aplicación del IVA en la proxima compra.">IVA:</strong></td>
                    <td colspan="2" class="text-center">'.number_format($ventaencab->ivafact,2,'.',',').'</td>
                  </tr>
                  <tr>
                    <td class="text-right" colspan="3"><strong>Total Venta al Detal:</strong></td>
                    <td colspan="2" class="text-center">'.number_format($ventaencab->totalfact,2,'.',',').'</td>
                  </tr>
                ';
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
</div>
