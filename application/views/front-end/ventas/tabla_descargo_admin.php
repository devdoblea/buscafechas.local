<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid d-flex h-100 flex-column">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-heading">
					<?php echo form_open(base_url().'ventas/validar_descargo','class="formulario"') ?>
					<div class="row">
            <div class="col-12 form-inline">
              <div class="col-md-6 col-sm-12 col-xs-12 input-group mb-3 mt-3 float-left">
								<input type="text" class="form-control" id="buscando" name="buscando" value="<?php echo set_value('buscando')?>" placeholder="Buscar por Cliente o Nro de factura..." accept-charset="utf-8" placeholder="Introducir búsqueda" autocomplete="off" autocapitalize="off" autocorrect="off" spellcheck="false" maxlength="45">
                <input type="hidden" name="iduser" value="<?php echo $iduser;?>">
								<div class="input-group-append">
									<button class="btn btn-danger" type="submit"><i class="ti-search"></i></button>
								</div>
							</div>
              <div class="col-md-6 col-sm-12 col-xs-12 mt-3">
                <!--  -->
                <a href="/ventas/regdescargo" class="btn btn-lg btn-success col-md-4 col-sm-12 col-xs-12 float-right margin-5" id="agregar"> <i class="mdi mdi-cash-register d-xs-block d-sm-block d-md-none d-lg-none"></i> <span class="d-xs-none d-sm-none d-md-block d-lg-block">Agregar Descargo</span> </a>
              </div>
            </div>
		  		</div>
					<?php echo form_close() ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<div class="card">
				<?php
				  if(!$resultados) {
					echo '
						<div class="card-body border-top height-box">
							<div class="alert alert-danger" role="alert">
  						  <h4 class="alert-heading">No Hay Resultados</h4>
  						  <p>Es posible que aùn no se haya registrado un Descargo.</p>
  						  <hr>
  						  <p class="mb-0"><strong>Por favor verifica y en caso que creas que esto no es correcto, llama al Administrador/Desarrollador del Sistema</strong></p>
  						</div>
            </div>
					';
		   		} else {
				?>
				<div class="card-body" style="min-height: 550px;">

					<div class="table-responsive">
						<table id="zero_config" class="table table-striped table-bordered">
              <thead>
                  <tr style="vertical-align:middle;">
                      <th class="text-center">Id.</th>
                      <th class="text-center">Nro Factura</th>
                      <th class="text-center">Cliente</th>
                      <th class="text-center">Cant. Prod.</th>
                      <th class="text-center">Neto.</th>
                      <th class="text-center">I.V.A</th>
                      <th class="text-center">Total Fact.</th>
                      <th class="text-center" style="width: 6vw;">Productos Vendidos</th>
                      <!-- <th class="text-center" style="width: 6vw;">ReImprime Factura</th> -->
                  </tr>
              </thead>
              <tbody>
                <?php
                  foreach ( $resultados as $r ) :
                    $cd = $r->nac;
                    if ($cd != 'V' && $cd != 'E') {
                      $cedula = $r->nac.'-'.$r->ccedula;
                    } else {
                      $cedula = $r->nac.'-'.number_format($r->ccedula,0,',','.');
                    }
                    echo '<tr>';
                    echo '<td style="text-align:center; vertical-align:middle;">'.$r->idfgen.'</td>';
                    echo '<td style="text-align:center; vertical-align:middle;">'.$r->nrofact.'</td>';
                    echo '<td style="text-align:left;   vertical-align:middle;">'.$cedula.' '.$r->apenombre.'</td>';
                    echo '<td style="text-align:center; vertical-align:middle;">'.$r->cant_prod.'</td>';
                    echo '<td style="text-align:center; vertical-align:middle;">'.number_format($r->netofact,2,'.',',').'</td>';
                    echo '<td style="text-align:center; vertical-align:middle;">'.number_format($r->ivafact,2,'.',',').'</td>';
                    echo '<td style="text-align:center; vertical-align:middle;">'.number_format($r->totalfact,2,'.',',').'</td>';
                    echo '<td style="text-align:center; vertical-align:middle;">
                            <button class="btn btn-info" data-toggle="modal" data-target="#ventanaConsultaProd" data-nrofac="'.$r->nrofact.'" title="Consulta cuales Productos se vendieron en esta factura"><i class="mdi mdi-receipt"></i></button></td>';
                    // echo '<td style="text-align:center; vertical-align:middle;">
                            // <button class="btn btn-danger reimpfact" id="reimpfact" name="reimpfact" class="btn btn-secondary" data-idfgen="'.$r->idfgen.'" title="Imprime esta factura"><i class="mdi mdi-receipt"></i></a></td>';
                  endforeach;
                ?>
              </tbody>
							<tfoot>
							</tfoot>
						</table>
						<?php // para mostrar los enlaces de la paginacion
              echo '
              <div class="col-md-12"><nav class="float-right" aria-label="..."> '.$this->pagination->create_links().'</nav></div>';
              //$this->session->unset_userdata('buscando');$this->session->unset_userdata('nroses');$this->session->unset_userdata('fecini');$this->session->unset_userdata('fecfin');?>
					</div>

				</div>
				<?php } // fin del else?>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- Con este grupo de divs se abre la ventana modal que traera la info a agregar estud-->
<div class="modal fade in" id="ventanaConsultaPag" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="ct">
            </div>
        </div>
    </div>
</div>
<!-- Con este grupo de divs se abre la ventana modal que traera la info a agregar estud-->
<div class="modal fade in" id="ventanaConsultaProd" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="ct">
            </div>
        </div>
    </div>
</div>
<script>// para acomodar las tablas en monitores de menos de 1024px
  $('#zero_config').stacktable();
</script>
<script src="<?php echo base_url()?>assets/libs/jquery.redirect/jquery.redirect.js"></script>
<script>
    $('.reimpfact').on('click', function(){
     //
     var seguro = '<?php echo $this->security->get_csrf_hash();?>';
     var idfgen = this.getAttribute("data-idfgen");
     //alert('codped: '+codped+' - '+'emailc: '+emailc);
     $.redirect("/ventas/reimprime_factura", { idfgen: idfgen, token:seguro }, "POST");
    });
</script>
<script>
  $('#ventanaConsultaPag').on('show.bs.modal', function(event) {
    var seguro = '<?php echo $this->security->get_csrf_hash();?>';
    var button = $(event.relatedTarget) // Button that triggered the modal
    var nrofac = button.data('nrofac'); // Extract info from data-* attributes
    var modal = $(this);
    var dataString = { nrofac:nrofac, token:seguro };
      $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>ventas/get_formas_pago_by_factura_admin",
          data: dataString,
          success: function (data) {
              //console.log(data);
              modal.find('.ct').html(data);
          },
          error: function(err) {
            toastr.options.timeOut = 4000; // 3s
            toastr.error('ventanaEditarCli: '+JSON.stringify(err['statusText']));
          }
      });
  });
  // Consultar los productos
  $('#ventanaConsultaProd').on('show.bs.modal', function(event) {
    var seguro = '<?php echo $this->security->get_csrf_hash();?>';
    var button = $(event.relatedTarget) // Button that triggered the modal
    var nrofac = button.data('nrofac'); // Extract info from data-* attributes
    var modal = $(this);
    var dataString = { nrofac:nrofac, token:seguro };
      $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>ventas/get_consulta_productos_sesion_admin",
          data: dataString,
          success: function (data) {
              //console.log(data);
              modal.find('.ct').html(data);
          },
          error: function(err) {
            toastr.options.timeOut = 4000; // 3s
            toastr.error('ventanaConsultaProd: '+JSON.stringify(err['statusText']));
          }
      });
  });
  // cuando haga clic en el boton editar
  $('#ventanaEditarCli').on('show.bs.modal', function(event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var ides = button.data('ides'); // Extract info from data-* attributes
      var seguro = '<?php echo $this->security->get_csrf_hash();?>';
      var modal = $(this);
      var dataString = {ides: ides, token:seguro }
          $.ajax({
              type: "POST",
              url: "<?php echo base_url()?>clientes/editcliente",
              data: dataString,
              success: function (data) {
                  //console.log(data);
                  modal.find('.ct').html(data);
              },
              error: function(err) {
                toastr.options.timeOut = 4000; // 3s
                toastr.error('ventanaEditarCli: '+JSON.stringify(err['statusText']));
              }
          });
  });
</script>
