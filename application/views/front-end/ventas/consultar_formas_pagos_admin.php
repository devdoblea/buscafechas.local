<div class="modal-header bg-success"style="color:#FFFFFF">
	<h5 class="modal-title" id="exampleModalLabel">Formas de pago de esta Factura</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
<div class="modal-body">
	<div class="row mb-3">
    <div class="col-md-12 col-sm-12  ">
      <div class="d-flex justify-content-center">
        <div class="table-responsive">
          <table id="zero_config" class="table table-striped table-bordered">
            <thead>
              <tr style="vertical-align:middle;">
                <th class="text-center" colspan="3">
                    FORMAS DE PAGO<br>
                    <h4 class="modal-title">Fact. #: <?php echo $nrofac;?></h4>
                    <br />
                    <b><small class="modal-title">Tasa Dolar para ésta Factura: <?php echo number_format($factencab->tasaDolar,2,'.',',');?></small></b>
                </th>
                <tr>
                  <tr style="vertical-align:middle;">
                  <th class="text-center">Forma Pago  </th>
                  <th class="text-center">Nro Vaucher </th>
                  <th class="text-center">Entregado   </th>
                </tr>
            </thead>
            <tbody>
              <?php
                $tot = 0;
                if($resultados != 'nada') :
                  foreach ( $resultados as $r ) :
                    echo '<tr>';
                    echo '<td class="text-center align-middle">'.$r->formapagoval.'</td>';
                    echo '<td class="text-center align-middle">'.$r->nrovaucher.'</td>';
                    echo '<td class="text-center align-middle">'.number_format($r->entregado,2,'.',',').'</td>';
                    echo '</tr>';
                    // $tot = $tot + $r->entregado;
                  endforeach;
                  echo '    
                    <tr style="vertical-align:middle;">
                      <td class="text-right" colspan="2">Total Factura en $:</td>
                      <td class="text-center">'.number_format($factencab->totusd,2,'.',',').'</td>
                    </tr>
                    <tr style="vertical-align:middle;">
                      <td class="text-right" colspan="2">Total Factura en '.$empresa->simbol_moneda.':</td>
                      <td class="text-center">'.number_format($factencab->totalfact,2,'.',',').'</td>
                    </tr>
                  ';
                else :
                  echo '
                    <tr style="vertical-align:middle;">
                      <td class="text-center" colspan="3">ERROR EN CONSULTA</td>
                    </tr>
                  ';
                endif;
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	</div>
</div>
