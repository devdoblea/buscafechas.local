<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
  <!-- ============================================================== -->
  <!-- Start Page Content -->
  <!-- ============================================================== -->

  <div class="row">

    <div class="col-md-5">
      <form id="form-registro" action="" method="post">
        <input type="hidden" name="token" value="<?php echo $this->security->get_csrf_hash(); ?>" />

        <div class="card">
          <div class="card-body">
            <div class="row mb-3">
              <h4 class="card-title">Datos del Cliente</h4>
            </div>
            <div class="row mb-3">
              <div class="col-md-3">
                <?php
                // Setear la fecha y la hora para que sea la del SO y no del Server
                $myDate = ucfirst(setear_fecha(date('Y-m-d'), "d-m-Y"));
                ?>
                <label for="fventa" class="control-label col-form-label">Fecha Venta:</label>
                <input type="text" name="fventa" id="fventa" class="form-control text-center border border-secondary" maxlength="10" value="<?php echo $myDate; ?>" autocomplete="off" />
              </div>
              <div class="col-md-4">
                <label for="tipo_venta" class="control-label col-form-label">Tipo Venta:</label>
                <select id="tipo_venta" name="tipo_venta" class="form-control text-center border border-secondary" placeholder="Seleccione una Opcion">
                  <option class="text-center" value="DESCARGO"> DESCARGO </option>
                  <option class="text-center" value="CONTADO"> CONTADO </option>
                  <option class="text-center" value="CREDITO"> CREDITO </option>
                </select>
                <input type="hidden" name="idvend" id="idvend" value="1" />
                <input type="hidden" name="condcomercio" id="condcomercio" value="1" />
                <input type="hidden" name="descuento" id="descuento" value="0" />
                <input type="hidden" name="pctcomisionvend" id="pctcomisionvend" value="0" />
              </div>
              <div class="col-md-5">
                <label for="precio_venta" class="control-label col-form-label">Precio Venta:</label>
                <select id="precio_venta" name="precio_venta" class="form-control text-center border border-secondary" placeholder="Seleccione una Opcion">
                  <option class="text-center" value="precio_proveedor">PROVEEDOR</option>
                  <option class="text-center" value="precioDetal">DETAL</option>
                  <option class="text-center" value="precioMayor">MAYOR</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-5">
                <label for="ccedula" class="control-label col-form-label">Ced/Rif Cliente: </label>
                <div class="input-group">
                  <select id="nac" name="nac" class="form-control col-md-4 border border-secondary" placeholder="Seleccione una Opcion">
                    <option value=""></option>
                    <option value="V"> V </option>
                    <option value="E"> E </option>
                    <option value="J"> J </option>
                    <option value="G"> G </option>
                    <option value="P"> P </option>
                  </select>
                  <input type="text" name="ccedula" id="ccedula" class="form-control col-md-8 text-center border border-secondary" maxlength="16" title="Escriba el Nro. de Cedula/RIF, espere a que se despliegue la lista de posibles coincidencias y escoja la que considere es la correcta" />
                  <input type="hidden" name="idnrocli" id="idnrocli" />
                </div>
              </div>
              <div class="col-md-7">
                <label for="apenombre" class="control-label col-form-label">Nombres del Cliente:</label>
                <input type="text" name="apenombre" id="apenombre" class="form-control border border-secondary" autofocus="on" onchange="javascript:this.value=this.value.toUpperCase();" />
              </div>
            </div>
            <div class="form-group row">
              <label for="ecorreo" class="col-sm-3 control-label col-form-label">Email:</label>
              <div class="col-md-9">
                <input type="text" name="ecorreo" id="ecorreo" class="form-control border border-secondary" />
              </div>
            </div>
            <div class="form-group row">
              <label for="dircli" class="col-sm-3 control-label col-form-label">Dirección:</label>
              <div class="col-md-9">
                <input type="text" name="dircli" id="dircli" class="form-control border border-secondary" onchange="javascript:this.value=this.value.toUpperCase();" />
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-md-6">
                <label for="tlfnocel">Celular:</label>
                <input type="text" name="tlfnocel" id="tlfnocel" class="form-control border border-secondary" maxlength="16" onchange="javascript:this.value=this.value.toUpperCase();" data-mask-tel />
              </div>
              <div class="col-md-6">
                <label for="tlfnocasa">Casa:</label>
                <input type="text" name="tlfnocasa" id="tlfnocasa" class="form-control border border-secondary" maxlength="16" onchange="javascript:this.value=this.value.toUpperCase();" data-mask-tel />
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-md-12">
                <label for="observacion">Observacion:</label>
                <textarea style="text-align:justify;" name="observacion" id="observacion" rows="3" class="form-control border border-secondary" spellcheck="true" maxlength="1000" onkeyup="if(this.value.length >= 1000){ toastr.options.timeOut = 4000; toastr.warning('Has superado el m&aacute;ximo de texto permitido'); return false; }" onchange="javascript:this.value=this.value.toUpperCase();"></textarea>
              </div>
            </div>
          </div> <!-- Fin del card-body-->
        </div> <!-- Fin del card-->

    </div> <!-- Fin del col-md-5-->

    <div class="col-md-7">

      <div class="card">
        <div class="card-body">
          <!-- Cuerpo de la orden -->
          <h1 class="card-title">Descargo</h1>
          <div class="row mb-3">
            <div class="col-md-7">
              <label for="descriprod">Nombre del Producto</label>
              <input name="descriprod" id="descriprod" class="form-control border border-secondary" autocomplete="off" placeholder="Escriba descripcion, agrege el código si aplica" onkeyup="if(this.value.length >= 80){ toastr.options.timeOut = 4000; toastr.warning('Has superado el m&aacute;ximo de texto permitido'); return false; }" onchange="javascript:this.value=this.value.toUpperCase();" />
              <input type="hidden" name="idproducto" id="idproducto" />
              <input type="hidden" name="codbarprod" id="codbarprod" />
              <input type="hidden" name="tipo_prod" id="tipo_prod" />
              <input type="hidden" name="existencia" id="existencia" />
              <input type="hidden" name="exento_iva" id="exento_iva" />
            </div>
            <div class="col-md-2">
              <label for="cantpedido">Cant.</label>
              <input type="text" name="cantpedido" id="cantpedido" class="form-control border border-secondary" maxlength="8" />
            </div>
            <div class="col-md-2">
              <label for="codprod">Cod. Prod.</label>
              <input type="text" name="codprod" id="codprod" class="form-control border border-secondary" maxlength="7" onchange="javascript:this.value=this.value.toUpperCase();" />
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-3">
              <label for="precio_prov">Precio Proveedor:</label>
              <input type="text" name="precio_prov" id="precio_prov" class="form-control text-center border border-secondary decimal-2-places" maxlength="16" />
            </div>
            <div class="col-md-3">
              <label for="precioDetal">Precio Detal:</label>
              <input type="text" name="precioDetal" id="precioDetal" class="form-control text-center border border-secondary decimal-2-places" maxlength="16" />
              <input type="hidden" name="precioMayor" id="precioMayor" maxlength="16" value="0" />
            </div>
            <div class="col-md-3">
              <label for="categoria">Categoria</label>
              <input type="text" name="categoria" id="categoria" class="form-control border border-secondary" maxlength="40" onchange="javascript:this.value=this.value.toUpperCase();" />
              <input type="hidden" name="idcategoria" id="idcategoria" />
            </div>
            <div class="col-md-3">
              <label for="subcategoria">Sub-Categoria</label>
              <input type="text" name="subcategoria" id="subcategoria" class="form-control border border-secondary" maxlength="40" onchange="javascript:this.value=this.value.toUpperCase();" />
            </div>
            <div class="col-md-12">
              <button type="button" id="agregar_art" class="btn btn-success float-right mt-2" title="Agrega el articulo que seleccionaste a la Venta. HAGA dobleclick PARA REINICAR LOS CAMPOS"><i class="fa fa-plus"></i> Agregar</button>
            </div>

          </div>
          <hr color="green">
          <!-- Botones para registrar o salir de la orden-->
          <div class="row mb-3">
            <div class="col-md-6">
              <a href="<?php echo base_url() ?>ventas/cancelar_venta_desc" name="cancelar" id="cancelar" class="btn btn-sm btn-danger float-left pb-2 pt-2" title="Haz Clic aqui si ya agregaste Productos para Eliminarlos y salir de esta Venta" accesskey="b"> <u>B</u>orrar Descargo</a>
            </div>
            <div class="col-md-6">
              <button type="submit" name="registrar" id="registrar" class="btn btn-primary float-right" value="Registrar Datos" title="Haz Clic aqui para Registrar esta Orden y salir" accesskey="g"><u>G</u>rabar Descargo</button>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-6">
              <label>
                Cantidad de Productos Registrados:
                <span id="count_contents"></span>
              </label>
            </div>
          </div>
          <!-- Fin del cuerpo de la orden-->

          <!--Inicio de tabla que muestra los conceptos de la orden-->
          <div class="row mb-3">
            <div class="table-responsive">
              <table id="tabla_servicio" class="table table-striped table-bordered">
                <thead>
                  <tr class="bg-success" style="color:#FFFFFF">
                    <th class="column-title text-center"> #. </th>
                    <th class="column-title text-center"> Cant. </th>
                    <th class="column-title text-center" style="width: 280px;"> Articulo despachado</th>
                    <th class="column-title text-center" style="width: 80px;"> Precio<br>$</th>
                    <th class="column-title text-center" style="width: 80px;"> Precio<br>Bs.</th>
                    <th class="column-title text-center"> Sub-Total </th>
                    <th class="column-title text-center"> Acción </th>
                  </tr>
                </thead>
                <tbody id="tbody_listar" style="vertical-align:middle; text-align:center;"> </tbody>
              </table>
            </div>
          </div>
          <!--Inicio de tabla que muestra los conceptos de la orden-->
        </div><!-- fin del card-body -->
      </div> <!-- Fin del card-->

    </div> <!-- Fin del col-md-7-->
    </form><!-- Fin del From -->
  </div> <!-- Fin del row-->

  <!-- ============================================================== -->
  <!-- End Page Content -->
  <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- Con este grupo de divs se abre la ventana modal que traera la info a editar del estudiante-->
<div class="modal fade in" id="ventanaBorrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="ct text-center">
        <h2> Borrando..! <?php echo $this->input->post('idart'); ?></h2>
      </div>
    </div>
  </div>
</div>
<!-- Con este grupo de divs se abre la ventana modal que traera la info a editar del estudiante-->
<div class="modal fade in" id="ventanaEnviando" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="ct text-center">
      </div>
    </div>
  </div>
</div>
<script>
  // para acomodar las tablas en monitores de menos de 1024px
  $('#tabla_serv').cardtable();
</script>
<!-- Datepicker de Bootstrap-->
<link href="<?php echo base_url() ?>assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
<script src="<?php echo base_url() ?>assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url() ?>assets/libs/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js"></script>
<!-- Para alertar al usuario que no salga de la orden sin estar seguro-->
<script src="<?php echo base_url() ?>assets/libs/jquery.AreYouSure/jquery.are-you-sure.js"></script>
<script src="<?php echo base_url() ?>assets/libs/jquery.AreYouSure/ays-beforeunload-shim.js"></script>
<!-- Para agregar o borrar una orden desde el teclado-->
<script src="<?php echo base_url() ?>assets/libs/shortcuts/shortcuts.js"></script>
<script>
  /* Activando acceso a los botones del formulario a través de shortcuts o accesos directos
   * esta funcion sirve cuando la acompañas de la libreria shortcut.js */
  $(function() {
    shortcut.add("Alt+G", function() {
      $("#registrar").click();
    });
    shortcut.add("Alt+A", function() {
      $("#agregar_art").click();
    });
    shortcut.add("Alt+B", function() {
      $("#cancelar").click();
    });
  });
</script>
<!-- Para el estilo del autocomplete-->
<link href="<?php echo base_url() ?>assets/libs/jQuery-Autocomplete/dist/jquery.autocomplete.css" rel="stylesheet">
<script src="<?php echo base_url() ?>assets/libs/jQuery-Autocomplete/dist/jquery.autocomplete.js"></script>
<script>
  // para traer los datos del proveedor
  $('#ccedula').devbridgeAutocomplete({
    paramName: 'term',
    serviceUrl: '<?php echo base_url(); ?>clientes/buscarCliente',
    ajaxSettings: {
      dataType: "json",
      method: "POST"
    },
    minChars: 1,
    noSuggestionNotice: 'SIN RESULTADOS',
    onSelect: function(suggestions) {
      $('#nac').val(suggestions.nac);
      $('#idnrocli').val(suggestions.idnrocli);
      $('#apenombre').val(suggestions.apenombre);
      $('#dircli').val(suggestions.dircli);
      $('#ecorreo').val(suggestions.ecorreo);
      $('#tlfnocel').val(suggestions.tlfnocel);
      $('#tlfnocasa').val(suggestions.tlfnocasa);
      // Inabilitar los campos para que no puedan ser modificados
      $('#nac').attr("readonly", "readonly");
      $('#apenombre').attr("readonly", "readonly");
      $('#dircli').attr("readonly", "readonly");
      $('#ecorreo').attr("readonly", "readonly");
      $('#tlfnocel').attr("readonly", "readonly");
      $('#tlfnocasa').attr("readonly", "readonly");
      // cambio el inputmask si traigo un cliente con autocomplete
      var nac = $('#nac').val();
      if (nac == 'J' || nac == 'G' || nac == 'P') {

        $('#ccedula').inputmask('99999999-9', {
          'placeholder': ''
        });

      } else if (nac == 'V' || nac == 'E') {

        $('#ccedula').inputmask(["9.999.999", "99.999.999"], {
          'placeholder': ''
        });

      }
      //$(this).valid();  // para revalidar los campos en caso de reescribirlos
      $('#descriprod').focus();
      return;
    },
  })
  $('#apenombre').devbridgeAutocomplete({
    paramName: 'term',
    serviceUrl: '<?php echo base_url(); ?>clientes/buscarClienteNombre',
    ajaxSettings: {
      dataType: "json",
      method: "POST"
    },
    minChars: 1,
    noSuggestionNotice: 'SIN RESULTADOS',
    onSelect: function(suggestions) {
      $('#nac').val(suggestions.nac);
      $('#idnrocli').val(suggestions.idnrocli);
      $('#ccedula').val(suggestions.ccedula);
      $('#dircli').val(suggestions.dircli);
      $('#ecorreo').val(suggestions.ecorreo);
      $('#tlfnocel').val(suggestions.tlfnocel);
      $('#tlfnocasa').val(suggestions.tlfnocasa);
      // Inabilitar los campos para que no puedan ser modificados
      $('#nac').attr("readonly", "readonly");
      $('#ccedula').attr("readonly", "readonly");
      $('#dircli').attr("readonly", "readonly");
      $('#ecorreo').attr("readonly", "readonly");
      $('#tlfnocel').attr("readonly", "readonly");
      $('#tlfnocasa').attr("readonly", "readonly");
      // cambio el inputmask si traigo un cliente con autocomplete
      var nac = $('#nac').val();
      if (nac == 'J' || nac == 'G' || nac == 'P') {

        $('#ccedula').inputmask('99999999-9', {
          'placeholder': ''
        });

      } else if (nac == 'V' || nac == 'E') {

        $('#ccedula').inputmask(["9.999.999", "99.999.999"], {
          'placeholder': ''
        });

      }
      //$(this).valid();  // para revalidar los campos en caso de reescribirlos
      $('#descriprod').focus();
      return;
    },
  })
  $('#nac').on('change', function(event, ui) {
    if ($('#apenombre').length > 0) {
      $('#idnrocli').val('');
      $('#codprod').val('');
      $('#ccedula').val(null);
      $('#apenombre').val('');
      $('#dircli').val('');
      $('#ecorreo').val('');
      $('#tlfnocel').val('');
      $('#tlfnocasa').val('');
      // remover el readonly si lo tiene
      $('#nac').removeAttr("readonly");
      $('#apenombre').removeAttr("readonly");
      $('#dircli').removeAttr("readonly");
      $('#ecorreo').removeAttr("readonly");
      $('#tlfnocel').removeAttr("readonly");
      $('#tlfnocasa').removeAttr("readonly");
      $('#descriprod').focus();
    }
  });
  // para traer los datos del cliente
  $('#descriprod').devbridgeAutocomplete({
    paramName: 'term',
    serviceUrl: '<?php echo base_url(); ?>ventas/buscarProd',
    ajaxSettings: {
      dataType: "json",
      method: "POST"
    },
    minChars: 1,
    noSuggestionNotice: 'SIN RESULTADOS',
    onSelect: function(suggestions) {
      $('#idproducto').val(suggestions.idprod);
      $('#codprod').val(suggestions.codprod);
      $('#codbarprod').val(suggestions.codbarprod);
      $('#tipo_prod').val(suggestions.tipoprod);
      $('#cantpedido').val(suggestions.cantpedido);
      $('#precio_prov').val($.number(suggestions.precio_prov, 2, '.', ''));
      $('#precioDetal').val($.number(suggestions.precioDetal, 2, '.', ''));
      $('#precioMayor').val($.number(suggestions.precioMayor, 2, '.', ''));
      $('#categoria').val(suggestions.categoria);
      $('#subcategoria').val(suggestions.subcategoria);
      $('#idcategoria').val(suggestions.idcategoria);
      $('#existencia').val(suggestions.existencia);
      $('#exento_iva').val(suggestions.exento_iva);
      $("#cantpedido").focus();
      return;
    }
  });
  // Para traer datos de las categorias
  $('#categoria').devbridgeAutocomplete({
    params: {
      search_type: $('#categoria').val()
    },
    paramName: 'term',
    serviceUrl: '<?php echo base_url(); ?>ventas/listar_catego',
    ajaxSettings: {
      dataType: "json",
      method: "POST"
    },
    minChars: 1,
    noSuggestionNotice: 'SIN RESULTADOS',
    onSelect: function(suggestions) {
      $('#categoria').val(suggestions.categ);
      //console.log(JSON.stringify(suggestions));
      //$(this).valid();  // para revalidar los campos en caso de reescribirlos
      return;
    },
    onSearchStart: function(query) {
      console.log("query: " + query)
    },
    onSearchComplete: function(query, suggestions) {
      console.log(suggestions.data)
    }
  })
  // para traer datos de las subcategorias
  $('#subcategoria').devbridgeAutocomplete({
    params: {
      search_type: $('#subcategoria').val()
    },
    paramName: 'term',
    serviceUrl: '<?php echo base_url(); ?>ventas/listar_subcatego',
    ajaxSettings: {
      dataType: "json",
      method: "POST"
    },
    minChars: 1,
    noSuggestionNotice: 'SIN RESULTADOS',
    onSelect: function(suggestions) {
      $('#subcategoria').val(suggestions.scatg);
      console.log(JSON.stringify(suggestions));
      //$(this).valid();  // para revalidar los campos en caso de reescribirlos
      return;
    },
  })
</script>
<script>
  // para traer los datos del vendedor
  $("#nombvendedor").on('change', function() {
    $("#nombvendedor option:selected").each(function() {
      var idvnd = $(this).val();
      var seguro = '<?php echo $this->security->get_csrf_hash(); ?>';
      var dataString = {
        idvnd: idvnd,
        token: seguro
      };
      $.ajax({
        type: "POST",
        url: "<?php echo base_url() ?>ventas/datos_vendedor",
        data: dataString,
        async: false, //espera la respuesta antes de continuar
        success: function(data) {
          //console.log(JSON.stringify(data));
          var datos = JSON.parse(data);
          $('#idvend').val(datos[0].id);
          $('#pctcomisionvend').val(datos[0].pct);
          //$('#nombvendedor').val(data.name);
        },
        error: function(err) {
          toastr.options.timeOut = 4000; // 3s
          toastr.error('Vendedor: ' + JSON.stringify(err['statusText']));
        }
      });
    });
  });
</script>
<script>
  // ACTUALIZAR LA CANTIDAD DE PRODUCTOS QUE SE HAN REGISTRADO EN EL CARRITO
  function fn_actualiza_count_items() {
    var count_items = $('#count_items').val();
    if (count_items === null || typeof count_items == 'undefined' || count_items.length == 0) {
      $('#count_contents').text(' 0');
    } else {
      $('#count_contents').text(' ' + count_items);
    }
  }
  // para ejecutar la accion de mostrar los articuloas a listar
  function fn_buscar() {
    var seguro = '<?php echo $this->security->get_csrf_hash(); ?>';
    var dataString = {
      token: seguro
    };

    $.ajax({
      url: '<?php echo base_url() ?>ventas/listar_articulos_desc',
      type: 'POST',
      data: dataString,
      success: function(data) {
        $("#tbody_listar").html(data);
        // Vuelvo a contar los items que se han registrado hasta el momento.
        // setTimeout(() => { fn_actualiza_count_items(); }, 100);
        fn_actualiza_count_items();
      },
      error: function(err) {
        toastr.options.timeOut = 4000; // 3s
        toastr.error('fn_buscar: ' + JSON.stringify(err['statusText']));
      }
    });
  }

  // CARGAR Y CALCULAR EL DESCUENTO SEGUN SE LE ESCRIBA
  function fn_calcular_descuento() {
    //
    var valor_desc = $('#descuento').val();
    $('#val_dsc').text(' ' + $.number(valor_desc, 2, '.', ',') + ' %');
    var desc_pct = $.number((valor_desc / 100), 2, '.', '');
    $('#valor_desc').val(desc_pct);
    //
    var valor_cart = $.number($('#valor_cart').text(), 2, '.', '');
    var valor_desc = $.number((valor_cart * desc_pct), 2, '.', '');
    //
    var iva_pct = $.number($('#iva_pct').text(), 2, '.', '');

    var total_tiva = valor_cart * (iva_pct / 100);
    var total_desc = valor_cart * desc_pct;
    //
    var valor_ivat = $.number($('#total_iva').text(), 2, '.', '');
    var total_vent = (valor_cart - total_desc) + total_tiva;
    //
    $('#total_iva').text($.number(total_tiva, 2, '.', ','));
    $('#total_desc').text($.number(valor_desc, 2, '.', ','));
    $('#total_ped').text($.number(total_vent, 2, '.', ','));
  }
  $("#descuento").on('input keydown', function() {
    fn_calcular_descuento();
  });
</script>
<!-- Jquery.currency -->
<script src="<?php echo base_url() ?>assets/libs/currency.js/dist/currency.min.js"></script>
<script>
  /* **********************************
   * Cuando un producto esta por debajo de su existencia minima
   * y se intenta despachar una cantidad superior el sistema lanza un
   * aviso pero quena bloqueado el campo de "descriprod" evitando que se
   * pueda escribir otro producto. Para superar este proeblema se usa esta
   * funcion. El usuario debe hacer "dobleclick" para que se borre todo lo escrito
   * en el area de "VENTA" y asi poder escribir y seleccionar otro producto
   */
  $("#agregar_art").on('dblclick', function() {
    //limpio los campos de donde envie los datos a ser guardados
    $('#idproducto').val('');
    $('#codprod').val('');
    $('#codbarprod').val('');
    $('#tipo_prod').val('');
    $('#descriprod').val('');
    $('#cantpedido').val('');
    $('#precio_prov').val('');
    $('#precioDetal').val('');
    $('#precioMayor').val('');
    $('#idcategoria').val('');
    $('#categoria').val('');
    $('#subcategoria').val('');
    $('#exento_iva').val('');
    // le aviso al usuario que ya se registrò el articulo
    toastr.remove();
    toastr.options.timeOut = 4000; // 3s
    toastr.success("Reiniciando los Campos..!");
    // muestro otra vez la lista de datos
    $("#descriprod").focus();
    fn_actualiza_count_items();
    return;
  });
  // para agregar articulo por articulo al servicio
  $('#agregar_art').on('click', function(event) {
    // Compruebo que no hayan mas de 12 productos cargados
    var maxItems = 12;
    var itemsReg = parseInt($("#count_contents").text());
    // console.log('itemsReg:'+itemsReg);
    // console.log('compare: '+parseInt(itemsReg) == parseInt(maxItems));
    if (parseInt(itemsReg) == parseInt(maxItems)) {
      //limpio los campos de donde envie los datos a ser guardados
      $('#idproducto').val('');
      $('#codprod').val('');
      $('#codbarprod').val('');
      $('#tipo_prod').val('');
      $('#descriprod').val('');
      $('#cantpedido').val('');
      $('#precio_prov').val('');
      $('#precioDetal').val('');
      $('#precioMayor').val('');
      $('#idcategoria').val('');
      $('#categoria').val('');
      $('#subcategoria').val('');
      $('#exento_iva').val('');
      // le aviso al usuario que ya se registrò el articulo
      toastr.remove();
      toastr.options.timeOut = 4000; // 3s
      toastr.error("Has llegado al maximo de Items a Registrar en esta Venta");
      // muestro otra vez la lista de datos
      $("#descriprod").focus();
      return;
    }
    // compruebo que se envien datos para grabar via ajax
    if ($("#tipo_venta").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Este Producto NO puede agregarse si no selecciona el Tipo de Venta");
      $("#tipo_venta").focus();
      return;
    }
    if ($("#nac").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Este Producto NO puede agregarse si no hay un Cliente al cual agregarselo");
      $("#nac").focus();
      return;
    }
    if ($("#ccedula").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Este Producto NO puede agregarse si no hay un Cliente al cual agregarselo");
      $("#ccedula").focus();
      return;
    }
    if ($("#apenombre").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Este Producto NO puede agregarse si no hay un Cliente al cual agregarselo");
      $("#apenombre").focus();
      return;
    }
    if ($("#ecorreo").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Este Producto NO puede agregarse si no hay un Cliente al cual agregarselo");
      $("#ecorreo").focus();
      return;
    }
    if ($("#idvend").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Este Producto NO puede agregarse si no se le asigna un Vendedor");
      $("#idvend").focus();
      return;
    }
    if ($("#condcomercio").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Este Producto NO puede agregarse si no se selecciona la fecha de Vencimiento del Credito (Cobrar en:)");
      $("#condcomercio").focus();
      return;
    }
    if ($("#descriprod").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Descripción no puede estar vacio\n");
      $("#descriprod").focus();
      return;
    }
    if ($("#cantpedido").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Cantidad no puede estar vacio\n");
      $("#cantpedido").focus();
      return;
    }

    //Compruebo primero que el articulo no esta en el minimo de existencia
    var cped = currency($("#cantpedido").val(), {
      decimal: '.',
      separator: ','
    }).value;
    var exis = currency($("#existencia").val(), {
      decimal: '.',
      separator: ','
    }).value;
    if (cped > exis) {
      toastr.remove();
      toastr.options.timeOut = 4000; // 3s
      toastr.error("Estas despachando una <br>CANTIDAD MAYOR ( " + cped + " ) a la EXISTENCIA ACTUAL <br>( hay " + exis + " en existencia) <br>de este Producto");
      $("#cantpedido").focus(function(e) {
        $("#cantpedido").val('');
        return;
      });
      return;
    }
    // console.log(`exis: ${exis}`);
    if (exis <= 0.00) {
      toastr.options.timeOut = 4000; // 3s
      toastr.error("Estas despachando una <br>CANTIDAD MAYOR a la EXISTENCIA ACTUAL <br>( hay " + exis + " en existencia) <br>de este Producto");
      $('#buscando').focus();
      return;
    }
    //
    var pv = $.number($('#precio_prov').val(), 2, '.', '');
    var pd = $.number($('#precioDetal').val(), 2, '.', '');
    var pm = $.number($('#precioMayor').val(), 2, '.', '');
    var tprice = $("#precio_venta option:selected").text();
    if (tprice == 'PROVEEDOR') {
      price = pv;
    } else if (tprice == 'DETAL') {
      price = pd;
    } else if (tprice == 'MAYOR') {
      price = pm;
    } else {
      price = pd;
    }
    var pric = currency(price, {
      decimal: '.',
      separator: ','
    }).value;
    if (pric <= 0.00) {
      toastr.options.timeOut = 4000; // 3s
      toastr.error("Estas despachando un Producto con Precio = CERO. Verifica e intentalo de Nuevo.");
      $('#buscando').focus();
      return;
    }
    if ($("#cantpedido").val() <= 0) {
      toastr.remove();
      toastr.options.timeOut = 4000; // 3s
      toastr.error("Agregale una CANTIDAD MAYOR A CERO a este Producto");
      $("#cantpedido").focus();
      return;
    }
    if ($("#precio_prov").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Precio Proveedor no puede estar vacio\n");
      $("#precio_prov").focus();
      return;
    }
    if ($("#precioDetal").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Precio Detal no puede estar vacio\n");
      $("#precioDetal").focus();
      return;
    }
    if ($("#categoria").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Categoria no puede estar vacio\n");
      $("#categoria").focus();
      return;
    }
    if ($("#subcategoria").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Subcategoria no puede estar vacio\n");
      $("#subcategoria").focus();
      return;
    }
    // recibo los datos y los convierto en variables para pasarlos a php
    var seguro = '<?php echo $this->security->get_csrf_hash(); ?>';
    var idprod = $('#idproducto').val();
    var codpr = $('#codprod').val();
    var codpv = $('#codbarprod').val();
    var tipop = $('#tipo_prod').val(); // Valor del atributo data-*
    var descr = $('#descriprod').val();
    var cantp = $('#cantpedido').val();
    var pprov = $.number($('#precio_prov').val(), 2, '.', '');
    var pdetal = $.number($('#precioDetal').val(), 2, '.', '');
    var pmayor = $.number($('#precioMayor').val(), 2, '.', '');
    //
    var idcat = $('#idcategoria').val();
    var categ = $('#categoria').val();
    var scatg = $('#subcategoria').val();
    var exiva = $('#exento_iva').val();
    var opciones = {
      codpr: codpr,
      codpv: codpv,
      tipop: tipop,
      tprice: tprice,
      pprov: pprov,
      pdetal: pdetal,
      idcat: idcat,
      categ: categ,
      scatg: scatg,
      exiva: exiva
    }
    var dataString = {
      idprod: idprod,
      cantp: cantp,
      price: price,
      descr: descr,
      opciones: opciones,
      token: seguro
    };
    $.ajax({
      url: "<?php echo base_url() ?>ventas/agregar_art_cart_desc",
      type: "POST",
      data: dataString,
      success: function(data) {
        //limpio los campos de donde envie los datos a ser guardados
        $('#idproducto').val('');
        $('#codprod').val('');
        $('#codbarprod').val('');
        $('#tipo_prod').val('');
        $('#descriprod').val('');
        $('#cantpedido').val('');
        $('#precio_prov').val('');
        $('#precioDetal').val('');
        $('#idcategoria').val('');
        $('#categoria').val('');
        $('#subcategoria').val('');
        $('#exento_iva').val('');
        // le aviso al usuario que ya se registrò el articulo
        toastr.options.timeOut = 4000; // 3s
        toastr.success("Agregando Articulo..!");
        // muestro otra vez la lista de datos
        fn_buscar();
        setTimeout(() => {
          fn_calcular_descuento();
        }, 200);
        $("#descriprod").focus();
        return;
      },
      error: function(err) {
        toastr.options.timeOut = 4000; // 3s
        toastr.error('agregar_art: ' + JSON.stringify(err['statusText']));
      }
    });
  });
</script>
<script>
  // cuando haga clic en el boton borrar
  $('#ventanaBorrar').on('show.bs.modal', function(event) {
    var seguro = '<?php echo $this->security->get_csrf_hash(); ?>';
    var button = $(event.relatedTarget) // Button that triggered the modal
    var idart = button.data('borrar');
    var dataBorra = {
      idart: idart,
      token: seguro
    };

    $.ajax({
      url: "<?php echo base_url() ?>ventas/borrar_articulo_desc",
      type: "POST",
      data: dataBorra,
      success: function(data) {
        $("#ventanaBorrar").modal("hide");
        fn_buscar();
      },
      error: function(err) {
        toastr.options.timeOut = 4000; // 3s
        toastr.error('ventanaBorrar: ' + JSON.stringify(err['statusText']));
      }
    });
  });
</script>
<script>
  function fn_funciones() {
    fn_buscar();
    fn_calcular_descuento();
  }
  // para cargar el div_listar cuando cargue la pagina
  if (document.addEventListener) {
    window.addEventListener('load', fn_funciones, false);
  } else {
    window.attachEvent('onload', fn_funciones);
  }
</script>
<script type="text/javascript">
  // Evitar que al presionar enter se envie el formulario
  $(document).on('keydown', function(e) {

    // Set self as the current item in focus
    var self = $(':focus'),
      // Set the form by the current item in focus
      form = self.parents('form:eq(0)'),
      focusable;

    // Array of Indexable/Tab-able items
    focusable = form.find('input,a,select,button,textarea').filter(':visible');

    function enterKey() {
      if (e.which === 13 && !self.is('textarea')) { // [Enter] key

        // If not a regular hyperlink/button/textarea
        if ($.inArray(self, focusable) && (!self.is('a')) && (!self.is('button'))) {
          // Then prevent the default [Enter] key behaviour from submitting the form
          e.preventDefault();
        } // Otherwise follow the link/button as by design, or put new line in textarea

        // Focus on the next item (either previous or next depending on shift)
        focusable.eq(focusable.index(self) + (e.shiftKey ? -1 : 1)).focus();

        return false;
      }
    }
    // We need to capture the [Shift] key and check the [Enter] key either way.
    if (e.shiftKey) {
      enterKey()
    } else {
      enterKey()
    }
  });
</script>
<script>
  $(document).bind('keydown', function(e) {
    if (e.which == 27) {
      alert('Si deseas salir de la Orden, debes presionar el Boton "Borrar Orden"');
    };
  });
</script>
<!--Autonumeric-->
<script src="<?php echo base_url() ?>assets/libs/autonumeric/dist/autoNumeric.min.js"></script>
<script>
  $(document).ready(function() {
    //
    var autoNumericInstance = new AutoNumeric('#cantpedido', {
      allowDecimalPadding: AutoNumeric.options.allowDecimalPadding.floats,
      alwaysAllowDecimalCharacter: AutoNumeric.options.alwaysAllowDecimalCharacter.alwaysAllow,
      caretPositionOnFocus: AutoNumeric.options.caretPositionOnFocus.start,
      decimalPlaces: AutoNumeric.options.decimalPlaces.three,
      leadingZero: AutoNumeric.options.leadingZero.allow
    });

    $('.decimal-3-places').on('keyup', function() {
      $('#cantpedido').val(autoNumericInstance.getNumericString());
    });
  });
</script>
<script>
  // Campo numerico
  $('.decimal-2-places').number(true, 2, '.', ',');
  $('.positive-integer').number(true, 0, ',', '.');
</script>
<script>
  // cambiando la mascara segun la seleccion de nacionalidad
  $('#ccedula').inputmask('99999999-9', {
    'placeholder': ''
  });
  $('#nac').on('change', function() {
    var nac = $('#nac').val();
    if (nac == 'J' || nac == 'G' || nac == 'P') {

      $('#ccedula').inputmask('99999999-9', {
        'placeholder': ''
      });

    } else if (nac == 'V' || nac == 'E') {

      $('#ccedula').inputmask(["9.999.999", "99.999.999"], {
        'placeholder': ''
      });

    }
  });
  //Mascara de Telefono
  //$('[data-mask-tel]').inputmask({ regexp: "/^\b\d{(4)}[ ]?\d{3}[-]?\d{2}[-]?\d{2}\b/" });
  $('[data-mask-tel]').inputmask('9999 999-99-99');
  //Date range as a button
  $("#fventa").datepicker({
    format: 'dd-mm-yyyy',
    autoclose: true,
    language: 'es'
  }).on('hide, blur', function() {
    $("#apenombre").focus();
  });
</script>
<!-- Para la validacion -->
<script src="<?php echo base_url() ?>assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo base_url() ?>assets/libs/jquery-validation/dist/additional-methods.js"></script>
<script>
  $(document).ready(function() {
    // para darle el foco al primer input editable
    $("#apenombre").focus();
    // en caso contrario, valido todos los otros campos
    $("#form-registro").validate({
      rules: {
        tipo_venta: "required",
        descuento: "required",
        nac: "required",
        ccedula: {
          required: true,
          pattern: /^[0-9.-]+$/,
          minlength: 9
        },
        fventa: "required",
        apenombre: "required",
        ecorreo: {
          required: true,
          email: true,
          remote: {
            param: {
              url: "/clientes/consultmail",
              type: "post",
              async: true
            },
            depends: function() {
              if ($('#idnrocli').val() === '') {
                return true;
              } else {
                return false;
              }
            },
          },
        },
        dircli: "required",
        tlfnocel: "required",
        tlfnocasa: "required",
        idvend: "required",
      },
      messages: {
        tipo_venta: "Requerido",
        descuento: "Requerido",
        nac: "Requerida",
        ccedula: {
          required: "Cedula no puede ir vacio",
        },
        fventa: "Fecha de Venta es requerida",
        apenombre: "Nombre no puede ir vacio",
        ecorreo: {
          required: "Email no puede ir vacio",
          email: "Escriba un email valido",
          remote: "Este Email YA existe"
        },
        dircli: "Direccion no puede ir vacio",
        tlfnocel: "Celular no puede ir vacio",
        tlfnocasa: "Tlfno Casa no puede ir vacio",
        idvend: "No puede ir vacio",
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        // Add the `invalid-feedback` class to the error element
        error.addClass("invalid-feedback");

        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      }
    });
    $.validator.methods.email = function(value, element) {
      //return this.optional( element ) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@(?:\S{1,63})$/.test( value );
      return this.optional(element) || /^(("[\w-\s]+")|([\w-].+(?:\._[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\._[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-zA-Z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/.test(value);
    };
  });
</script>
<script src="<?php echo base_url() ?>assets/libs/jquery.redirect/jquery.redirect.js"></script>
<script>
  /* SI EL USUARIO PRESIONA EL BOTON "REGISTRAR" SE REALIZA PRIMERO UNA COMPROBACION
   * SOLO PARA SABER SI NO SE ENVIAN DATOS VACIOS */
  $(document).on('click', "#registrar", function(e) {
    // VERIFICO LOS DATOS ANTES DE ENVIAR POR SI HAY DATOS VACIOS
    if ($("#fventa").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Coloque una fecha a la compra");
      $("#fventa").focus();
      return;
    }
    if ($("#tipo_venta").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Este Producto NO puede agregarse si no selecciona el Tipo de Venta");
      $("#tipo_venta").focus();
      return;
    }
    if ($("#idvend").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Este Producto NO puede agregarse si no se le asigna un Vendedor");
      $("#idvend").focus();
      return;
    }
    if ($("#condcomercio").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Este Producto NO puede agregarse si no se selecciona la fecha de Vencimiento del Credito (Cobrar en:)");
      $("#condcomercio").focus();
      return;
    }
    if ($("#nac").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Esta Venta/Producto NO puede agregarse si no hay un cliente al cual agregarselo");
      $("#nac").focus();
      return;
    }
    if ($("#ccedula").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Esta Venta/Producto NO puede agregarse si no hay un cliente al cual agregarselo");
      $("#ccedula").focus();
      return;
    }
    if ($("#apenombre").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Esta Venta/Producto NO puede agregarse si no hay un cliente al cual agregarselo");
      $("#apenombre").focus();
      return;
    }
    if ($("#ecorreo").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Esta Venta/Producto NO puede agregarse si no hay un cliente al cual agregarselo");
      $("#ecorreo").focus();
      return;
    }
    if ($("#dircli").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Esta Venta/Producto NO puede agregarse si no hay un cliente al cual agregarselo");
      $("#dircli").focus();
      return;
    }
    if ($("#tlfnocel").val().length < 1) {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Esta Venta/Producto NO puede agregarse si no hay un cliente al cual agregarselo");
      $("#tlfnocel").focus();
      return;
    }
    // 
    var total_items = $('#total_items').val();
    if (total_items == 0 || total_items == "undefined" || total_items == "null" || total_items == '') {
      toastr.options.timeOut = 4000; // 3s
      toastr.warning("Este Descargo no se puede crear hasta que se inserte un ITEM nuevo o registre uno venido de la base de datos de productos");
      $("#descriprod").focus();
      //console.log(total_items);
      $("#form-registro").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
      });
    } else {
      // inactivar el boton de envio
      $('#registrar').css("pointer-events", "none");
      $('#registrar').prop('disabled', true);
      // parar el envio de cualquier cosa mientras se esta enviando los datos escritos en el envio
      $("#form-registro").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
      });
      // preparar los datos para enviarlos via POST a travez de Jquery.redirect
      var seguro = '<?php echo $this->security->get_csrf_hash(); ?>';
      var fventa = $('#fventa').val();
      var tipo_venta = $('#tipo_venta').val();
      var idvend = $('#idvend').val();
      var condcomercio = $('#condcomercio').val();
      var descuento = $('#descuento').val();
      var pctcomisionvend = $('#pctcomisionvend').val();
      var precio_venta = $('#precio_venta').val();
      var nac = $('#nac').val();
      var ccedula = $('#ccedula').val();
      var idnrocli = $('#idnrocli').val();
      var apenombre = $('#apenombre').val();
      var ecorreo = $('#ecorreo').val();
      var dircli = $('#dircli').val();
      var tlfnocel = $('#tlfnocel').val();
      var tlfnocasa = $('#tlfnocasa').val();
      var observacion = $('#observacion').val();
      // console.log(JSON.stringify(dataString));
      $.redirect("/ventas/insertar_descargo", {
        fventa: fventa,
        tipo_venta: tipo_venta,
        idvend: idvend,
        condcomercio: condcomercio,
        descuento: descuento,
        pctcomisionvend: pctcomisionvend,
        precio_venta: precio_venta,
        nac: nac,
        ccedula: ccedula,
        idnrocli: idnrocli,
        apenombre: apenombre,
        ecorreo: ecorreo,
        dircli: dircli,
        tlfnocel: tlfnocel,
        tlfnocasa: tlfnocasa,
        observacion: observacion,
        token: seguro
      }, "POST");
      // le aviso al usuario que ya se registrò el articulo
      toastr.options.timeOut = 3000; // 3s
      toastr.info("Grabando..!");
    }
  });

  // ENVIANDO UN MENSAJE AL USUARIO DE CANCELANDO LA ORDEN SI YA NO QUIERO HACERLA
  $(document).on('click', "#cancelar", function() {
    toastr.options.timeOut = 3000; // 3s
    toastr.error("Cancelando Orden..!");
  });
</script>