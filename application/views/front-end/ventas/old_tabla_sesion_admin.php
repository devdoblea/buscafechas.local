<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid d-flex h-100 flex-column">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-heading">
					<?php echo form_open(base_url().'ventas/validar_sesion','class="formulario"') ?>
					<div class="row">
  					<div class="col-12 form-inline">
              <div class="col-md-6 col-sm-12 col-xs-12 input-group mb-3 mt-3 float-left">
								<input type="text" class="form-control" id="buscando" name="buscando" value="<?php echo set_value('buscando')?>" placeholder="Buscar por nro de factura..." accept-charset="utf-8" placeholder="Introducir búsqueda" autocomplete="off" autocapitalize="off" autocorrect="off" spellcheck="false" maxlength="45">
                <input type="hidden" name="idsess" value="<?php echo $idsess;?>">
                <input type="hidden" name="nroses" value="<?php echo $nroses;?>">
                <input type="hidden" name="fecini" value="<?php echo $fecini;?>">
                <input type="hidden" name="fecfin" value="<?php echo $fecfin;?>">
                <input type="hidden" name="iduser" value="<?php echo $iduser;?>">
								<div class="input-group-append">
									<button class="btn btn-danger" type="submit"><i class="ti-search"></i></button>
								</div>
							</div>
              <div class="col-md-6 col-sm-12 col-xs-12 mt-3">
                <!--  -->
                <a href="/ventas/dashboard" class="btn btn-outline-secondary col-md-4 col-sm-12 col-xs-12 float-right margin-5" id="volver">Volver</a>
              </div>
  						<div style="color:red"><?php echo validation_errors(); ?></div>
            </div>
		  		</div>
					<?php echo form_close() ?>
				</div>
			</div>
		</div>
	</div>
  <!--  -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<?php
				  if(!$resultados) :
  					echo '
  						<div class="card-body border-top height-box">
  							<div class="alert alert-danger" role="alert">
    						  <h4 class="alert-heading">No Hay Resultados</h4>
    						  <p>Es posible que aùn no se haya registrado una Venta.</p>
    						  <hr>
    						  <p class="mb-0"><strong>Por favor verifica y en caso que creas que esto no es correcto, llama al Administrador/Desarrollador del Sistema</strong></p>
    						</div>
              </div>
  					';
		   		else :?>
            <!--  -->
            <div class="row">
              <div class="col-12 form-inline">
                <div class="col-md-5 col-sm-12 col-xs-12 text-center" style="padding: 1.5rem 1.5rem;">
                  <table>
                    <tbody>
                      <tr style="vertical-align:middle;">
                        <td colspan="2" class="text-left pl-2">
                          <h4 class="card-title">SESIÓN POS/<?php echo date('Y/m/d',strtotime($fecini)).'/'.$nroses;?></h4>
                        </td>
                      </tr>
                      <tr style="vertical-align:middle;">
                        <td class="text-right">
                          <h5 class="card-title">Responsable:</h5>
                        </td>
                        <td class="text-left pl-3">
                          <h5 class="card-title"><?php echo $responsable;?></h5>
                        </td>
                      </tr>
                      <tr style="vertical-align:middle;">
                        <td class="text-right">
                          <h6 class="card-title">Fecha Apertura:</h6>
                        </td>
                        <td class="text-left pl-3">
                          <h6 class="card-title"><?php echo fecha_larga_sin_dia_semana(date('d-m-Y',strtotime($fecini))).' '.date('h:i:s a',strtotime($fecini));?></h6>
                        </td>
                      </tr>
                      <tr style="vertical-align:middle;">
                        <td class="text-right">
                          <h6 class="card-title">Fecha Cierre:</h6>
                        </td>
                        <td class="text-left pl-3">
                          <h6 class="card-title"> <?php echo fecha_larga_sin_dia_semana(date('d-m-Y',strtotime($fecfin))).' '.date('h:i:s a',strtotime($fecfin));?></h6>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="col-md-7 col-sm-12 col-xs-12 text-center" style="padding: 1.5rem 1.5rem;">
                  <!-- TABLA DE FORMAS DE PAGO  -->
                  <table id="tabla_totfpag" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th style="width: 12vw;"><label class="card-title label-resume">Forma de Pago </label></th>
                        <th style="width: 6vw;" ><label class="card-title label-resume">Entregado     </label></th>
                        <th style="width: 6vw;" ><label class="card-title label-resume">Tasa Dolar    </label></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        if( $tot_formpa != 'nada') :
                          foreach ($tot_formpa as $k) : 
                            if( strpos( $k->formapagoval, 'USD') !== false) :
                              $simb_moneda = '$';
                            else : 
                              $simb_moneda = $empresa->simbol_moneda;
                            endif;?>
                            <tr>
                              <td class="text-right align-middle">
                                <label class="card-title label-resume"><?php echo $k->formapagoval;?></label>
                              </td>
                              <td class="text-center align-middle">
                                <h3 class="card-title">
                                  <?php echo number_format($k->entregado,2,'.',',').' <small>'.$simb_moneda.'</small>';?>
                                </h3>
                              </td>
                              <td class="text-center align-middle">
                                <h3 class="card-title">
                                  <?php 
                                    if( strpos( $k->formapagoval, 'USD') !== false) :
                                      echo number_format($k->tasaDolar,2,'.',',').'<small>(Bs x $)</small>';
                                    else : 
                                      echo 'N/A';
                                    endif;
                                  ?>
                                </h3>
                              </td>
                            </tr><?php 
                          endforeach;
                        else :
                          echo '
                            <div class="card-body border-top height-box">
                              <div class="alert alert-danger" role="alert">
                                <h4 class="alert-heading">ERROR: No Hay Formas de Pago Registradas</h4>
                                <p class="mb-0"><strong>Por favor verifica y en caso que creas que esto no es correcto, llama al Administrador/Desarrollador del Sistema</strong></p>
                              </div>
                            </div>
                          ';
                        endif;
                      ?>
                    </tbody>
                  </table>
                  <!-- TABLA DE GANANCIAS -->
                  <table id="tabla_totales" class="table table-striped table-bordered">
                    <tbody>
                      <tr>
                        <td class="text-right" style="width: 12vw;vertical-align: middle;">
                          <label class="card-title label-resume">Total Crédito:</label>
                        </td>
                        <td class="text-center" colspan="2" style="width: 6vw;vertical-align: middle;">
                          <h3 class="card-title">
                            <?php echo $empresa->simbol_moneda.' '.number_format($tot_creses,2,'.',',');?>
                          </h3>
                          <small>Ventas realizadas a CRÉDITO de esta sesión</small>
                        </td>
                      </tr>
                      <tr>
                        <td class="text-right" style="width: 12vw;vertical-align: middle;">
                          <label class="card-title label-resume">Total Contado:</label>
                        </td>
                        <td class="text-center" colspan="2" style="width: 6vw;vertical-align: middle;">
                          <h3 class="card-title">
                            <?php echo $empresa->simbol_moneda.' '.number_format($tot_conses,2,'.',',');?>
                          </h3>
                          <small>Ventas realizadas a CONTADO de esta sesión</small>
                        </td>
                      </tr>
                      <tr>
                        <td class="text-right" style="width: 12vw;vertical-align: middle;">
                          <label class="card-title label-resume">Total Sesión:</label>
                        </td>
                        <td class="text-center" colspan="2" style="width: 6vw;vertical-align: middle;">
                          <h3 class="card-title">
                            <?php //echo $empresa->simbol_moneda.' '.number_format($this->session->userdata('tot_sesion'),2,'.',',');?>
                            <?php echo $empresa->simbol_moneda.' '.number_format($tot_sesion,2,'.',',');?>
                          </h3>
                          <small>Tasa Promedio del Sistema para este Dia: <strong><?php echo number_format($promTasa,2,'.',',');?></strong></small>
                        </td>
                      </tr>
                      <tr>
                        <td class="text-right" style="vertical-align:middle;">
                          <label class="card-title label-resume">Ganancia:</label>
                        </td>
                        <td class="text-center" style="vertical-align:middle;">
                          <h3 class="card-title">
                            <?php echo $empresa->simbol_moneda.' '.number_format($gain * $promTasa,2,'.',',');?>
                          </h3>
                        </td>
                        <td class="text-center" style="vertical-align:middle;">
                          <h3 class="card-title">
                            $ <?php echo number_format($gain,2,'.','.')?>
                          </h3>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!--  -->
            <div class="row">
              <div class="col-12">
                <div class="col-md-12 col-sm-12 col-xs-12" style="padding: 1.5rem 1.5rem;">
        					<div class="table-responsive">
        						<table id="zero_config" class="table table-striped table-bordered">
                      <thead>
                          <tr style="vertical-align:middle;">
                            <!-- <th class="text-center">Id.</th> -->
                            <th class="text-center">Hora</th>
                            <th class="text-center">Nro Factura</th>
                            <th class="text-center">Cliente</th>
                            <th class="text-center" style="width: 6vw;">Cant. Prod.</th>
                            <th class="text-center">I.V.A</th>
                            <th class="text-center">Total Fact. Bs.</th>
                            <th class="text-center">Total Fact. $.</th>
                            <th class="text-center">Tasa Dolar.</th>
                            <th class="text-center" style="width: 6vw;">Metodos de Pago</th>
                            <th class="text-center" style="width: 6vw;">Productos Vendidos</th>
                            <!-- <th class="text-center" style="width: 6vw;">ReImprime Factura</th> -->
                          </tr>
                      </thead>
                      <tbody>
                        <?php
                          $subtdolar = 0;
                          $subtboliv = 0;
                          foreach ( $resultados as $r ) :
                            $cd = $r->nac;
                            if ($cd != 'V' && $cd != 'E') : $cedula = $r->nac.'-'.$r->ccedula;
                            else : $cedula = $r->nac.'-'.number_format($r->ccedula,0,',','.');
                            endif;
                            echo '<tr>';
                            // echo '<td style="text-align:center; vertical-align:middle;">'.$r->idfgen.'</td>';
                            echo '<td style="text-align:center; vertical-align:middle;">'.date('h:i:s a',strtotime($r->timestamp)).'</td>';
                            echo '<td style="text-align:center; vertical-align:middle;">'.$r->nrofact.'</td>';
                            echo '<td style="text-align:left;   vertical-align:middle;">'.$cedula.' '.$r->apenombre.'</td>';
                            echo '<td style="text-align:center; vertical-align:middle;">'.$r->cant_prod.'</td>';
                            echo '<td style="text-align:center; vertical-align:middle;">'.number_format($r->ivafact,2,'.',',').'</td>';
                            echo '<td style="text-align:center; vertical-align:middle;">'.number_format($r->totalfact,2,'.',',').'</td>';
                            echo '<td style="text-align:center; vertical-align:middle;">'.number_format($r->totusd,2,'.',',').'</td>';
                            echo '<td style="text-align:center; vertical-align:middle;">'.number_format($r->tasaDolar,2,'.',',').'</td>';
                            echo '<td style="text-align:center; vertical-align:middle;">
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#ventanaConsultaPag" data-nrofac="'.$r->nrofact.'" title="Consulta cómo se pagó esta factura"><i class="fa fa-search "></i></button></td>';
                            echo '<td style="text-align:center; vertical-align:middle;">
                                    <button class="btn btn-info" data-toggle="modal" data-target="#ventanaConsultaProd" data-nrofac="'.$r->nrofact.'" title="Consulta cuales Productos se vendieron en esta factura"><i class="mdi mdi-receipt"></i></button></td>';
                            // echo '<td style="text-align:center; vertical-align:middle;">
                                    // <button class="btn btn-danger reimpfact" id="reimpfact" name="reimpfact" class="btn btn-secondary" data-idfgen="'.$r->idfgen.'" title="Imprime esta factura"><i class="mdi mdi-receipt"></i></a></td>';
                            // recopilando subtotales
                            $subtdolar += $r->totusd;
                            $subtboliv += $r->totalfact;
                            echo '</tr>';
                          endforeach;
                        ?>
                      </tbody>
        							<tfoot>
                        <?php
                          echo '<tr>';
                          echo '<td colspan="5"></td>';
                            echo '<td style="text-align:center; vertical-align:middle;">'.number_format($subtboliv,2,'.',',').'</td>';
                            echo '<td style="text-align:center; vertical-align:middle;">'.number_format($subtdolar,2,'.',',').'</td>';
                          echo '</tr>';
                        ?>
        							</tfoot>
        						</table>
        						<?php // para mostrar los enlaces de la paginacion
                      echo '
                      <div class="col-md-12"><nav class="float-right" aria-label="..."> '.$this->pagination->create_links().'</nav></div>';
                      //$this->session->unset_userdata('buscando');$this->session->unset_userdata('nroses');$this->session->unset_userdata('fecini');$this->session->unset_userdata('fecfin');?>
        					</div>
                </div>
              </div>
            </div>
            <!--  -->
    				<?php 
          endif;
        ?>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- Con este grupo de divs se abre la ventana modal que traera la info a agregar estud-->
<div class="modal fade in" id="ventanaConsultaPag" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="ct">
            </div>
        </div>
    </div>
</div>
<!-- Con este grupo de divs se abre la ventana modal que traera la info a agregar estud-->
<div class="modal fade in" id="ventanaConsultaProd" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="ct">
            </div>
        </div>
    </div>
</div>
<script>// para acomodar las tablas en monitores de menos de 1024px
  $('#zero_config').stacktable();
</script>
<script src="<?php echo base_url()?>assets/libs/jquery.redirect/jquery.redirect.js"></script>
<script>
    $('.reimpfact').on('click', function(){
     //
     var seguro = '<?php echo $this->security->get_csrf_hash();?>';
     var idfgen = this.getAttribute("data-idfgen");
     //alert('codped: '+codped+' - '+'emailc: '+emailc);
     $.redirect("/ventas/reimprime_factura", { idfgen: idfgen, token:seguro }, "POST");
    });
</script>
<script>
  $('#ventanaConsultaPag').on('show.bs.modal', function(event) {
    var seguro = '<?php echo $this->security->get_csrf_hash();?>';
    var button = $(event.relatedTarget) // Button that triggered the modal
    var nrofac = button.data('nrofac'); // Extract info from data-* attributes
    var modal = $(this);
    var dataString = { nrofac:nrofac, token:seguro };
      $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>ventas/get_formas_pago_by_factura_admin",
          data: dataString,
          success: function (data) {
              //console.log(data);
              modal.find('.ct').html(data);
          },
          error: function(err) {
            toastr.options.timeOut = 4000; // 3s
            toastr.error('ventanaEditarCli: '+JSON.stringify(err['statusText']));
          }
      });
  });
  // Consultar los productos
  $('#ventanaConsultaProd').on('show.bs.modal', function(event) {
    var seguro = '<?php echo $this->security->get_csrf_hash();?>';
    var button = $(event.relatedTarget) // Button that triggered the modal
    var nrofac = button.data('nrofac'); // Extract info from data-* attributes
    var modal = $(this);
    var dataString = { nrofac:nrofac, token:seguro };
      $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>ventas/get_consulta_productos_sesion_admin",
          data: dataString,
          success: function (data) {
              //console.log(data);
              modal.find('.ct').html(data);
          },
          error: function(err) {
            toastr.options.timeOut = 4000; // 3s
            toastr.error('ventanaConsultaProd: '+JSON.stringify(err['statusText']));
          }
      });
  });
</script>
<!-- Con este grupo de divs se abre la ventana modal que traera la info a editar del estudiante-->
<div class="modal fade bs-example-modal-lg" id="ventanaDevolverProd" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content ctdev">
    </div>
  </div>
</div>
<script>
  // PARA DEVOLVER UN PRODUCTO DE UNA FACTURA
  $('#ventanaDevolverProd').on('show.bs.modal', function (event) {
    var seguro = '<?php echo $this->security->get_csrf_hash();?>';
    var button = $(event.relatedTarget); // Button that triggered the modal
    var idfact = button.data('idfact'); // Extract info from data-* attributes
    var desc = button.data('desc'); // Extract info from data-* attributes
    var cant = button.data('cant'); // Extract info from data-* attributes
    var pvpd = button.data('pvpd'); // Extract info from data-* attributes
    var idinv = button.data('idinv'); // Extract info from data-* attributes
    var nfact = button.data('nfact'); // Extract info from data-* attributes
    var tasa = button.data('tasa'); // Extract info from data-* attributes
    var modal = $(this);
    var dataString = { idfact:idfact, desc:desc, cant:cant, pvpd:pvpd, idinv:idinv, nfact:nfact, tasa:tasa, token:seguro };
    $.ajax({
      type: 'POST',
      url: '<?php echo base_url()?>ventas/devolver_prod',
      data: dataString,
      cache: false,
        beforesend: function(data) {
          //console.log(data);
          modal.find('.ct').hide();
        },
        success: function(data) {
          //console.log(data);
          modal.find('.ctdev').html(data);
        },
        error: function(err) {
          toastr.options.timeOut = 4000; // 3s
          toastr.error('ventanaDevolverProd: '+JSON.stringify(err['statusText']));
        }
    });
  });
</script>



