<!DOCTYPE html>
<html dir="ltr">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="Angel Emiro Antunez Villasmil @Devdoblea">
	<!-- Favicon icon -->
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/images/favicon.png">
	<title>Inconet Systems | Logueate</title>
	<!-- Custom CSS -->
	<link href="<?php echo base_url()?>assets/dist/css/style.min.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/dist/css/icons/font-awesome/css/fontawesome-all.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/dist/css/icons/themify-icons/themify-icons.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/dist/css/icons/material-design-iconic-font/css/materialdesignicons.min.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body oncontextmenu="return false">
	<div class="main-wrapper">
		<!-- ============================================================== -->
		<!-- Preloader - style you can find in spinners.css -->
		<!-- ============================================================== -->
		<div class="preloader">
			<div class="lds-ripple">
				<div class="lds-pos"></div>
				<div class="lds-pos"></div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- Preloader - style you can find in spinners.css -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Login box.scss -->
		<!-- ============================================================== -->
		<div class="auth-wrapper d-flex no-block justify-content-center align-items-center bg-dark">
			<div class="auth-box bg-dark border-top border-secondary">

				<div id="recoverform" style="display: inherit !important;">
					<div class="text-center">
						<span class="text-white">Introduzca su correo eléctronico y nosotros le enviaremos las instrucciones para recuperar su contraseña.</span>
					</div>
					<div class="row m-t-20">
						<!-- Form -->
						<?php echo form_open("auth/forgot_password", 'class="col-12"');?>
							<!-- email -->
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-email"></i></span>
								</div>
								<?php echo form_input($identity);?>
								<!--<input type="text" class="form-control form-control-lg" placeholder="Email Address" aria-label="Username" aria-describedby="basic-addon1">-->
							</div>
							<?php
								if(isset($message)) {
									echo '
										<div class="alert alert-danger" role="alert">
										  '.$message.'
										</div>
									';
								}
							?>
							<!-- pwd -->
							<div class="row m-t-20 p-t-20 border-top border-secondary">
								<div class="col-12">
									<a class="btn btn-success" href="/auth/login" id="to-login" name="action">Regresar al Login</a>
									<button class="btn btn-info float-right" type="submit" name="action">Enviar</button>
								</div>
							</div>
						<?php echo form_close();?>

					</div>
				</div>

			</div>
		</div>
		<!-- ============================================================== -->
		<!-- Login box.scss -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Page wrapper scss in scafholding.scss -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Page wrapper scss in scafholding.scss -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Right Sidebar -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Right Sidebar -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- All Required js -->
	<!-- ============================================================== -->
	<!-- JQUERY JavaScript -->
	<script src="<?php echo base_url()?>assets/libs/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap tether Core JavaScript -->
	<script src="<?php echo base_url()?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
	<script src="<?php echo base_url()?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- ============================================================== -->
	<!-- This page plugin js -->
	<!-- ============================================================== -->
	<script>

		$('[data-toggle="tooltip"]').tooltip();
		$(".preloader").fadeOut();
		// ============================================================== 
		// Login and Recover Password 
		// ============================================================== 
		$('#to-recover').on("click", function() {
			$("#loginform").slideUp();
			$("#recoverform").fadeIn();
		});
		$('#to-login').click(function(){
			
			$("#recoverform").hide();
			$("#loginform").fadeIn();
		});
	</script>

</body>

</html>
<!-- <h1><?php //echo lang('forgot_password_heading');?></h1>
<p><?php //echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>

<div id="infoMessage"><?php //echo $message;?></div>

<?php //echo form_open("auth/forgot_password");?>

      <p>
      	<label for="identity"><?php //echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label> <br />
      	<?php //echo form_input($identity);?>
      </p>

      <p><?php //echo form_submit('submit', lang('forgot_password_submit_btn'));?></p>

<?php //echo form_close();?> -->
