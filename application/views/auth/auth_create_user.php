<div class="modal-header bg-success"style="color:#FFFFFF">
  <h5 class="modal-title" id="exampleModalLabel">Crear Usuario</h5>
  <small class="mt-2 ml-4">Por favor introduzca la nueva información del usuario.</small>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
  <form id="form-registro" action="<?php echo base_url()?>usuarios/create_user" method="post" >
    <h5>Perfil de Cliente:</h5>
    <hr color ="green">
    <div class="row mb-3">
      <div class="col-md-4">
        <div class="form-group">
          <label for="nac">Nac - Cédula/RIF: </label> <i class="mdi mdi-asterisk"></i>
          <div class="input-group">
            <select id="nac" name="nac" class="form-control col-md-3" placeholder="Seleccione una Opcion">
              <option value="" ></option>
              <option value="V" > V </option>
              <option value="E" > E </option>
              <option value="J" > J </option>
              <option value="G" > G </option>
              <option value="P" > P </option>
            </select>
            <input type="text" name="ccedula" id="ccedula" class="form-control text-center positive-integer" maxlength="14" autofocus="on" autocomplete="off" placeholder="Nro de Cedula"/>
            <input type="hidden" name="token" value="<?php echo $this->security->get_csrf_hash();?>">
          </div>
        </div>
      </div>
      <div class="col-md-8">
        <label for="cliente">Nombre del Cliente: </label> <i class="mdi mdi-asterisk"></i>
        <input type="text" name="cliente" id="cliente" class="form-control" maxlength="50" autocomplete="off" placeholder="Nombres y Apellidos" onchange="javascript:this.value=this.value.toUpperCase();" />
      </div>
    </div>
    <div class="row mb-3">
      <div class="col-md-6">
        <label for="tlfnocel">Telefono Celular: </label> <i class="mdi mdi-asterisk"></i>
        <input type="text" name="tlfnocel" id="tlfnocel" class="form-control text-center" maxlength="16" autocomplete="off" data-mask-tel />
      </div>
      <div class="col-md-6">
        <label for="tlfnocasa">Telefono Casa: </label> <i class="mdi mdi-asterisk"></i>
        <input type="text" name="tlfnocasa" id="tlfnocasa" class="form-control text-center" maxlength="16" autocomplete="off" data-mask-tel />
      </div>
    </div>
    <h5>Perfil de Usuario:</h5>
    <hr color ="green">
    <div class="row mb-3">
      <div class="col-md-6">
        <div class="col-md-12">
          <label for="email">Nombre Usuario: </label> <i class="mdi mdi-asterisk"></i>
          <input type="email" name="email" id="email" class="form-control" maxlength="50" autocomplete="off" placeholder="Coloque un correo valido" autocomplete="off" />
        </div>
        <div class="row mb-3 m-3">
          <label class="col-md-8">Asignar al Grupo:</label>
          <div class="col-md-6">
            <div class="custom-control custom-checkbox mr-sm-2">
              <input type="checkbox" name="groups[]" id="customControlAutosizing1" class="custom-control-input" value="1" checked="checked">
              <label for="customControlAutosizing1" class="custom-control-label">Admin</label>
            </div>
          </div>
          <div class="col-md-6">
            <!-- <div class="custom-control custom-checkbox mr-sm-2">
              <input type="checkbox" name="groups[]" id="customControlAutosizing2" class="custom-control-input" value="2">
              <label for="customControlAutosizing2" class="custom-control-label">Cajeros</label>
            </div> -->
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="col-md-8">
          <label for="observ">Password: </label>
          <input type="text" name="password" id="password" minlength="8" class="form-control"/>
        </div>
        <div class="col-md-8">
          <label for="observ">Confirmar Password: </label>
          <input type="text" name="password_confirm" id="password_confirm" minlength="8" class="form-control"/>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      <button type="submit" name="modificar" class="btn btn-primary" value="Modificar">Registrar Datos</button>
    </div>
  </form>
</div>

<script>
  // Campo numerico
  $('.decimal-2-places').number( true, 2, ',', '.' );
  $('.positive-integer').number( true, 0, ',', '.' );
</script>
<script>
   //Mascara de Telefono
  $("[data-mask-tel]").inputmask("9999 999-99-99");
</script>
<!-- Para la validacion -->
<script src="<?php echo base_url()?>assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo base_url()?>assets/libs/jquery-validation/dist/additional-methods.js"></script>
<script>
  $( document ).ready( function () {

    $('#ccedula').focus();

    $( "#form-registro" ).validate( {
      rules: {
        nac: "required",
        ccedula: {
          required: true,
          remote: {
            url: "consultcli",
            type: "post",
            async: true
          },
          digits: true
        },
        cliente: "required",
        direccion: "required",        
        tlfnocel: "required",
        tlfnocasa: "required",
        email: {
          required: true,
          email: true,
          remote: {
            url: "consultmail",
            type: "post",            
            async: true
          },
        },
        password: {
          required: true,
          minlength: 8
        },
        password_confirm: {
          required: true,
          minlength: 8,
          equalTo: "#password"
        },
      },
      messages: {
        nac: "No puede ir vacio",
        ccedula: {
          required: "cedula es requerido",
          remote: "Cedula ya existe"
        },
        cliente: "Nombre no puede ir vacio",
        direccion: "Direccion no puede ir vacio",
        email: {
          required: "Email no puede ir vacio",
          remote: "Email ya existe"
        },
        tlfnocel: "Celular no puede ir vacio",
        tlfnocasa: "Tlfno Casa no puede ir vacio",
        password: {
          required: "Password no puede ir vacio",
          minlength: "Password debe tener al menos 8 caracteres"
        },
        password_confirm: {
          required: "Confirme el password",
          minlength: "Password debe tener al menos 8 caracteres",
          equalTo: "Escriba el password igual al campo de arriba"
        },
      },
      errorElement: "em",
      errorPlacement: function ( error, element ) {
        // Add the `invalid-feedback` class to the error element
        error.addClass( "invalid-feedback" );

        if ( element.prop( "type" ) === "checkbox" ) {
          error.insertAfter( element.next( "label" ) );
        } else {
          error.insertAfter( element );
        }
      },
      highlight: function ( element, errorClass, validClass ) {
        $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
      },
      unhighlight: function (element, errorClass, validClass) {
        $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
      }
    });
    $.validator.methods.email = function( value, element ) {
      //return this.optional( element ) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@(?:\S{1,63})$/.test( value );
      return this.optional( element ) || /^(("[\w-\s]+")|([\w-].+(?:\._[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\._[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/.test( value );
    };
  });
</script>