<div class="modal-header bg-primary"style="color:#FFFFFF">
  <h5 class="modal-title" id="exampleModalLabel">Editar Usuario</h5>
  <small class="mt-2 ml-4">Por favor introduzca la nueva información del usuario.</small>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
  <form id="form-registro" action="<?php echo base_url()?>usuarios/update_user" method="post" >
    <h5>Perfil de Cliente:</h5>
    <hr color ="green">
    <div class="row mb-3">
      <div class="col-md-4">
        <label for="nacced">Cedula / RIF: </label> <i class="mdi mdi-asterisk"></i>
        <input type="text" name="nacced" id="nacced" class="form-control text-center" maxlength="14" autofocus="on" autocomplete="off" title="Este dato no se puede modificar" placeholder="Nro de Cedula" value="<?php echo $usuario->last_name;?>" readonly/>
        <input type="hidden" name="ccedula" value="<?php echo $usuario->last_name;?>">
        <input type="hidden" name="iduser" value="<?php echo $usuario->id;?>">
        <input type="hidden" name="active" value="<?php echo $usuario->active;?>">
        <input type="hidden" name="token" value="<?php echo $this->security->get_csrf_hash();?>">
      </div>
      <div class="col-md-8">
        <label for="cliente">Nombre del Cliente: </label> <i class="mdi mdi-asterisk"></i>
        <input type="text" name="cliente" id="cliente" class="form-control" maxlength="50" autocomplete="off" placeholder="Nombres y Apellidos" onchange="javascript:this.value=this.value.toUpperCase();" value="<?php echo $usuario->first_name;?>" />
      </div>
    </div>
    <div class="row mb-3" style="">
      <div class="col-md-6">
        <label for="tlfnocel">Telefono Celular: </label> <i class="mdi mdi-asterisk"></i>
        <input type="text" name="tlfnocel" id="tlfnocel" class="form-control text-center" maxlength="16" autocomplete="off" value="<?php echo $usuario->phone;?>" data-mask-tel />
      </div>
      <div class="col-md-6">
        <label for="tlfnocasa">Telefono Casa: </label> <i class="mdi mdi-asterisk"></i>
        <input type="text" name="tlfnocasa" id="tlfnocasa" class="form-control text-center" maxlength="16" autocomplete="off" value="<?php echo $usuario->company;?>" data-mask-tel />
      </div>
    </div>
    <h5>Perfil de Usuario:</h5>
    <hr color ="green">
    <div class="row mb-3">
      <div class="col-md-6">
        <div class="col-md-12">
          <label for="email">Nombre Usuario: </label> <i class="mdi mdi-asterisk"></i>
          <input type="email" name="email" id="email" class="form-control" maxlength="50" autocomplete="off" title="Escriba un correo válido. El sistema enviará un correo para confirmarlo" placeholder="Escriba un correo valido" autocomplete="off" value="<?php echo $usuario->email;?>" readonly="readonly"/>
        </div>
        <div class="row mt-3 mb-3">
          <label class="col-md-8">Pertenece a:</label>
          <?php
            $checked= ' checked="checked"';
            foreach($currentGroups as $group) {
              echo ' 
              <div class="col-md-6">
                <div class="custom-control custom-checkbox mr-sm-2">
                  <input type="checkbox" name="groups[]" class="custom-control-input" value="'.$group->id.'"'.$checked.' disabled="disabled">
                  <label class="custom-control-label">'.htmlspecialchars($group->name,ENT_QUOTES,"UTF-8").'</label>
                </div>
              </div>
              ';
            }
          ?>
        </div>
          <?php
            if ($this->ion_auth->is_admin()){
              echo ' 
              <div class="row mt-3 mb-3">
                <label class="col-md-8">Cambiar al Grupo:</label>
                <div class="col-md-6">
                  <div class="custom-control custom-checkbox mr-sm-2">
                    <input type="checkbox" name="groups[]" id="customControlAutosizing1" class="custom-control-input" value="1">
                    <label for="customControlAutosizing1" class="custom-control-label">Admin</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="custom-control custom-checkbox mr-sm-2">
                    <input type="checkbox" name="groups[]" id="customControlAutosizing2" class="custom-control-input" value="3">
                    <label for="customControlAutosizing2" class="custom-control-label">Cajeros</label>
                  </div>
                </div>
              </div>
              ';
            }
          ?>
      </div>
      <div class="col-md-6">
        <div class="col-md-8">
          <label for="password">Password: </label>
          <input type="text" name="password" id="password" class="form-control"/>
        </div>
        <div class="col-md-8">
          <label for="password_confirm">Confirmar Password: </label>
          <input type="text" name="password_confirm" id="password_confirm" class="form-control"/>
        </div>
        <div class="col-md-8">
         <label>Status: </label>
          <select class="form-control text-center" name="status" id="status" placeholder="Status">
            <?php if($usuario->active == '1') { $status =  "ACTIVO"; } else { $status = "INACTIVO"; }?>  
            <option value="<?php echo $usuario->active;?>"><?php echo $status;?></option>
            <option value="1" > ACTIVO   </option>
            <option value="0" > INACTIVO </option>
          </select>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      <button type="submit" name="modificar" class="btn btn-primary" value="Modificar">Registrar Datos</button>
    </div>
  </form>
</div>
<script>
  // Campo numerico
  $('.decimal-2-places').number( true, 2, ',', '.' );
  $('.positive-integer').number( true, 0, ',', '.' );
</script>
<script>
   //Mascara de Telefono
  $("[data-mask-tel]").inputmask("9999 999-99-99");
</script>
<!-- Para la validacion -->
<script src="<?php echo base_url()?>assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo base_url()?>assets/libs/jquery-validation/dist/additional-methods.js"></script>
<script>
    $( "#form-registro" ).validate( {
      rules: {
        cliente: "required",
        direccion: "required",
        tlfnocel: "required",
        tlfnocasa: "required",
        password: {
          required: function() {
            if($('#password').val().lenght > 0){
              return true;  
            } else {
              return false;
            }
          },
          minlength: 8
        },
        password_confirm: {
          required: function() {
            if($('#password').val().lenght > 0){
              return true;  
            } else {
              return false;
            }
          },
          minlength: 8,
          equalTo: "#password"
        },
      },
      messages: {
        cliente: "Nombre no puede ir vacio",
        direccion: "Direccion no puede ir vacio",
        tlfnocel: "Celular no puede ir vacio",
        tlfnocasa: "Tlfno Casa no puede ir vacio",
        password: {
          required: "Please provide a password",
          minlength: "Your password must be at least 5 characters long"
        },
        password_confirm: {
          required: "Please provide a password",
          minlength: "Your password must be at least 5 characters long",
          equalTo: "Please enter the same password as above"
        },
      },
      errorElement: "em",
      errorPlacement: function ( error, element ) {
        // Add the `invalid-feedback` class to the error element
        error.addClass( "invalid-feedback" );

        if ( element.prop( "type" ) === "checkbox" ) {
          error.insertAfter( element.next( "label" ) );
        } else {
          error.insertAfter( element );
        }
      },
      highlight: function ( element, errorClass, validClass ) {
        $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
      },
      unhighlight: function (element, errorClass, validClass) {
        $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
      }
    });
    $.validator.methods.email = function( value, element ) {
      //return this.optional( element ) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@(?:\S{1,63})$/.test( value );
      return this.optional( element ) || /^(("[\w-\s]+")|([\w-].+(?:\._[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\._[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/.test( value );
    };
</script>