<!DOCTYPE html>
<html dir="ltr">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="Angel Emiro Antunez Villasmil @Devdoblea">
	<!-- Favicon icon -->
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/images/favicon.png">
	<title>Inconet Systems | Logueate</title>
	<!-- Custom CSS -->
	<link href="<?php echo base_url()?>assets/dist/css/style.min.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/dist/css/icons/font-awesome/css/fontawesome-all.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/dist/css/icons/themify-icons/themify-icons.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/dist/css/icons/material-design-iconic-font/css/materialdesignicons.min.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body oncontextmenu="return false">
	<div class="main-wrapper">
		<!-- ============================================================== -->
		<!-- Preloader - style you can find in spinners.css -->
		<!-- ============================================================== -->
		<!-- <div class="preloader">
			<div class="lds-ripple">
				<div class="lds-pos"></div>
				<div class="lds-pos"></div>
			</div>
		</div> -->
		<!-- ============================================================== -->
		<!-- Preloader - style you can find in spinners.css -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Login box.scss -->
		<!-- ============================================================== -->
		<div class="auth-wrapper d-flex no-block justify-content-center align-items-center bg-dark">
			<div class="auth-box bg-dark border-top border-secondary">

				<div id="loginform">
					<div class="text-center p-t-10 p-b-10">
						<h4 style="color:#FFFFFF"><img src="<?php echo base_url()?>assets/images/logo-icon.png" alt="logo" /> /> INCONET SYSTEMS IS C.A.</h4><br>
						<h5 style="color:#FFFFFF"><?php echo lang('reset_password_heading');?></h5>
					</div>

					<?php echo form_open('auth/reset_password/' . $code);?>
						<div class="row p-b-30">
							<div class="col-12 text-center">

								<label for="new_password" style="color:#FFFFFF" class="col-8">
										<?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text bg-warning text-white" id="basic-addon2"><i class="ti-pencil"></i></span>
									</div>
									<?php echo form_input($new_password);?>
								</div>

								<div class="clearfix"></div>
								<br> 

								<label for="new_password" style="color:#FFFFFF">
										<?php echo sprintf(lang('reset_password_new_password_confirm_label'), 'new_password_confirm');?></label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text bg-warning text-white" id="basic-addon2"><i class="ti-pencil"></i></span>
									</div>
									<?php echo form_input($new_password_confirm);?>
								</div>

							</div>
						</div>
						<?php
							if(isset($message)) {
								echo '
									<div class="alert alert-danger" role="alert">
									  '.$message.'
									</div>
								';
							}
						?>
						<div class="row border-top border-secondary">
							<div class="col-12">
								<div class="form-group">
									<div class="p-t-20">
										<?php echo form_input($user_id);?>
										<?php echo form_hidden($csrf); ?>

										<?php echo form_submit('submit', lang('reset_password_submit_btn'), 'class="btn btn-success float-right"');?>
									</div>
								</div>
							</div>
						</div>
					<?php echo form_close();?>
				
				</div>

			</div>
		</div>
		<!-- ============================================================== -->
		<!-- Login box.scss -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Page wrapper scss in scafholding.scss -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Page wrapper scss in scafholding.scss -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Right Sidebar -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Right Sidebar -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- All Required js -->
	<!-- ============================================================== -->
	<!-- JQUERY JavaScript -->
	<script src="<?php echo base_url()?>assets/libs/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap tether Core JavaScript -->
	<script src="<?php echo base_url()?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
	<script src="<?php echo base_url()?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>


</body>

</html>	