<!DOCTYPE html>
<html dir="ltr">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="Angel Emiro Antunez Villasmil @Devdoblea">
  <!-- Favicon icon -->
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/images/favicon.png">
  <title> <?php echo $this->config->item("site_name"); ?>&nbsp;| Logueate</title>
  <!-- Custom CSS -->
  <link href="<?php echo base_url()?>assets/dist/css/style.min.css" rel="stylesheet">
  <!-- Custom TAILWIND LOGIN CSS -->
  <link href="<?php echo base_url()?>assets/dist/css/auth_login.css" rel="stylesheet">
  <!-- TailwindCSS -->
  <script src="https://cdn.tailwindcss.com/?plugins=forms,typography,aspect-ratio,line-clamp"></script>
  <style>
    body {
      height: 100%;
      background-image: url("<?php echo base_url();?>uploads/images/fondo-barbacoa.jpg");
      background-repeat: no-repeat !important;
      background-attachment: fixed !important;
      background-position: center !important;
      background-size: cover;
    }
  </style>

</head>

<body oncontextmenu="return false">
  <!-- ============================================================== -->
  <!-- Preloader - style you can find in spinners.css -->
  <!-- ============================================================== -->
  <div class="preloader">
    <div class="lds-ripple">
      <div class="lds-pos"></div>
    </div>
  </div>
  <!-- ============================================================== -->
  <!-- Preloader - style you can find in spinners.css -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- Login box.scss -->
  <!-- ============================================================== -->
  <div class="main-wrapper">
    <div class="flex items-center min-h-screen lg:justify-center lg:px-20 lg:py-8 xs:px-10">
      <div class="flex flex-col overflow-hidden bg-white rounded-md shadow-lg max md:flex-row md:flex-1 lg:max-w-screen-md">
        <!-- Logo  -->
        <div class="flex xs:w-[100%] lg:w-[50%] xl:w-[50%] px-6 py-10 justify-center items-center">
          <img
            class="ma-4"
            src="/uploads/images/<?php echo $imagenFondo;?>"
            alt=""
          />
        </div>
        <!-- div interno centrado -->
        <div class="flex lg:w-[50%] sm:w-[100%] xs:w-[100%] px-6 py-12">
          <!-- saludo -->
          <div class="inline-flex w-[100%] flex-col bg-white items-center">
            <h1 class="text-gray-800 font-bold text-2xl mb-1">Hola otra vez!</h1>
            <br>
            <p class="text-sm font-normal text-gray-600 mb-7">
              Bienvenid@ de vuelta
            </p>
             <!-- Form -->
            <?php echo form_open("auth/login", 'class="form-horizontal m-t-20" id="loginform"');?>
              <div class="flex sm:w-[90%] items-center border-2 py-2 px-3 rounded-2xl mb-4">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5 text-gray-400"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207"
                  />
                </svg>
                <input
                  class="pl-2 outline-none border-none"
                  type="text"
                  name="identity"
                  id="identity"
                  placeholder="Tu Email"
                />
              </div>
              <div class="flex sm:w-[90%] items-center border-2 py-2 px-3 rounded-2xl">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5 text-gray-400"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fill-rule="evenodd"
                    d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z"
                    clip-rule="evenodd"
                  />
                </svg>
                <input
                  class="pl-2 outline-none border-none"
                  type="password"
                  name="password"
                  id="password"
                  placeholder="Contraseña"
                />
              </div>
              <div class="text-danger w-full text-center" role="alert">
                <?php if(isset($message)) : echo $message; endif; ?>
              </div>
              <button type="submit"
                class="block w-full bg-[#ea4e4e] mt-4 py-2 rounded-2xl text-white font-semibold mb-2">
                Login
              </button>
              <span class="text-sm ml-2 hover:text-blue-500 cursor-pointer">Olvidaste tu Contraseña ?</span>
            <?php echo form_close();?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ============================================================== -->
  <!-- Login box.scss -->
  <!-- ============================================================== -->


  <!-- ============================================================== -->
  <!-- This page plugin js -->
  <!-- ============================================================== -->
  <!-- JQUERY JavaScript -->
  <script src="<?php echo base_url()?>assets/libs/jquery/dist/jquery.min.js"></script>
  <script>

    $(".preloader").fadeOut();

  </script>

</body>

</html>
