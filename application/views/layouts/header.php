<!DOCTYPE html>
<html dir="ltr" lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Sistema para el control de Compras y Ventas, Inventario y PUNTO DE VENTA">
    <meta name="author" content="Angel Emiro Antunez Villasmil @Devdoblea">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/images/favicon.png">
    <title>
      <?php echo $this->config->item("site_name") . ' | ' . $titulo; ?>
    </title>
    <!-- Custom CSS -->
    <link href="<?php echo base_url() ?>assets/dist/css/style.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/dist/css/icons/font-awesome/css/fontawesome-all.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/dist/css/icons/themify-icons/themify-icons.css" rel="stylesheet">
    <link
      href="<?php echo base_url() ?>assets/dist/css/icons/material-design-iconic-fonts-5/css/materialdesignicons.min.css"
      rel="stylesheet">
    <!-- JQuery -->
    <script src="<?php echo base_url() ?>assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- INPUTMASK -->
    <script src="<?php echo base_url() ?>assets/libs/inputmask-5/dist/jquery.inputmask.min.js"></script>
    <!-- Para acomodar las tablas en monitores pequeños -->
    <link href="<?php echo base_url() ?>assets/libs/stacktable/stacktable.css" rel="stylesheet">
    <script src="<?php echo base_url() ?>assets/libs/stacktable/stacktable.js"></script>
    <!--Jquery.Number-->
    <script src="<?php echo base_url() ?>assets/libs/jquery-number/jquery.number.js"></script>
    <!-- Toastr Para las Notificaciones tipo PNotify-->
    <link href="<?php echo base_url() ?>assets/libs/toastr/build/toastr.min.css" rel="stylesheet" />
    <script src="<?php echo base_url() ?>assets/libs/toastr/build/toastr.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url() ?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo base_url() ?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
  </head>

  <!-- <body oncontextmenu="return false"> -->

  <body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
      <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
      </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
      <!-- ============================================================== -->
      <!-- Topbar header - style you can find in pages.scss -->
      <!-- ============================================================== -->
      <header class="topbar" data-navbarbg="skin5">
        <nav class="navbar top-navbar navbar-expand-md navbar-dark">
          <div class="navbar-header" data-logobg="skin5">
            <!-- This is for the sidebar toggle which is visible on mobile only -->
            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                class="ti-menu ti-close"></i></a>
            <!-- ============================================================== -->
            <!-- Logo -->
            <!-- ============================================================== -->
            <a class="navbar-brand" href="#">
              <!-- Logo icon -->
              <b class="logo-icon p-l-10">
                <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                <!-- Dark Logo icon -->
                <i class="mdi mdi-24px mdi-view-dashboard"></i>
              </b>
              <!--End Logo icon -->
              <!-- Logo text -->
              <span class="logo-text">
                <!-- dark Logo text -->
                <?php echo $this->config->item("site_name"); ?>
              </span>
            </a>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Toggle which is visible on mobile only -->
            <!-- ============================================================== -->
            <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
              data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
              aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
          </div>
          <!-- ============================================================== -->
          <!-- End Logo -->
          <!-- ============================================================== -->
          <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav float-left mr-auto">
              <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light"
                  href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a>
              </li>
              <!-- ============================================================== -->
              <!-- create new -->
              <!-- ============================================================== -->
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                  aria-haspopup="true" aria-expanded="false">
                  <span class="d-none d-md-block">Crear Nuevo <i class="fas fa-angle-down"></i></span>
                  <span class="d-block d-md-none"><i class="fas fa-plus"></i></span>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="/clientes/index">Cliente</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="/ventas/pos">Venta</a>
                </div>
              </li>
              <!-- ============================================================== -->
              <!-- Search -->
              <!-- ============================================================== -->
              <!-- <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
							<form class="app-search position-absolute">
								<input type="text" class="form-control" placeholder="Search &amp; enter"> <a class="srh-btn"><i class="ti-close"></i></a>
							</form>
						</li> -->
            </ul>
            <!-- ============================================================== -->
            <!-- Right side toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav float-right">
              <!-- ============================================================== -->
              <!-- Nombre del Usuario -->
              <!-- ============================================================== -->
              <li class="nav-item dropdown">
                <div class="col-md-3 p-4">
                  <label class="text-white">
                    <?php //$user = $this->ion_auth->user()->row();
									//$name = explode(' ', $user->first_name);
									//echo $name[0]; ?>
                  </label>
                </div>
              </li>
              <!-- ============================================================== -->
              <!-- End Nombre del Usuario -->
              <!-- ============================================================== -->

              <!-- ============================================================== -->
              <!-- User profile and search -->
              <!-- ============================================================== -->
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic pt-2" href=""
                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="rounded-circle"><i class="fa fa-power-off fa-2x m-r-5 m-l-5"></i></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right user-dd animated">
                  <a class="dropdown-item" href="javascript:void(0)"><i class="ti-user m-r-5 m-l-5"></i> Mi Perfil</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="/empresa/index"><i class="ti-settings m-r-5 m-l-5"></i>
                    Configuracion</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="/auth/logout"><i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                  <div class="dropdown-divider"></div>
                  <!-- <div class="p-l-30 p-10"><a href="javascript:void(0)" class="btn btn-sm btn-success btn-rounded">View Profile</a></div> -->
                </div>
              </li>
              <!-- ============================================================== -->
              <!-- User profile and search -->
              <!-- ============================================================== -->
            </ul>
          </div>
        </nav>
      </header>
      <!-- ============================================================== -->
      <!-- End Topbar header -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- Left Sidebar - style you can find in sidebar.scss  -->
      <!-- ============================================================== -->
      <aside class="left-sidebar" data-sidebarbg="skin5">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar">
          <!-- Sidebar navigation-->
          <nav class="sidebar-nav">
            <ul id="sidebarnav" class="p-t-30">
              <li class="sidebar-item">
                <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/main/dashboard"
                  aria-expanded="false">
                  <i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span>
                </a>
              </li>
            </ul>
          </nav>
          <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
      </aside>
      <!-- ============================================================== -->
      <!-- End Left Sidebar - style you can find in sidebar.scss  -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- Page wrapper  -->
      <!-- ============================================================== -->
      <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
          <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
              <?php if ($this->uri->segment(2) == 'presup') {
                  $pageTitle = 'Presupuesto';
                } elseif ($this->uri->segment(2) == 'index') {
                  $pageTitle = strtoupper($this->uri->segment(1));
                } else {
                  $pageTitle = ($this->uri->segment(2) !== NULL ) ? ucfirst($this->uri->segment(2)) : '';
                } 
              ?>
              <h4 class="page-title"> <?php echo $pageTitle;?> </h4>
              <div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <?php
                  $segm1 = '';
                  $segm2 = '';
									if ($this->uri->segment(1) != 'main') {
										if ($this->uri->segment(2) != 'index') {
                      $segm1 = ($this->uri->segment(1) != NULL ) ? lcfirst($this->uri->segment(1)) : '';
                      $segm2 = ($this->uri->segment(2) != NULL ) ? lcfirst($this->uri->segment(2)) : '';
											echo '
												<li class="breadcrumb-item"><a href="/main/dashboard">Home</a></li>
												<li class="breadcrumb-item active" aria-current="page"><a href="/' . $segm1 . '/index">' . $segm1 . '</a></li>
												<li class="breadcrumb-item active" aria-current="page">' . $segm2 . '</li>
											';
										} else {
											echo '
													<li class="breadcrumb-item"><a href="/main/dashboard">Home</a></li>
													<li class="breadcrumb-item active" aria-current="page">' . $segm1 . '</li>
												';
										}
									} else {
										echo '
												<li class="breadcrumb-item"><a href="/main/dashboard">Home</a></li>
												<li class="breadcrumb-item active" aria-current="page">' . $segm2 . '</li>
											';
									}
									?>
                  </ol>
                </nav>
              </div>
            </div>
          </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <!-- <div class="d-sm-flex align-items-center justify-content-between mb-4">
			    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
			    <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
			  </div> -->