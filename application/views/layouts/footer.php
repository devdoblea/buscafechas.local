			</div>
			<!-- /.container-fluid -->
			<!-- ============================================================== -->
			<!-- footer -->
			<!-- ============================================================== -->
			<footer class="footer text-center">
				All Rights Reserved by your Developer <a href="https://elcuartodeangel.wordpress.com">DEVDOBLEA</a> and Designed with <i class="mdi mdi-heart"></i> by <a href="https://wrappixel.com">WrapPixel</a>. Pantalla a: <span id="pnt"><script>document.write(window.screen.width+'x'+window.screen.height+'pixels');</script></span>. Ventana a: <span id="pnt"><script>
                document.write(
                  (window.innerWidth ||
                    document.documentElement.clientWidth ||
                    document.body.clientWidth) +
                    "x" +
                    (window.innerHeight ||
                      document.documentElement.clientHeight ||
                      document.body.clientHeight) +
                    "pixels."
                )
              </script></span>
			</footer>
			<!-- ============================================================== -->
			<!-- End footer -->
			<!-- ============================================================== -->
		</div>
		<!-- ============================================================== -->
		<!-- End Page wrapper  -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Wrapper -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- All Jquery -->
	<!-- ============================================================== -->

	<!-- <script src="<?php echo base_url()?>assets/dist/js/jquery-ui.min.js"></script> -->
	<script src="<?php echo base_url()?>assets/dist/js/jquery.ui.touch-punch-improved.js"></script>
	<!-- slimscrollbar scrollbar JavaScript -->
	<script src="<?php echo base_url()?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
	<script src="<?php echo base_url()?>assets/extra-libs/sparkline/sparkline.js"></script>
	<!--Wave Effects -->
	<script src="<?php echo base_url()?>assets/dist/js/waves.js"></script>
	<!--Menu sidebar -->
	<script src="<?php echo base_url()?>assets/dist/js/sidebarmenu.js"></script>
	<!--Custom JavaScript -->
	<script src="<?php echo base_url()?>assets/dist/js/custom.min.js"></script>
	<!-- this page js -->
	<script src="<?php echo base_url()?>assets/libs/moment/min/moment.min.js"></script>

  <script type="text/javascript">
		// captura de mensajes para las notificaciones
		var msg_txt = "Mensaje del Sistema : </br><?php echo $this->session->flashdata('message');?>";
		var tipo_alert = "<?php echo $tipomsg = $this->session->flashdata('tipomsg');?>";
		if (msg_txt) {
		  if(tipo_alert == 'error'){
			toastr.options.timeOut = 4000; // 3s
			toastr.error(msg_txt);
		  } else if(tipo_alert == 'success'){
			toastr.options.timeOut = 4000; // 3s
			toastr.success(msg_txt);
		  } else if(tipo_alert == 'warning'){
			toastr.options.timeOut = 4000; // 3s
			toastr.warning(msg_txt);
		  } else if(tipo_alert == 'info'){
			toastr.options.timeOut = 4000; // 3s
			toastr.info(msg_txt);
		  }
		}
	</script>
	<script type="text/javascript">
		//****************************
	  /* This is for sidebartoggler*/
	  //****************************
    // Cuando una pantalla es mas pequeña de lo normal para un PC -->
    function sidebartoggler() {
      $("#main-wrapper").toggleClass("mini-sidebar");
      if ($("#main-wrapper").hasClass("mini-sidebar")) {
          $(".sidebartoggler").prop("checked", !0);
          $("#main-wrapper").attr("data-sidebartype", "mini-sidebar");
      } else {
          $(".sidebartoggler").prop("checked", !1);
          $("#main-wrapper").attr("data-sidebartype", "full");
      }
		};
    function encogeBarra() {
      var pantalla = window.screen.width;
      //console.log('HOLA'+pantalla);
      if (pantalla < 1700) {
        sidebartoggler();
      }
    };
	  // para cargar el div_listar cuando cargue la pagina
	  if (document.addEventListener){
	    window.addEventListener('load',encogeBarra(),false);
	  } else {
	    window.attachEvent('onload',encogeBarra());
	  }
  </script>

</body>

</html>