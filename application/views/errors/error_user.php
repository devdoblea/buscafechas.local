<div class="modal-header bg-success"style="color:#FFFFFF">
  <h5 class="modal-title" id="exampleModalLabel">Editar Usuario</h5>
  <small class="mt-2 ml-4"><?php print_r($usuario);?></small>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
    <div class="row mb-12">
        <p style="text-align: center;">
          <h3> UD. NO ESTA AUTORIZADO PARA VER/EDITAR ESTE USUARIO. <br>SI NECESITA VERLOS DEBE LLAMAR AL DESARROLLADOR</h3>
        </p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
</div>