<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ventas extends CI_Controller {

  public function __construct() {

    parent::__construct();
    $this->load->model('Ventas_model');
    $this->load->model('Empresa_model');
    $this->load->model('Ion_auth_model');
  }

  //con esta función validamos y protegemos el buscador
  public function validar() {

    // if(!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin())) {redirect('auth/login');}
    // print("<pre>POST:".print_r($_POST,true)."</pre>");

    // validar el dato que envia el usuario considerando que no haya inyecciones de codigo.
    $this->form_validation->set_rules('buscando', 'buscador', 'trim|xss_clean');
    $this->form_validation->set_message('required', 'El %s no puede ir vacío!');
    if ($this->form_validation->run() == TRUE) {

      $codserv = $this->input->post('buscando');
      $fecsess = $this->input->post('fec_sess');

      $this->session->set_userdata('buscando', $codserv);
      $this->session->set_userdata('fec_sess', $fecsess);
      redirect('ventas/index');
    } else {

      //cargamos la vista y el array data
      $this->session->unset_userdata('buscando');
      $this->session->unset_userdata('fec_sess');
      redirect('ventas/index');
    }
  }

  // para consultar por numero de orden
  public function index() {

    // if(!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin())) {redirect('auth/login');}

    // para borrar de la limpiar la memoria de la ultima sesion buscada
    $this->session->unset_userdata('idsess');
    $this->session->unset_userdata('nroses');
    $this->session->unset_userdata('fecini');
    $this->session->unset_userdata('fecfin');
    $this->session->unset_userdata('iduser');
    $this->session->unset_userdata('tot_formpa');
    $this->session->unset_userdata('tot_sesion');
    $this->session->unset_userdata('tot_gananc');

    // datos de las sesiones activas y cerradas
    $buscador = $this->session->userdata('buscando');
    $fec_sess = $this->session->userdata('fec_sess');
    $segmento = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    //$por_pagina = $this->input->post('limite'); //Número de registros mostrados por páginas
    $por_pagina = 9; //Número de registros mostrados por páginas
    $config['base_url'] = base_url() . 'ventas/index'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
    $config['total_rows'] = $this->Ventas_model->busqueda($buscador,$fec_sess); //calcula el número de filas
    $config['per_page'] = $por_pagina; //Número de registros mostrados por páginas
    $config["uri_segment"] = 3;
    $config['num_links'] = 5; //Número de links mostrados en la paginación
    //$config['use_page_numbers'] = TRUE;
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['prev_link'] = 'Primera';
    $config['prev_tag_open'] = '<li class="page-item"><a class="page-link"';
    $config['prev_tag_close'] = '</a></li>';
    $config['next_link'] = 'Siguiente';
    $config['next_tag_open'] = '<li class="page-item"><a class="page-link"';
    $config['next_tag_close'] = '</a></li>';
    $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" tabindex="-1">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li class="page-item"><a class="page-link"';
    $config['num_tag_close'] = '</a></li>';
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li class="page-item"><a class="page-link"';
    $config['last_tag_close'] = '</a></li>';
    $config['first_link'] = 'Inicio';
    $config['last_link'] = 'Ultima';

    $this->pagination->initialize($config);
    //el array con los datos a paginar ya preparados
    $datos["resultados"] = $this->Ventas_model->total_posts_paginados($buscador, $fec_sess, $por_pagina, $segmento);

    // cargamos la vista principal
    $header['titulo']  = 'Ventas';
    $datos['empresa']  = $this->Empresa_model->datos_empresa_row();
    $datos["is_admin"] = ( ! $this->ion_auth->is_admin() ) ? 0 : $this->ion_auth->is_admin();
    // Setear los valores en caso de aplicar la tasa del dolar
    $aplicar_tasa = $datos['empresa']->aplicar_tasa;
    $tasa_dolar = $datos['empresa']->tasa_dolar;
    if ( $aplicar_tasa == 'Si' && $tasa_dolar > 0 ) :
      $datos["tasa_dolar"] = $tasa_dolar;
    elseif ( $aplicar_tasa == 'Si' && $tasa_dolar <= 0 ) :
      $datos["tasa_dolar"] = 1;
    elseif ( $aplicar_tasa == 'No' ) :
      $datos["tasa_dolar"] = 1;
    endif;

    $this->load->view('layouts/header',$header);
    $this->load->view('front-end/ventas/tabla_ventas',$datos);
    $this->load->view('layouts/footer');

    /*$sections = array(
      'config'  => TRUE,
      'queries' => TRUE,
      'controller_info' => TRUE,
      'uri_strig' => TRUE,
      'session_data' => TRUE

      );
    $this->output->set_profiler_sections($sections);
    $this->output->enable_profiler(TRUE);*/
  }

  // ir al Punto de Venta
  public function pos() {
    // if( ! $this->ion_auth->logged_in() || ! $this->ion_auth->is_admin() ) {redirect('auth/login');}
    /* Por el redireccionamiento hecho de insertar_registro() a modulo/ticket_pos()
     * que se hizo pasando via flashsessions el valor del id de la factura para que se 
     * busque en esta ultima funcion los datos de la factura a imprimir, debo resetear
     * lo guardado en la flashsession id_factura  */
    $this->session->set_flashdata('id_factura','');

    //print("<pre>POST:".print_r($_POST,true)."</pre>");
    // Setear la fecha y la hora para que
    // sea la del sistema operativo y no la del servidor
    $timezone = "America/Caracas";
    date_default_timezone_set($timezone);
    $date = new DateTime("now",new DateTimeZone($timezone));
    // darle una fecha a la sesion
    $datos["diasess"]  = $date->format('Y-m-d');
    // darle una hora de inicio de la sesion
    $datos["horasess"] = $date->format('H:i:s');
    // ubicar numero de sesion actual
    $emp = $this->Ventas_model->datos_empresa_row();
    $datos["nrosess"] = $emp->nrosess;
    //Ubicar la sesion a utilizar
    $datos["pos_session"] = $this->Ventas_model->get_sesion_punto_de_venta($datos["nrosess"],$datos["diasess"],$datos["horasess"]);
    // echo '<br>Pos_sess:'.$datos["pos_session"];
    // procedo a buscar los datos del producto en las tablas involucradas
    $datos["productos"] = $this->Ventas_model->get_listar_productos_mas_vendidos($buscador="", $catego="TODAS", $por_pagina="32", $limite="");

    //print("<pre>POST:".print_r($_POST,true)."</pre>");
    $buscador = $this->session->userdata('buscandocli');
    $segmento = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    $por_pagina = 32; //Número de registros mostrados por páginas
    $config['total_rows'] = $this->Ventas_model->busqueda_clientes($buscador); //calcula el número de filas
    //el array con los datos a paginar ya preparados
    $datos["resultados"] = $this->Ventas_model->total_posts_clientes_paginados($buscador, $por_pagina, $segmento);
    // cargamos la vista principal
    $datos["vienede"] = $this->input->post('vienede');
    // Si se selecciona el cliente entonces cargo la variable para pasarla a la vista
    $idcli = $this->input->post('idcliente');
    // Compruebo que vengan datos para enviar una variable valida
    if( $idcli != '' ) :
      $apenomcli = $this->Ventas_model->get_apenom_cliente_by_id($idcli);
    else :
      $apenomcli = 'Cliente';
    endif;
    $datos["apenomcli"]   = $apenomcli;
    $datos["controlador"] = 'ventas';
    // cargamos la vista principal
    $datos['empresa']  = $this->Empresa_model->datos_empresa_row();
    // Setear los valores en caso de aplicar la tasa del dolar
    $aplicar_tasa = $datos['empresa']->aplicar_tasa;
    $tasa_dolar = $datos['empresa']->tasa_dolar;
    if ( $aplicar_tasa == 'Si' && $tasa_dolar > 0 ) :
      $datos["tasa_dolar"] = $tasa_dolar;
    elseif ( $aplicar_tasa == 'Si' && $tasa_dolar <= 0 ) :
      $datos["tasa_dolar"] = 1;
    elseif ( $aplicar_tasa == 'No' ) :
      $datos["tasa_dolar"] = 1;
    endif;

    //$this->load->view('modulopos/punto/pos');
    $this->load->view('modulopos/poslayout/headpos');
    $this->load->view('modulopos/punto/pos_2', $datos);
    $this->load->view('modulopos/poslayout/footpos');
  }

  // registrar una venta
  public function regventa() {

    if(!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin())) {redirect('auth/login');}

    // cargamos la vista principal
    $header['titulo']  = 'Regitrar Venta';
    $datos['empresa']  = $this->Empresa_model->datos_empresa_row();
    $datos["is_admin"] = ( ! $this->ion_auth->is_admin() ) ? 0 : $this->ion_auth->is_admin();

    $this->load->view('layouts/header',$header);
    $this->load->view('front-end/ventas/reg_venta');
    $this->load->view('layouts/footer');
  }
  // listar los articulos creados
  public function listar_articulos() {
    // cargamos la vista principal
    $datos['empresa']  = $this->Empresa_model->datos_empresa_row();
    // Setear los valores en caso de aplicar el iva
    if ($datos['empresa']->aplicar_iva == 'Si') :
      $datos["iva"] = $datos['empresa']->iva;
    elseif ($datos['empresa']->aplicar_iva == 'No') :
      $datos["iva"] = 0;
    endif;
    // Setear los valores en caso de aplicar la tasa del dolar
    $aplicar_tasa = $datos['empresa']->aplicar_tasa;
    $tasa_dolar = $datos['empresa']->tasa_dolar;
    if ( $aplicar_tasa == 'Si' && $tasa_dolar > 0 ) :
      $datos["tasa_dolar"] = $tasa_dolar;
    elseif ( $aplicar_tasa == 'Si' && $tasa_dolar <= 0 ) :
      $datos["tasa_dolar"] = 1;
    elseif ( $aplicar_tasa == 'No' ) :
      $datos["tasa_dolar"] = 1;
    endif;
    $this->load->view('modulopos/punto/listar_articulos', $datos);
  }

  // listar los precios totales
  public function listar_totales() {
    // cargamos la vista principal
    $datos['empresa']  = $this->Empresa_model->datos_empresa_row();
    // Setear los valores en caso de aplicar el iva
    if ($datos['empresa']->aplicar_iva == 'Si') :
      $datos["iva"] = $datos['empresa']->iva;
    elseif ($datos['empresa']->aplicar_iva == 'No') :
      $datos["iva"] = 0;
    endif;
    // Setear los valores en caso de aplicar la tasa del dolar
    $aplicar_tasa = $datos['empresa']->aplicar_tasa;
    $tasa_dolar = $datos['empresa']->tasa_dolar;
    if ( $aplicar_tasa == 'Si' && $tasa_dolar > 0 ) :
      $datos["tasa_dolar"] = $tasa_dolar;
    elseif ( $aplicar_tasa == 'Si' && $tasa_dolar <= 0 ) :
      $datos["tasa_dolar"] = 1;
    elseif ( $aplicar_tasa == 'No' ) :
      $datos["tasa_dolar"] = 1;
    endif;
    $this->load->view('modulopos/punto/listar_totales',$datos);
  }

  // listar los precios totales
  public function listar_prepro_totales_post() {

    $datos['empresa'] = $this->Modulo_model->datos_empresa_row();
    // Setear los valores en caso de aplicar el iva
    if ($datos['empresa']->aplicar_iva == 'Si') :
      $datos["iva"] = $datos['empresa']->iva;
    elseif ($datos['empresa']->aplicar_iva == 'No') :
      $datos["iva"] = 0;
    endif;
    $datos["sim"] = $datos['empresa']->simbol_moneda;
    // Setear los valores en caso de aplicar la tasa del dolar
    $aplicar_tasa = $datos['empresa']->aplicar_tasa;
    $tasa_dolar = $datos['empresa']->tasa_dolar;
    if ( $aplicar_tasa == 'Si' && $tasa_dolar > 0 ) :
      $datos["tasa_dolar"] = $tasa_dolar;
    elseif ( $aplicar_tasa == 'Si' && $tasa_dolar <= 0 ) :
      $datos["tasa_dolar"] = 1;
    elseif ( $aplicar_tasa == 'No' ) :
      $datos["tasa_dolar"] = 1;
    endif;

    $this->load->view('modulopos/punto/preproceso/prepro_totales',$datos);
  }

  // para registrar los articulos pero con codeigniter-Cart
  public function agregar_art_cart(){
    //print("<pre>".print_r($_POST,true)."</pre>");
    $idprd = $this->input->post('idprd');
    if($idprd == '') :
      $id = random_int(8000,9000);
    else :
      $id = $idprd;
    endif;
    $qty   = $this->input->post('cantp');
    $price = $this->input->post('prcio');
    $name  = $this->input->post('descr');

    $data = array(
      'id'      => $id,
      'qty'     => $qty,
      'price'   => $price,
      'name'    => $name,
      'options' => array($this->input->post('opciones'))
    );

    return $this->cart->insert($data);
  }

  // para borrar producto por producto del pedido
  public function borrar_articulo(){

    //print("<pre>posteos: ".print_r($_POST,true)."</pre>");
    // recibo el post
    $rowid = $this->input->post('rowid'); // variable q indica que el valor viene de la bd
    $cantp = $this->input->post('cantp'); // variable q indica la cantidad del producto
    // en caso contrario borro el articulo del carrito de la sesion activa
    $data_cart = array(
      'rowid'   => $rowid,
      'qty'     => 0
    );

    $result = $this->cart->update($data_cart);

      if( $result == false) {

        $this->session->set_flashdata('message', 'No se puede borrar este registro..!');
        $this->session->set_flashdata('tipomsg','error');
        $this->load->view('modulopos/punto/listar_articulos');

      } else {

        $this->session->set_flashdata('message', 'Se borró correctamente el registro.!');
        $this->session->set_flashdata('tipomsg','success');
        $this->load->view('modulopos/punto/listar_articulos');
      }

    //return $result; // envio el resultado del update. solo se retorna la palabra FALSE si hubo error o TRUE si se actualizó correctamente
  }

  // pre-procesamiento del pedido de productos
  // seleccion de cliente - Precios Total - Seleccion Forma de Pago
  public function preproceso() {
    // Recibo los posteos
    //print("<pre>posteos: ".print_r($_POST,true)."</pre>");
    $datos['idcli']    = $this->input->post('idcli');
    $datos['apenom']   = $this->input->post('apenom');
    $datos['pos_sess'] = $this->input->post('pos_sess');
    $datos['dia_sess'] = $this->input->post('dia_sess');
    $datos['hrs_sess'] = $this->input->post('hrs_sess');
    $datos["controlador"] = 'ventas';
    $datos['empresa'] = $this->Modulo_model->datos_empresa_row();
    // Setear los valores en caso de aplicar el iva
    if ($datos['empresa']->aplicar_iva == 'Si') :
      $datos["iva"] = $datos['empresa']->iva;
    elseif ($datos['empresa']->aplicar_iva == 'No') :
      $datos["iva"] = 0;
    endif;
    $datos["sim"] = $datos['empresa']->simbol_moneda;
    // Setear los valores en caso de aplicar la tasa del dolar
    $aplicar_tasa = $datos['empresa']->aplicar_tasa;
    $tasa_dolar = $datos['empresa']->tasa_dolar;
    if ( $aplicar_tasa == 'Si' && $tasa_dolar > 0 ) :
      $datos["tasa_dolar"] = $tasa_dolar;
    elseif ( $aplicar_tasa == 'Si' && $tasa_dolar <= 0 ) :
      $datos["tasa_dolar"] = 1;
    elseif ( $aplicar_tasa == 'No' ) :
      $datos["tasa_dolar"] = 1;
    endif;
    // Redireccion
    $this->load->view('modulopos/punto/preproceso/preproceso', $datos);
  }

  // listar los precios totales
  public function prepro_totales() {

    $datos['empresa'] = $this->Modulo_model->datos_empresa_row();
    // Setear los valores en caso de aplicar el iva
    if ($datos['empresa']->aplicar_iva == 'Si') :
      $datos["iva"] = $datos['empresa']->iva;
    elseif ($datos['empresa']->aplicar_iva == 'No') :
      $datos["iva"] = 0;
    endif;
    $datos["sim"] = $datos['empresa']->simbol_moneda;
    // Setear los valores en caso de aplicar la tasa del dolar
    $aplicar_tasa = $datos['empresa']->aplicar_tasa;
    $tasa_dolar = $datos['empresa']->tasa_dolar;
    if ( $aplicar_tasa == 'Si' && $tasa_dolar > 0 ) :
      $datos["tasa_dolar"] = $tasa_dolar;
    elseif ( $aplicar_tasa == 'Si' && $tasa_dolar <= 0 ) :
      $datos["tasa_dolar"] = 1;
    elseif ( $aplicar_tasa == 'No' ) :
      $datos["tasa_dolar"] = 1;
    endif;

    $this->load->view('modulopos/punto/preproceso/prepro_totales',$datos);
  }


  //////////////////////////////////////////////////////////////////////////////
  // BUSCAR CLIENTES - funcion inical
  function clientepos() {

    if( ! $this->ion_auth->logged_in() || ! $this->ion_auth->is_admin() ) {redirect('auth/login');}

    //print("<pre>POST:".print_r($_POST,true)."</pre>");
    $buscador = $this->session->userdata('buscandocli');
    $segmento = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    $por_pagina = 30; //Número de registros mostrados por páginas
    $config['total_rows'] = $this->Modulo_model->busqueda_clientes($buscador); //calcula el número de filas
    //el array con los datos a paginar ya preparados
    $datos["resultados"] = $this->Modulo_model->total_posts_clientes_paginados($buscador, $por_pagina, $segmento);
    // cargamos la vista principal
    $datos["vienede"] = $this->input->post('vienede');
    // Redirecciono
    $this->load->view('modulopos/punto/clientepos/clientepos',$datos);
  }

  // para consultar si la cedula ya existe
  public function consultcli(){
    $cedcli = str_replace('.','',$this->input->post('ccedula'));
    $consulta = $this->Modulo_model->consultar_ced_cli($cedcli);
      //echo $consulta;
      if ($consulta == 0 ) :
        // si el valor es menor a 1 o igual a cero valid valdrá "TRUE" e indica que NO existe esa cedula
        $valid = json_encode(TRUE);
        echo $valid;
      elseif ($consulta >= 1 ) :
        // si el valor es mayor a 1 valid valdrá "FALSE" e indica que SI existe esa cedula
        $valid = json_encode(FALSE);
        echo $valid;
      endif;
  }

  // para consultar si la cedula ya existe
  public function consultmail(){
    $ecorreo = $this->input->post('ecorreo');
    $consulta = $this->Modulo_model->consultar_email($ecorreo);
      //echo $consulta;
      if ($consulta == 0 ) :
        // si el valor es menor a 1 o igual a cero valid valdrá "TRUE" e indica que NO existe esa cedula
        $valid = json_encode(TRUE);
        echo $valid;
      elseif ($consulta >= 1 ) :
        // si el valor es mayor a 1 valid valdrá "FALSE" e indica que SI existe esa cedula
        $valid = json_encode(FALSE);
        echo $valid;
      endif;
  }

  //con esta función validamos y protegemos el buscador
  public function validar_cli() {

    if(!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin())) {redirect('auth/login');}

    $this->form_validation->set_rules('buscador', 'buscador', 'required|min_length[2]|max_length[20]|trim|xss_clean');
    $this->form_validation->set_message('required', 'El %s no puede ir vacío!');
    $this->form_validation->set_message('min_length', 'El %s debe tener al menos %s carácteres');
    $this->form_validation->set_message('max_length', 'El %s no puede tener más de %s carácteres');
    if ($this->form_validation->run() == TRUE) :
      // print("<pre>POST:".print_r($_POST,true)."</pre>");
      $buscador = $this->input->post('buscador');
      $this->session->set_userdata('buscador', $buscador);
      redirect('ventas/clientes_encontrados');
    else :

      //cargamos la vista y el array data
      $this->session->unset_userdata('buscador');
      redirect('ventas/clientes_encontrados');
    endif;
  }

  // CLientes encontrados
  function clientes_encontrados() {

    if(!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin())) {redirect('auth/login');}

    // print("<pre>POST:".print_r($_POST,true)."</pre>");
    $buscador = $this->session->userdata('buscador');
    $segmento = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    //$por_pagina = $this->input->post('limite'); //Número de registros mostrados por páginas
    $por_pagina = 30; //Número de registros mostrados por páginas
    $config['base_url'] = base_url() . 'ventas/clientes_encontrados'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
    $config['total_rows'] = $this->Ventas_model->busqueda_clientes($buscador); //calcula el número de filas
    //el array con los datos a paginar ya preparados
    $datos["resultados"] = $this->Ventas_model->total_posts_clientes_paginados($buscador, $por_pagina, $segmento);
    // cargamos la vista principal
    $datos["vienede"] = $this->input->post('vienede');
    // Redirecciono
    $this->load->view('modulopos/punto/clientepos/clientes_encontrados',$datos);
  }

  //con esta función registramos clientes
  public function regcliente() {

    if(!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin())) {redirect('auth/login');}

    $this->load->view('modulopos/punto/clientepos/regcliente');
  }

  // para imprimir el ticket de la factura
  public function rpt_print_ticket_pdf() {

    if(!$this->ion_auth->logged_in() ) {redirect('auth/login');}

    if( ! $_POST ) :
      // envio un mensaje
      $this->session->set_flashdata('message','No hay Datos para Mostrar');
      $this->session->set_flashdata('tipomsg','error');
      redirect('ventas/pos');
    else :
      // datos ed la empresa
      $datos['empresa'] = $this->Empresa_model->datos_empresa_row();

      // busco los datos para el encabezado
      $datos["idfact"] = $this->input->post('idfact');
      //datos edl encabezado y cuerpo del recibo
      $datos['encabe'] = $this->Ventas_model->get_encabezado_ticket($datos["idfact"]);
      $datos['cuerpo'] = $this->Ventas_model->get_cuerpo_ticket($datos['encabe']->nrofact);

      $this->load->view('reportes/rpt_print_ticket_pdf', $datos);
    endif;
  }

  //con esta función insertaremos los comentarios
  public function insertar_cliente() {

    if(!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin())) {redirect('auth/login');}

    // print("<pre>POST:".print_r($_POST,true)."</pre>");
    $this->form_validation->set_rules('ccedula', 'Cedula', 'required|trim|xss_clean');
    $this->form_validation->set_rules('cliente', 'Cliente', 'required|trim|xss_clean');
    $this->form_validation->set_rules('direccion', 'Direccion', 'required|trim|xss_clean');
    $this->form_validation->set_rules('ecorreo', 'Ecorreo', 'required|valid_email|trim|xss_clean');
    $this->form_validation->set_rules('tlfnocel', 'Tlfnocel', 'required|trim|xss_clean');
    $this->form_validation->set_rules('tlfnocasa', 'Tlfnocasa', 'required|trim|xss_clean');

    $this->form_validation->set_message('required', '%s no tiene caracteres validos');

    if ($this->form_validation->run() == TRUE) :

      /* Uso de transacciones->CODEIGNITER para ejecutar varios inserts a la BD's
      de tal manera que si falla alguno de ellos entonces se regresan todas las inserciones hechas */
      $this->db->trans_start();

        //si se ha pulsado el botón submit validamos el formulario con codeIgniter
        $nace = $this->input->post('nac');
        $cede = str_replace(array(',','.'), '', $this->input->post('ccedula'));
        $apes = $this->input->post('cliente');
        $emae = $this->input->post('ecorreo');
        $tlce = $this->input->post('tlfnocel');
        $tlae = $this->input->post('tlfnocasa');
        $dire = $this->input->post('direccion');

        //conseguimos la hora de nuestro país,
        $timezone = "America/Caracas";
        date_default_timezone_set($timezone);
        $timestamp = new DateTime("now",new DateTimeZone($timezone));
        $fec = $timestamp->format('Y-m-d');
        $ope = $this->session->userdata('user_id');

        // inserta los datos arreglados en la tabla de estudiante
        $insert_e  = $this->Ventas_model->insertar_cliente($nace,$cede,$apes,$emae,$tlce,$tlae,$dire,$fec,$ope);

      $this->db->trans_complete();

      // si la transaccion no sale bien, codeigniter hace lo propio: revierte las inserciones hechas
      if ($this->db->trans_status() === FALSE) :

        $this->session->set_flashdata('message', 'No se puede Grabar el dato..!');
        $this->session->set_flashdata('tipomsg','error');
        //el array con los datos a paginar ya preparados
        $datos["resultados"] = $this->Ventas_model->total_posts_clientes_paginados($buscador='', $por_pagina=30, $segmento=0);
        // cargamos la vista principal
        $datos["vienede"] = $this->input->post('vienede');
        // Redirecciono
        $this->load->view('modulopos/punto/clientepos/clientes_encontrados',$datos);
      else :

        $this->session->set_flashdata('message', 'El registro se hizo correctamente!');
        $this->session->set_flashdata('tipomsg','success');
        //el array con los datos a paginar ya preparados
        $datos["resultados"] = $this->Ventas_model->total_posts_clientes_paginados($buscador='', $por_pagina=30, $segmento=0);
        // cargamos la vista principal
        $datos["vienede"] = $this->input->post('vienede');
        // Redirecciono
        $this->load->view('modulopos/punto/clientepos/clientes_encontrados',$datos);
      endif;
    else :
      //redirect them back to the Clientes page
      $this->session->set_flashdata('message', 'Existen errores que PROHIBEN registrar estos datos');
      $this->session->set_flashdata('tipomsg', 'error');
      //el array con los datos a paginar ya preparados
      $datos["resultados"] = $this->Ventas_model->total_posts_clientes_paginados($buscador='', $por_pagina=30, $segmento=0);
      // cargamos la vista principal
      $datos["vienede"] = $this->input->post('vienede');
      // Redirecciono
      $this->load->view('modulopos/punto/clientepos/clientes_encontrados',$datos);
    endif;
  }

  // insertar el registro de todo lo que se envias desde el PUNTO DE VENTA
  public function insertar_registro() {
    if( ! $this->ion_auth->logged_in() || ! $this->ion_auth->is_admin() ) {redirect('auth/login');}
    /* Por el redireccionamiento hecho de insertar_registro() a modulo/ticket_pos()
     * que se hizo pasando via flashsessions el valor del id de la factura para que se 
     * busque en esta ultima funcion los datos de la factura a imprimir, debo resetear
     * lo guardado en la flashsession id_factura  */
    $this->session->set_flashdata('id_factura','');

    //print("<pre>".print_r($_POST,true)."</pre>");
    //print("<pre>".print_r($this->cart->contents(),true)."</pre>");

    // Posteos
    $pos_sess = $this->input->post('pos_sess');
    $dia_sess = $this->input->post('dia_sess');
    $hrs_sess = $this->input->post('hrs_sess');
    //
    $nrovaucher = $this->input->post('nrovaucher');
    $entregado  = convierte_num_gringo($this->input->post('entregado'));
    $formpagoval= $this->input->post('formpagoval');
    //
    $totfact = convierte_num_gringo($this->input->post('totfact'));
    $subiva  = convierte_num_gringo($this->input->post('subiva'));
    $subtot  = convierte_num_gringo($this->input->post('subtot'));
    $totusd  = convierte_num_gringo($this->input->post('totusd'));
    $tasaDolar = convierte_num_gringo($this->input->post('tasaDolar'));
    $idcli   = $this->input->post('idcli');
    $apenom  = $this->input->post('apenom');
    // envio los datos al Modelo para los respectivos inserts
    $id_factura = $this->Ventas_model->insertar_factura($pos_sess,$dia_sess,$hrs_sess,$nrovaucher,$entregado,$formpagoval,$totfact,$subiva,$subtot,$totusd,$tasaDolar,$idcli);

    //echo 'resultado: '.$print_ticket;
    if ($id_factura == 'error') :

      $error = 'MSG: '.$this->db->_error_message().'<br>Error#: '.$this->db->_error_number().'<br>Query: '.$this->db->last_query();

      $this->session->set_flashdata('message', $error);
      $this->session->set_flashdata('tipomsg','error');
      redirect('ventas/pos');
    else :
      // envio el numero de la factura a la siguiente funcion para que imprima el ticket
      $this->session->set_flashdata('id_factura',$id_factura);
      // redirijo para que imprima el ticket
      redirect('ventas/ticket_pos');
    endif;
  }

  // mandando a imprimir la factura para q en esta pagina pueda recargar si asi se necesita
  public function ticket_pos() {
    $id_factura = $this->session->flashdata('id_factura');
    if (! empty($id_factura) ) :
      // le aviso al usuario que todo salió bien..!
      $this->session->set_flashdata('message', 'Todo Ok.!');
      $this->session->set_flashdata('tipomsg','success');
      // ubico el numero de factura a imprimir
      $datos['id_factura'] = $id_factura;
      // ubico el encabezado del ticket
      $datos['efact'] = $this->Modulo_model->get_encabezado_ticket($id_factura);
      // Redireccion
      $this->load->view('modulopos/poslayout/headpos');
      $this->load->view('modulopos/punto/preproceso/ticket_pos', $datos);
      $this->load->view('modulopos/poslayout/footpos');
    else :
      $this->session->set_flashdata('message', 'No se puede recargar la pagina del Ticket. Realiza una nueva factura');
      $this->session->set_flashdata('tipomsg','error');
      redirect('ventas/pos');
    endif;
  }

  // para cerrar sesiones que queden abiertas o segun decision del usuario
  public function cerrar_session() {
    //print("<pre>data: ".print_r($_POST,true)."</pre>");
    $idsess = $this->input->post("idsess");
    $nroses = $this->input->post("nroses");
    $diases = $this->input->post("diases");
    $iduser = $this->input->post("iduser");

    $cerrar = $this->Ventas_model->cerrar_sesion_pos($idsess,$nroses,$diases,$iduser);

    //echo 'res: '.$cerrar;
    if ($cerrar == 'error') :

      $error = 'MSG: '.$this->db->_error_message().'<br>Error#: '.$this->db->_error_number().'<br>Query: '.$this->db->last_query();

      $this->session->set_flashdata('message', $error);
      $this->session->set_flashdata('tipomsg','error');
      redirect('ventas/index');
    elseif ($cerrar == 'error_update') :

      $error = 'MSG: '.$this->db->_error_message().'<br>Error#: '.$this->db->_error_number().'<br>Query: '.$this->db->last_query();

      $this->session->set_flashdata('message', $error);
      $this->session->set_flashdata('tipomsg','error');
      redirect('ventas/index');
    elseif ($cerrar == 'error_cerrada') :

      $this->session->set_flashdata('message', 'Ya ha sido cerrada anteriormente.! Verifique');
      $this->session->set_flashdata('tipomsg','error');
      redirect('ventas/index');
    else :

      // le aviso al usuario que todo salió bien..!
      $this->session->set_flashdata('message', 'Sesion cerrada Ok.!');
      $this->session->set_flashdata('tipomsg','success');
      // Redireccion
      redirect('ventas/index');
    endif;
  }

  // para reimprimir la factura
  public function reimprime_factura() {
    // ubico el numero de factura a imprimir
    $idfgen = $this->input->post('idfgen');
    $datos['id_factura'] = $idfgen;
    // Redireccion
    $this->load->view('modulopos/poslayout/headpos');
    $this->load->view('modulopos/punto/preproceso/ticket_pos', $datos);
    $this->load->view('modulopos/poslayout/footpos');
  }


  ///////////////////////////////////////////////////////////////////////////////
  /// SECCION PARA LA CONSULTA DE LAS SESIONES CERRADAS
  //con esta función validamos y protegemos el buscador
  public function validar_sesion() {

    if( ! $this->ion_auth->logged_in() || ! $this->ion_auth->is_admin() ) {redirect('auth/login');}
    //print("<pre>POST:".print_r($_POST,true)."</pre>");
    // validar el dato que envia el usuario considerando que no haya inyecciones de codigo.
    $this->form_validation->set_rules('buscando', 'buscador', 'trim|xss_clean');
    $this->form_validation->set_message('required', 'El %s no puede ir vacío!');
    if ($this->form_validation->run() == TRUE) {

      $codserv = $this->input->post('buscando');
      $idsess = $this->input->post('idsess');
      $nroses = $this->input->post('nroses');
      $fecini = $this->input->post('fecini');
      $fecfin = $this->input->post('fecfin');
      $iduser = $this->input->post('iduser');

      // Para sacar los totales segun las formas de pago que hubo en la sesion
      $tot_fpag = $this->Ventas_model->get_totales_por_formapago_por_sesion($idsess,$fecini,$fecfin,'TODAS');
      // para mostrar el total de la sesion
      $tot_ses = $this->Ventas_model->get_totales_por_sesion($idsess,$fecini,$fecfin,'TODAS');
      $tot_sesion = $tot_ses->totfact;
      // para mostrar totales a CONTADO de la sesion
      $tot_cred = $this->Ventas_model->get_totales_por_sesion($idsess,$fecini,$fecfin,'CREDITO');
      $tot_cred_sesion = $tot_cred->totfact;
      // para mostrar totales a CONTADO de la sesion
      $tot_cont = $this->Ventas_model->get_totales_por_sesion($idsess,$fecini,$fecfin,'CONTADO');
      $tot_cont_sesion = $tot_cont->totfact;
      // Para mostrar la ganancia de la sesion
      $tot_gananc = $this->Ventas_model->get_ganancias_por_sesion($iduser,$nroses,$fecini,$fecfin);

      $this->session->set_userdata('buscando', $codserv);
      $this->session->set_userdata('idsess', $idsess);
      $this->session->set_userdata('nroses', $nroses);
      $this->session->set_userdata('fecini', $fecini);
      $this->session->set_userdata('fecfin', $fecfin);
      $this->session->set_userdata('iduser', $iduser);
      $this->session->set_userdata('tot_formpa', $tot_fpag);
      $this->session->set_userdata('tot_sesion', $tot_sesion);
      $this->session->set_userdata('tot_gananc', $tot_gananc);
      $this->session->set_userdata('tot_creses', $tot_cred_sesion);
      $this->session->set_userdata('tot_conses', $tot_cont_sesion);

      redirect('ventas/sesion');
    } else {

      //cargamos la vista y el array data
      $this->session->unset_userdata('buscando');
      $this->session->unset_userdata('idsess');
      $this->session->unset_userdata('nroses');
      $this->session->unset_userdata('fecini');
      $this->session->unset_userdata('fecfin');
      $this->session->unset_userdata('iduser');
      $this->session->unset_userdata('tot_formpa');
      $this->session->unset_userdata('tot_sesion');
      $this->session->unset_userdata('tot_gananc');
      $this->session->unset_userdata('tot_creses');
      $this->session->unset_userdata('tot_conses');

      redirect('ventas/sesion');
    }
  }

  public function sesion() {

    if( ! $this->ion_auth->logged_in() || ! $this->ion_auth->is_admin() ) {redirect('auth/login');}
    //print("<pre>POST:".print_r($_POST,true)."</pre>");
    // datos de las sesiones activas y cerradas
    $buscador = $this->session->userdata('buscando');
    $idsess = $this->session->userdata('idsess');
    $nroses = $this->session->userdata('nroses');
    $fecini = $this->session->userdata('fecini');
    $fecfin = $this->session->userdata('fecfin');
    $iduser = $this->session->userdata('iduser');

    $segmento = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    //$por_pagina = $this->input->post('limite'); //Número de registros mostrados por páginas
    $por_pagina = 150; //Número de registros mostrados por páginas
    $config['base_url'] = base_url() . 'ventas/index'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
    $config['total_rows'] = $this->Ventas_model->busqueda_sesion($buscador,$iduser,$nroses,$fecini,$fecfin); //calcula el número de filas
    $config['per_page'] = $por_pagina; //Número de registros mostrados por páginas
    $config["uri_segment"] = 3;
    $config['num_links'] = 5; //Número de links mostrados en la paginación
    //$config['use_page_numbers'] = TRUE;
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['prev_link'] = 'Primera';
    $config['prev_tag_open'] = '<li class="page-item"><a class="page-link"';
    $config['prev_tag_close'] = '</a></li>';
    $config['next_link'] = 'Siguiente';
    $config['next_tag_open'] = '<li class="page-item"><a class="page-link"';
    $config['next_tag_close'] = '</a></li>';
    $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" tabindex="-1">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li class="page-item"><a class="page-link"';
    $config['num_tag_close'] = '</a></li>';
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = '</li>';
    $config['first_link'] = 'Inicio';
    $config['last_link'] = 'Ultima';
    // para la paginacion
    $this->pagination->initialize($config);
    //el array con los datos a paginar ya preparados
    $datos["resultados"] = $this->Ventas_model->total_posts_paginados_sesion($idsess,$nroses,$fecini,$fecfin,$iduser,$buscador, $por_pagina, $segmento);
    // envio tambien a la vista para que se de a conocer
    $datos["idsess"] = $idsess;
    $datos["nroses"] = $nroses;
    $datos["fecini"] = $fecini;
    $datos["fecfin"] = $fecfin;
    $datos["iduser"] = $iduser;

    // para mostrar el total de la sesion
    $datos["tot_formpa"] = $this->session->userdata('tot_formpa');
    $datos["tot_sesion"] = $this->session->userdata('tot_sesion');
    $datos["tot_creses"] = $this->session->userdata('tot_creses');
    $datos["tot_conses"] = $this->session->userdata('tot_conses');
    // Para mostrar el total de ganancias en bolivares y en dolares en encab de la tabla
    $datos["gain"]       = $this->Ventas_model->get_ganancias_por_sesion($iduser,$nroses,$fecini,$fecfin);
    $datos["promTasa"]   = $this->Ventas_model->get_promedio_tasa_por_sesion($iduser,$nroses,$fecini,$fecfin);
    // Ubicando datos del responsable de la sesion
    $qope = $this->ion_auth->user($iduser)->row();
    $datos["responsable"] = $qope->first_name;
    // cargamos la vista principal
    $header['titulo']  = 'Ventas';
    $datos['empresa']  = $this->Empresa_model->datos_empresa_row();
    $datos["is_admin"] = ( ! $this->ion_auth->is_admin() ) ? 0 : $this->ion_auth->is_admin();

    $this->load->view('layouts/header',$header);
    $this->load->view('front-end/ventas/tabla_sesion_admin',$datos);
    $this->load->view('layouts/footer');

    // $sections = array(
    //   'config'  => TRUE,
    //   'queries' => TRUE,
    //   'controller_info' => TRUE,
    //   'uri_strig' => TRUE,
    //   'session_data' => TRUE

    //   );
    // $this->output->set_profiler_sections($sections);
    // $this->output->enable_profiler(TRUE);
  }

  // Abrir el modal para editar el status de la orden de servicio
  public function get_consulta_productos_sesion_admin() {
    //print("<pre>POST:".print_r($_POST,true)."</pre>");
    $datos["nrofac"] = $this->input->post('nrofac');
    $datos['empresa']    = $this->Empresa_model->datos_empresa_row();
    $datos["ventaencab"] = $this->Modulo_model->consulta_encab_venta($datos["nrofac"]);
    $datos["ventacuerp"] = $this->Modulo_model->consulta_cuerpo_venta($datos["nrofac"]);

    $this->load->view('front-end/ventas/consulta_productos_sesion_admin', $datos);
  }

  // funcion para calcular cuales formas de pago fueron usadas para pagar la factura
  public function get_formas_pago_by_factura_admin() {
    //print("<pre>POST:".print_r($_POST,true)."</pre>");
    $datos["nrofac"] = $this->input->post('nrofac');
    $datos['empresa']    = $this->Empresa_model->datos_empresa_row();
    $datos["factencab"]  = $this->Ventas_model->consulta_encab_venta($datos["nrofac"]);
    $datos["resultados"] = $this->Ventas_model->get_formas_pago_by_factura($datos["nrofac"]);
    // Redirecciono
    $this->load->view('front-end/ventas/consultar_formas_pagos_admin',$datos);
  }


  ///////////////////////////////////////////////////////////////////////
  // FUNCIONES PARA DEVOLVER PRODUCTOS O DEVOLVER FACTURAS COMPLETAS

  //con esta función abrimos el modal para editar matricula
  public function devolver_prod() {

    if( ! $this->ion_auth->logged_in() || ! $this->ion_auth->is_admin() ) {redirect('main/index');}

    $datos['idfact'] = $this->input->post('idfact',TRUE);
    $datos['desc']   = $this->input->post('desc',TRUE);
    $datos['cant']   = $this->input->post('cant',TRUE);
    $datos['pvpd']   = $this->input->post('pvpd',TRUE);
    $datos['idinv']  = $this->input->post('idinv',TRUE);
    $datos['nfact']  = $this->input->post('nfact',TRUE);
    $datos['tasa']   = $this->input->post('tasa',TRUE);

    $this->load->view('front-end/ventas/devolver_prod', $datos);
  }

  // proceso para devolver el producto en la BD
  public function devolver_producto() {
    if( ! $this->ion_auth->logged_in() || ! $this->ion_auth->is_admin() ) {redirect('main/index');}
    $opc = $this->input->post('optionsRadios');
    if ($opc =='Si') {
      $idfact = $this->input->post('idfact');
      $nfact  = $this->input->post('nfact');
      $idinv  = $this->input->post('idinv');
      $cantp  = $this->input->post('cantp');
      $pvpd   = $this->input->post('pvpd');
      $tasa   = $this->input->post('tasa');
    }

    //echo 'idgme: '.$idg;
    $devuprod  = $this->Ventas_model->devolver_producto($idfact, $nfact, $idinv, $cantp, $pvpd, $tasa);

    if ($devuprod == 'error') {

      $this->session->set_flashdata('message', 'ERROR DEVOLVIENDO PRODUCTO.!');
      $this->session->set_flashdata('tipomsg','error');
      redirect('ventas/sesion');

    } else if ($devuprod == 'correcto') {

      $this->session->set_flashdata('message', 'Se devolvió correctamente el Producto.!');
      $this->session->set_flashdata('tipomsg','success');
      redirect('ventas/sesion');

    }
  }
  // para inactivar un registro de la tabla de facturas
  public function devolver_factura(){

    if( ! $this->ion_auth->logged_in() || ! $this->ion_auth->is_admin() ) {redirect('main/index');}

    $opc = $this->input->post('optionsRadios');
    if ($opc =='Si') {
      $idfa = $this->input->post('idfgen');
      $nten = $this->input->post('notent');
    }

    //echo 'idgme: '.$idg;
    $borra  = $this->Ventas_model->devolver_factura($idfa, $nten);

    if ($borra == 'con_abonos') {

      $this->session->set_flashdata('message', 'NOTA ENTREGA / FACTURA con ABONOS,\nNO SE PUEDE ANULAR..!');
      $this->session->set_flashdata('tipomsg','warning');
      redirect('facturas/index');

    } else if ($borra == 'error') {

      $this->session->set_flashdata('message', 'ERROR ANULANDO LA FACTURA.!');
      $this->session->set_flashdata('tipomsg','error');
      redirect('facturas/index');

    } else if ($borra == 'correcto') {

      $this->session->set_flashdata('message', 'Se borró correctamente la Factura.!');
      $this->session->set_flashdata('tipomsg','success');
      redirect('facturas/index');

    }
  }


  ///////////////////////////////////////////////////////////////////////////////
  /// SECCION PARA LA VENTA POR DESCARGO (Sacar productos del inventario que no
  /// seran vendidos como tal sino descontados a los empleados a precio proveedor)
  // con esta función validamos y protegemos el buscador
  public function validar_descargo() {

    if( ! $this->ion_auth->logged_in() || ! $this->ion_auth->is_admin() ) {redirect('auth/login');}
    //print("<pre>POST:".print_r($_POST,true)."</pre>");
    // validar el dato que envia el usuario considerando que no haya inyecciones de codigo.
    $this->form_validation->set_rules('buscando', 'buscador', 'trim|xss_clean');
    $this->form_validation->set_message('required', 'El %s no puede ir vacío!');
    if ($this->form_validation->run() == TRUE) {

      $codserv = $this->input->post('buscando');
      $iduser  = $this->input->post('iduser');

      $this->session->set_userdata('buscando', $codserv);
      $this->session->set_userdata('iduser', $iduser);
      redirect('ventas/descargos');
    } else {

      //cargamos la vista y el array data
      $this->session->unset_userdata('buscando');
      $this->session->unset_userdata('iduser');
      redirect('ventas/descargos');
    }
  }

  // index de los descargos
  public function descargos() {

    if( ! $this->ion_auth->logged_in() || ! $this->ion_auth->is_admin() ) {redirect('auth/login');}
    //print("<pre>POST:".print_r($_POST,true)."</pre>");
    // datos de las sesiones activas y cerradas
    $buscador = $this->session->userdata('buscando');
    $iduser   = $this->session->userdata('user_id');

    $segmento = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    //$por_pagina = $this->input->post('limite'); //Número de registros mostrados por páginas
    $por_pagina = 25; //Número de registros mostrados por páginas
    $config['base_url'] = base_url() . 'ventas/descargos'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
    $config['total_rows'] = $this->Ventas_model->busqueda_descargo($buscador); //calcula el número de filas
    $config['per_page'] = $por_pagina; //Número de registros mostrados por páginas
    $config["uri_segment"] = 3;
    $config['num_links'] = 5; //Número de links mostrados en la paginación
    //$config['use_page_numbers'] = TRUE;
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['prev_link'] = 'Primera';
    $config['prev_tag_open'] = '<li class="page-item"><a class="page-link"';
    $config['prev_tag_close'] = '</a></li>';
    $config['next_link'] = 'Siguiente';
    $config['next_tag_open'] = '<li class="page-item"><a class="page-link"';
    $config['next_tag_close'] = '</a></li>';
    $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" tabindex="-1">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li class="page-item"><a class="page-link"';
    $config['num_tag_close'] = '</a></li>';
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = '</li>';
    $config['first_link'] = 'Inicio';
    $config['last_link'] = 'Ultima';
    // para la paginacion
    $this->pagination->initialize($config);
    //el array con los datos a paginar ya preparados
    $datos["resultados"] = $this->Ventas_model->total_posts_paginados_descargo($buscador, $por_pagina, $segmento);

    // Ubicando datos del responsable de la sesion
    $datos["iduser"] = $iduser;
    $qope = $this->ion_auth->user($iduser)->row();
    $datos["responsable"] = $qope->first_name;
    // cargamos la vista principal
    $header['titulo']  = 'Descargos';
    $datos['empresa']  = $this->Empresa_model->datos_empresa_row();
    $datos["is_admin"] = ( ! $this->ion_auth->is_admin() ) ? 0 : $this->ion_auth->is_admin();
    // Setear los valores en caso de aplicar la tasa del dolar
    $aplicar_tasa = $datos['empresa']->aplicar_tasa;
    $tasa_dolar = $datos['empresa']->tasa_dolar;
    if ( $aplicar_tasa == 'Si' && $tasa_dolar > 0 ) :
      $datos["tasa_dolar"] = $tasa_dolar;
    elseif ( $aplicar_tasa == 'Si' && $tasa_dolar <= 0 ) :
      $datos["tasa_dolar"] = 1;
    elseif ( $aplicar_tasa == 'No' ) :
      $datos["tasa_dolar"] = 1;
    endif;

    $this->load->view('layouts/header',$header);
    $this->load->view('front-end/ventas/tabla_descargo_admin',$datos);
    $this->load->view('layouts/footer');

    // $sections = array(
    //   'config'  => TRUE,
    //   'queries' => TRUE,
    //   'controller_info' => TRUE,
    //   'uri_strig' => TRUE,
    //   'session_data' => TRUE

    //   );
    // $this->output->set_profiler_sections($sections);
  }

  //con esta función registramos estudiantes
  public function regdescargo() {

    if(!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin())) {redirect('auth/login');}

    // cargamos la vista principal
    $header['titulo']   = 'Descargo';
    $header['empresa']  = $this->Empresa_model->datos_empresa_row();
    $header["is_admin"] = ( ! $this->ion_auth->is_admin() ) ? 0 : $this->ion_auth->is_admin();
    // cargamos la vista principal
    $this->load->view('layouts/header',$header);
    $this->load->view('front-end/ventas/reg_descargo');
    $this->load->view('layouts/footer');
  }

  // listar los articulos creados
  public function listar_articulos_desc() {
    //
    $this->load->view('front-end/ventas/listar_articulos_desc');
  }

  // listar la categoria principal
  public function listar_catego() {
    //print("<pre>".print_r($_POST,true)."</pre>");
    $catego = $this->input->post('term',TRUE);

      $valores = $this->Productos_model->listar_categorias($catego);
      return $valores;
  }

  // listar la subcategoria
  public function listar_subcatego() {
    //print("<pre>".print_r($_POST,true)."</pre>");
    $catego = $this->input->post('term',TRUE);
    if ($catego){
      $valores = $this->Productos_model->listar_subcategorias($catego);
      return $valores;
    }
  }

  // para buscar compras o material en el tabulador
  public function buscarProd(){
    $descrip = $this->input->post('term',TRUE);
    if ($descrip){
      $valores = $this->Ventas_model->consultar_prod($descrip);
      return $valores;
    }
  }

  // para registrar los articulos pero con codeigniter-Cart
  public function agregar_art_cart_desc(){
    //print("<pre>".print_r($_POST,true)."</pre>");
    $idprod = $this->input->post('idprod');
    if($idprod == '') {
      $id = random_int(8000,9000);
    } else {
      $id = $idprod;
    }
    $qty   = str_replace(',', '', $this->input->post('cantp'));
    $price = str_replace(',', '', $this->input->post('price'));
    $name  = $this->input->post('descr');

    $data = array(
      'id'      => $id,
      'qty'     => number_format($qty,2,'.',''),
      'price'   => number_format($price,2,'.',''),
      'name'    => $name,
      'options' => array($this->input->post('opciones'))
    );

    return $this->cart->insert($data);
  }

  //con esta función insertaremos los comentarios
  public function insertar_descargo() {

    if(!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin())) {redirect('auth/login');}

    // print("<pre>".print_r($_POST,true)."</pre>");
    // print("<pre>".print_r($this->cart->contents(),true)."</pre>");
    $this->form_validation->set_rules('ccedula', 'Cedula/RIF', 'trim|xss_clean');
    $this->form_validation->set_rules('fventa', 'Fecha Compra', 'trim|xss_clean');
    $this->form_validation->set_rules('apenombre', 'Nombre Cliente', 'trim|xss_clean');
    $this->form_validation->set_rules('ecorreo', 'Correo Cliente', 'trim|xss_clean');
    $this->form_validation->set_rules('dircli', 'Direccion', 'trim|xss_clean');
    $this->form_validation->set_rules('tlfnocel', 'Tlfnocel', 'trim|xss_clean');
    $this->form_validation->set_rules('tlfnocasa', 'Tlfnocasa', 'trim|xss_clean');
    $this->form_validation->set_rules('tipo_venta', 'Tipo Venta', 'trim|xss_clean');
    $this->form_validation->set_rules('idvend', 'Vendedor', 'trim|xss_clean');
    $this->form_validation->set_rules('condcomercio', 'Cobrar en', 'trim|xss_clean');

    $this->form_validation->set_message('required', '%s no tiene caracteres validos');

    if ($this->form_validation->run() == TRUE) :
      //recibo las variables por post
      $fven  = cambfecha($this->input->post('fventa'));
      $idvnd = $this->input->post('idvend');
      $pcomv = $this->input->post('pctcomisionvend'); //OJO VIENE CON FORMATO GRINGO por el decimal-2-place
      $tvent = $this->input->post('tipo_venta');
      $ccond = $this->input->post('condcomercio');
      $descu = $this->input->post('descuento');
      $nacc  = $this->input->post('nac');
      $cced  = str_replace(array('#','-',',','.'), '', $this->input->post('ccedula'));
      $idcli = $this->input->post('idnrocli');
      $nomc  = $this->input->post('apenombre');
      $emai  = $this->input->post('ecorreo');
      $dirc  = $this->input->post('dircli');
      $tlfc  = $this->input->post('tlfnocel');
      $tlfo  = $this->input->post('tlfnocasa');
      $obsr  = $this->input->post('observacion');

      //conseguimos la hora de nuestro país,
      $timezone = "America/Caracas";
      date_default_timezone_set($timezone);
      $timestamp = new DateTime("now",new DateTimeZone($timezone));
      $fec = $timestamp->format('Y-m-d');
      $ope = $this->session->userdata('user_id');

      // inserta los datos arreglados en la tabla de importadores
      $insertar = $this->Ventas_model->insertar_descargo($idcli,$nacc,$cced,$fven,$nomc,$emai,$dirc,$tlfc,$tlfo,$obsr,$tvent,$idvnd,$pcomv,$ccond,$descu,$fec,$ope);

      //echo 'res_insert: '.$insertar;

      if ($insertar == 'error') :
        // le aiso al usuario que hay un error
        $this->session->set_flashdata('message', 'Ha Ocurrido un error al Insertar.!');
        $this->session->set_flashdata('tipomsg','error');
          redirect('ventas/descargos');
      elseif ($insertar == 'correcto') :
        // Y le aviso al usuario que todo salió bien..!
        $this->session->set_flashdata('message', 'La Inserción se hizo correctamente.!');
        $this->session->set_flashdata('tipomsg','success');
          redirect('ventas/descargos');
      endif;
    else :
      //redirect them back to the Clientes page
      $this->session->set_flashdata('message', 'Existen errores GRAVES que PROHIBEN registrar estos datos');
      $this->session->set_flashdata('tipomsg', 'error');
      redirect('ventas/descargos');
    endif;
  }

  // para borrar producto por producto de la guia
  public function borrar_articulo_desc(){

    // recibo el post
    $idart = $this->input->post('idart');
    $cantidad = $this->input->post('canti');
    $data = array(
      'rowid'   => $idart,
      'qty'     => 0
    );

    $result = $this->cart->update($data);

      if( $result == false) {

        $this->session->set_flashdata('message', 'No se puede borrar este registro..!');
        $this->session->set_flashdata('tipomsg','error');
        redirect('ventas/regdescargo');

      } else {

        $this->session->set_flashdata('message', 'Se borró correctamente el registro.!');
        $this->session->set_flashdata('tipomsg','success');
        redirect('ventas/regdescargo');
      }
  }

  // para cancelar la creacion de la guia
  public function cancelar_venta_desc(){

    // usando la libreria cart de codeigniter solo hay que destruir el carrito
    $this->cart->destroy();
    redirect('ventas/descargos');
  }


}



