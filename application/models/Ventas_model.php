<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ventas_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	//obtenemos el total de filas para hacer la paginación del buscador
	function busqueda($buscador,$fec_sess) {
		$this->db->select('tbsesspos.*,users.first_name,users.last_name,users.company');
		$this->db->join('users', 'users.id = tbsesspos.iduser');
		$this->db->like('users.first_name', $buscador);
		if( $fec_sess != '') {
			$this->db->where('DATE(tbsesspos.tstampini)', cambfecha($fec_sess));
		}
		$this->db->order_by('idses', 'DESC');
		$consulta = $this->db->get('tbsesspos');
		return $consulta->num_rows();
	}

	//obtenemos todos los posts a paginar con la función
	//total_posts_paginados pasando lo que buscamos, la cantidad por página y el segmento
	//como parámetros de la misma
	function total_posts_paginados($buscador, $fec_sess, $por_pagina, $segmento) {
		$this->db->select('tbsesspos.*,users.first_name,users.last_name,users.company');
		$this->db->join('users', 'users.id = tbsesspos.iduser');
		$this->db->like('users.first_name', $buscador);
		if( $fec_sess != '') {
			$this->db->where('DATE(tbsesspos.tstampini)', cambfecha($fec_sess));
		}
		$this->db->order_by('idses', 'DESC');
		$consulta = $this->db->get('tbsesspos', $por_pagina, $segmento);
		// echo $this->db->last_query();
		if ($consulta->num_rows() > 0) {
			foreach ($consulta->result() as $fila) {
				$datos[] = $fila;
			}
			return $datos;
		} else {
			return 'nada';
		}
	}

	// consultar cliente por id
	public function datos_empresa_row() {
		$query = $this->db->get('tbempresa');
		if($query->num_rows() > 0){

			return $query->row();
		} else {

			return array ('empresa' => 'nada');
		}
	}

	// para consultar y devolver la sesion actual del punto de venta
	public function get_sesion_punto_de_venta($nrosess,$diasess,$horasess) {
		// ubicar quien es el usuario que esta activando esta sesion
		$user = $this->session->userdata('user_id');
		// para el timestamp
		$tstampini = $diasess.' '.$horasess;
		// ubicar si existe una sesion igual previamente iniciada
		$this->db->where('nrosesion',$nrosess);
		$this->db->where('DATE(tstampini)',$diasess);
		$this->db->where('iduser',$user);
		$query = $this->db->get('tbsesspos');
		// echo 'cualses:'.$this->db->last_query();
		if ($query->num_rows() > 0) :
			$qsess = $query->num_rows();
			$qrows = $query->row();
    else :
      $qsess = 'nada';
    endif;

		// darle un numero a la sesion
		// echo 'haySess: '.$qsess;
		// echo 'Statses: '.$qrows->status;
		// echo 'Cerrada: '.($qrows->status == 'CERRADA');
		// echo 'Abierta: '.($qrows->status == 'ABIERTA');
		if( $qsess == 'nada' ) :
			/* si no hay ninguna sesion creada con estas
			 * caracterisiticas entonces creo una */
			$nro_actual = $this->Empresa_model->nro_sesion_actual();

			$datases = array (
				'nrosesion' => $nro_actual,
				'iduser'    => $user,
				'tstampini' => $tstampini,
				'status'    => 'ABIERTA'
			);
			$this->db->insert('tbsesspos',$datases);
			// echo '<br>tbses1'.$this->db->last_query();
			// devuelvo el nuevo numero de sesion
			return $nro_actual;
		elseif( $qsess > 0 ) :
			if( ($qrows->status == 'CERRADA') == 1 ) :
				/* si ya hay una sesion igual pero esta CERRADA entonces creo
				 * una sesion nueva pero con el nro de sesion incrementado */
				$nro_nuevo = $this->Empresa_model->nro_nueva_sesion();

				// creo / inserto la nueva sesion
				$datases = array (
					'nrosesion' => $nro_nuevo,
					'iduser'    => $user,
					'tstampini' => $tstampini,
					'status'    => 'ABIERTA'
				);
				$this->db->insert('tbsesspos',$datases);
				// echo 'nronuevo: '.$nro_nuevo;
				// echo '<br>tbses2:'.$this->db->last_query();
	      // retorno el numero de session
				return $nro_nuevo;
			elseif( ($qrows->status == 'ABIERTA') == 1 ) :
				/* solo si ya hay una sesion igual y abierta
				 * envio el mismo numero de sesion */
				return $nrosess;
			endif;
		endif;
	}

	// cerrando la sesion del punto de venta
	public function cerrar_sesion_pos($idsess,$nroses,$diases,$iduser) {
		// ubicar si existe una sesion igual previamente iniciada
		//echo 'Send: '.$nroses.'-'.$diases.'-'.$iduser;
		// ubicar si existe una sesion igual previamente iniciada
		$this->db->where('idses',$idsess);
		$qses  = $this->db->get('tbsesspos')->row();
		// echo $this->db->last_query();
		if ( $qses->status == 'ABIERTA') :

			/* Uso de transacciones->CODEIGNITER para ejecutar varios inserts a la BD's
			de tal manera que si falla alguno de ellos entonces se regresan todas las inserciones hechas */
			$this->db->trans_start();
				/* Si el dia de cierre es mayor al dia que se abrió la sesion
				* entonces hago que el dia de cierre sea igual al dia de que
				* se abrió la sesion y tambien la hora se la registro como si
				* se hubiese cerrado a ultima hora del dia*/
				//$date = date('Y-m-d');
				// seteo la hora para que sea igual a la del sistema
				$timezone = "America/Caracas";
				date_default_timezone_set($timezone);
				$fec = new DateTime("now",new DateTimeZone($timezone));
				$date = $fec->format('Y-m-d');
				//
				$d1 = strtotime($diases);
				$d2 = strtotime($date);
				// Comparo si el dia a cerrar es el dia anterior al actual
				if ( $d1 < $d2 ) :
					$horases = date('23:59:59');
					// para el timestamp
					$tstampfin = $diases.' '.$horases;
					/* y si el dia a cerrar es el dia anterior al actual
					 * actualizo el numero de sesion en la tbempresa  */
					$dataemp = array (
						'nrosess' => '1'
					);
					$this->db->where('idemp','1');
					$this->db->update('tbempresa',$dataemp);
				else :
					/* verifico la hora debido a que si se graba 00:00:00 Mysql
					* compara la fecha en modo string y no lo interpreta como si
					* fueran las 12 de la noche sino la CERO HORAS.*/
					//$hora = date('H:i:s');
					$hora = $fec->format('H:i:s');
					if( $hora == '00:00:00' ) {
						$horases = date('23:59:59');
					} else {
						$horases = $hora;
					}
					// para el timestamp
					$tstampfin = $diases.' '.$horases;
				endif;
				// preparo el array para actualizar
				$datases = array(
					'tstampfin' => $tstampfin,
					'status'    => 'CERRADA'
				);
				// cierro la session
				$this->db->where('idses',$idsess);
				$this->db->update('tbsesspos',$datases);

			$this->db->trans_complete();

			// si la transaccion no sale bien, codeigniter hace lo propio: revierte las inserciones hechas
			if ($this->db->trans_status() === FALSE) {
				// retorno un aviso al usuario
				return 'error_update';
			} else {
				// retorno un aviso al usuario
				return 'correcto';
			}
		else :
			// retorno un aviso al usuario
			return 'error_cerrada';
		endif;
	}

	// funcion para generar un numero nuevo de recibo
	public function get_nuevo_nro_factura() {

		// ubicamos el proximo nro de control
		$nro  = $this->db->get('tbempresa')->row();
		$nrof = $nro->nrofactura;
		// echo $this->db->last_query().'<br>';
		// echo 'numero: '.$nrof.'<br>';
		// print("<pre>data: ".print_r($nrof,true)."</pre>");
		/* le sumo uno al numero de control interno alla en la tbempresa
		 * para q quede listo para el proximo servicio */
		$nro_nuevo  = ($nrof + 1);
		$update_nro = array ('nrofactura' => $nro_nuevo);
		$this->db->where('tbempresa.idemp',"1");
		$this->db->update('tbempresa',$update_nro);
		// formateo el codigo de la nueva orden de servicio
		//$siguiente_nro = sprintf("%06d",$nrof);
		//return $siguiente_nro;

		return $nrof;
	}

	// para buscar 4 productos recien llegados y mostrarlos en la pagina principal
	function imagen_producto_especifico_cod($busqueda) {
		$this->db->distinct('
			tbimgprod.idimg,
			tbimgprod.codprod,
			tbimgprod.imagen');
		$this->db->where('tbimgprod.codprod', $busqueda);
		$this->db->group_by('tbimgprod.codprod');
		$consulta = $this->db->get('tbimgprod', 2);
		if ($consulta->num_rows() > 0) {
			foreach ($consulta->result() as $fila) {
				$datos[] = $fila;
			}
			//return $datos;
			return $consulta;
		} else {
			return 'nada';
		}
	}

	// para sacar los productos con todas las categorias
	public function cuantos_cat_over() {
		$this->db->select('tbproductos.idcategoria, tbcatego.subcategoria, COUNT(*) AS hay');
		$this->db->join('tbcatego','tbcatego.idcat = tbproductos.idcategoria');
		$this->db->group_by('subcategoria');
		$query = $this->db->get('(SELECT * FROM tbproductos WHERE tbproductos.status_prod != "INACTIVO" ) AS tbproductos');
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return 'nada';
		}
	}

	// para buscar los productos para mostrar en la pagina principal y paginarlos
	function get_listar_productos($buscador, $categoria, $limite, $segmento) {
		//print("<pre>posteos: ".print_r($_POST,true)."</pre>");
		$categoria  = str_replace('_', ' ', $categoria);
		$por_pagina = 0;

		$this->db->distinct('
			tbproductos.idprod,
			tbproductos.codprod,
			tbproductos.codbarprod,
			tbproductos.descrpcorta,
			tbproductos.descripcion,
			tbproductos.idcategoria,
			tbproductos.unidad,
			tbcatego.categoria,
			tbcatego.subcategoria,
			tbproductos.precio_proveedor,
			tbproductos.precioMayor,
			tbproductos.precioDetal,
			tbproductos.existencia,
			tbproductos.status_prod,
			tbimgprod.imagen');
		$this->db->join('tbcatego','tbproductos.idcategoria = tbcatego.idcat');
		$this->db->join('tbimgprod','tbproductos.codprod = tbimgprod.codprod');
		if( $categoria != 'TODAS') {
			$this->db->where('tbcatego.subcategoria', $categoria);
		}
		if( $buscador != '') {
			$this->db->group_start();
			$this->db->or_like('tbproductos.nomprod', $buscador);
			$this->db->or_like('tbproductos.descrpcorta', $buscador);
			$this->db->or_like('tbproductos.codprod', $buscador);
			$this->db->group_end();
		}
		$this->db->group_by('tbproductos.codprod');
		$this->db->order_by('RAND()');
		if( $categoria != 'TODAS') {
			//$segmento = $segmento + $limite;
			$query = $this->db->get('(SELECT * FROM tbproductos WHERE tbproductos.status_prod != "INACTIVO" ) AS tbproductos');
		} else {
			$this->db->limit($limite);
			$query = $this->db->get('(SELECT * FROM tbproductos WHERE tbproductos.status_prod != "INACTIVO" ) AS tbproductos', $por_pagina, $segmento);
		}

		//echo $this->db->last_query();
		if ($query->num_rows() > 0) {
			return $query;
		} else {
			return 'nada';
		}
	}

	// para buscar los productos para mostrar en la pagina principal y paginarlos
	function get_listar_productos_mas_vendidos($buscador, $categoria, $limite, $segmento) {
		//print("<pre>posteos: ".print_r($_POST,true)."</pre>");
		$categoria  = str_replace('_', ' ', $categoria);
		$por_pagina = 0;

		$this->db->distinct('
			tbproductos.idprod,
			tbproductos.codprod,
			tbproductos.codbarprod,
			tbproductos.descrpcorta,
			tbproductos.descripcion,
			tbproductos.tipo_prod,
			tbproductos.idcategoria,
			tbproductos.unidad,
			tbcatego.categoria,
			tbcatego.subcategoria,
			tbproductos.precio_proveedor,
			tbproductos.precioMayor,
			tbproductos.precioDetal,
			tbproductos.existencia,
			tbproductos.status_prod,
			tbimgprod.imagen,
			tbfacturas.codprod');
		if( $categoria != 'TODAS') {
			$this->db->join('tbfacturas','tbproductos.codprod = tbfacturas.codprod');
		}
		$this->db->join('tbcatego','tbproductos.idcategoria = tbcatego.idcat');
		$this->db->join('tbimgprod','tbproductos.codprod = tbimgprod.codprod');
		$this->db->where('tbcatego.categoria != "SUBPRODUCTO"');
		if( $categoria != 'TODAS') {
			$this->db->where('tbcatego.subcategoria', $categoria);
		}
		if( $buscador != '') {
			$this->db->group_start();
			$this->db->like('tbproductos.nomprod', $buscador);
			$this->db->or_like('tbproductos.descrpcorta', $buscador);
			$this->db->or_like('tbproductos.codprod', $buscador);
			$this->db->or_like('tbcatego.subcategoria', $buscador);
			$this->db->group_end();
		}

		$this->db->group_by('tbproductos.codprod');
		$this->db->order_by('RAND()');

		if( $categoria != 'TODAS') {
			//$segmento = $segmento + $limite;
			$this->db->having('COUNT(tbfacturas.codprod) > 1');
			$query = $this->db->get('(SELECT * FROM tbproductos WHERE tbproductos.precioDetal != 0 AND tbproductos.status_prod = "ACTIVO") AS tbproductos');
		} else {
			$this->db->limit($limite);
			$query = $this->db->get('(SELECT * FROM tbproductos WHERE tbproductos.precioDetal != 0 AND tbproductos.status_prod = "ACTIVO") AS tbproductos', $por_pagina, $segmento);
		}

		// echo $this->db->last_query();
		if ($query->num_rows() > 0) {
			return $query;
		} else {
			return 'nada';
		}
	}

	// para consultar las formas de pago existentes
	function get_formas_pago() {
		//
		$this->db->order_by('idfpag ASC');
		$query = $this->db->get('(SELECT * FROM tbformapago WHERE formapago != "Efectivo") AS tbformapago');
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return 'nada';
		}
	}

	// Consultar el cliente por el id
	function get_apenom_cliente_by_id($idcli) {
		$this->db->select('apenombre');
		$this->db->where('idnrocli',$idcli);
		$query = $this->db->get('tbclientes');
		$q = $query->num_rows();
		return $q;
	}

	//obtenemos el total de filas para hacer la paginación del buscador
	function busqueda_clientes($buscador) {
		$this->db->like('ccedula', $buscador);
		$this->db->or_like('apenombre', $buscador);
		$consulta = $this->db->get('tbclientes');
		return $consulta->num_rows();
	}

	/* obtenemos todos los posts a paginar con la función total_posts_clientes
	 * paginados pasando lo que buscamos,la cantidad por página y el segmento
	 * como parámetros de la misma */
	function total_posts_clientes_paginados($buscador, $por_pagina, $segmento) {
		$this->db->like('ccedula', $buscador);
		$this->db->or_like('apenombre', $buscador);
		$this->db->order_by('idnrocli', 'DESC');
		$consulta = $this->db->get('tbclientes', $por_pagina, $segmento);
		//echo $this->db->last_query();
		if ($consulta->num_rows() > 0) {
			foreach ($consulta->result() as $fila) {
				$datos[] = $fila;
			}
			return $datos;
		} else {
			return 'nada';
		}
	}

	// para buscar los datos del cliente por cedula
	function consultar_ced_cli($cedcli) {
		$this->db->where('ccedula',$cedcli);
		$query = $this->db->get('tbclientes');
		$q = $query->num_rows();
		return $q;
	}

	// para buscar los datos del cliente por email
	function consultar_email($ecorreo) {
		$this->db->where('ecorreo',$ecorreo);
		$query = $this->db->get('tbclientes');
		$q = $query->num_rows();
		return $q;
	}

	// para consultar la compra
  function consulta_encab_venta($nrofac) {
    $this->db->join('tbclientes', 'tbclientes.idnrocli = tbfactgen.idcli');
    $this->db->where('tbfactgen.nrofact', $nrofac);
    $this->db->order_by('tbfactgen.idfgen', 'DESC');
    $query = $this->db->get('tbfactgen');
    //echo $this->db->last_query();
    if($query->num_rows() > 0){
      $row = $query->row();
      return $row;
    } else {
      return 'nada';
    }
  }

	// para la impresion del encabezado del recibo
	function get_encabezado_ticket($idfgen) {
		$this->db->join('tbclientes','tbfactgen.idcli = tbclientes.idnrocli');
		$this->db->where('idfgen', $idfgen);
		$query = $this->db->get('tbfactgen');
		//echo $this->db->last_query();
		if($query->num_rows() > 0){

			return $query->row();
		} else {

			return array ('empresa' => 'nada');
		}
	}

	// para la impresion del cuerpo del recibo
	function get_cuerpo_ticket($nrofact) {
		//
		$this->db->where('nrofact', $nrofact);
		$query = $this->db->get('tbfacturas');
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return 'nada';
		}
	}

	// Para sacar los totales de ventas por sesion
	// public function get_totales_por_sesion($iduser,$nrosesion,$tstampini,$tstampfin) {
	// 	$this->db->select('SUM(totalfact) AS totfact');
	// 	$this->db->where('operador', $iduser);
	// 	$this->db->where('nrosess', $nrosesion);
	// 	$this->db->where('timestamp <=', $tstampfin);
	// 	$this->db->where('timestamp >=', $tstampini);
	// 	$this->db->order_by('idfgen', 'DESC');
	// 	$consulta = $this->db->get('(SELECT * FROM tbfactgen WHERE tipo_venta != "DESCARGO") AS tbfactgen');
	// 	//echo $this->db->last_query();
	// 	if ($consulta->num_rows() > 0) {
	// 		$datos = $consulta->row();
	// 		return $datos;
	// 	}
	// }

	// para sacar los totales por forma de pago
	public function get_totales_por_formapago_por_sesion($idsess,$tstampini,$tstampfin,$tipventa) {
		$this->db->select('tbfactupagos.fecfpag, tbfactupagos.formapagoval, SUM(tbfactupagos.entregado) AS entregado, tbfactgen.tasaDolar');
		$this->db->join('tbfactgen', 'tbfactgen.nrofact = tbfactupagos.nrofact');
		$this->db->join('tbsesspos', 'tbsesspos.nrosesion = tbfactgen.nrosess');
			$this->db->group_start();
				$this->db->where('tbfactgen.tipo_venta !=', 'DESCARGO');
		  $this->db->group_end();
		$this->db->where('tbfactgen.timestamp <=', $tstampfin);
		$this->db->where('tbfactgen.timestamp >=', $tstampini);
		$this->db->where('tbsesspos.idses', $idsess);
		$this->db->group_by('tbfactupagos.formapagoval');
		$this->db->order_by('tbfactupagos.formapagoval', 'ASC');
		$consulta = $this->db->get('tbfactupagos');
		// echo $this->db->last_query();
		if ($consulta->num_rows() > 0) :
			foreach ($consulta->result() as $fila) :
				$datos[] = $fila;
			endforeach;
			return $datos;
		else :
			return 'nada';
		endif;
	}

	// Para sacar los totales de ventas por sesion
	public function get_totales_por_sesion($idsess,$tstampini,$tstampfin,$tipventa) {
		$this->db->select('SUM(tbfactgen.totalfact) AS totfact');
		$this->db->join('tbfactgen', 'tbfactgen.nrofact = tbfactupagos.nrofact');
		$this->db->join('tbsesspos', 'tbsesspos.nrosesion = tbfactgen.nrosess');
		if( $tipventa == "TODAS") :
			$this->db->where('tbfactgen.tipo_venta !=', 'DESCARGO');
		elseif ( $tipventa != "TODAS") :
			$this->db->group_start();
			  $this->db->where('tbfactgen.tipo_venta !=', 'DESCARGO');
			  $this->db->where('tbfactgen.tipo_venta', $tipventa);
	   	$this->db->group_end();
		endif;
		$this->db->where('tbfactgen.timestamp <=', $tstampfin);
		$this->db->where('tbfactgen.timestamp >=', $tstampini);
		$this->db->where('tbsesspos.idses', $idsess);
		$this->db->order_by('tbfactgen.idfgen', 'DESC');
		$consulta = $this->db->get('tbfactupagos');
		// echo $this->db->last_query();
		if ($consulta->num_rows() > 0) {
			$datos = $consulta->row();
			return $datos;
		}
	}

	// Para sacar TOTALES GENERALES DE VENTAS incluidos CREDITO Y CONTADO de ventas por sesion
	public function get_totfact_por_sesion($iduser,$nrosesion,$tstampini,$tstampfin) {
		$this->db->select('SUM(totalfact) AS totfact');
		$this->db->where('operador', $iduser);
		$this->db->where('nrosess', $nrosesion);
		$this->db->where('timestamp <=', $tstampfin);
		$this->db->where('timestamp >=', $tstampini);
		$this->db->order_by('idfgen', 'DESC');
		$consulta = $this->db->get('(SELECT * FROM tbfactgen WHERE tipo_venta != "DESCARGO") AS tbfactgen');
		//echo $this->db->last_query();
		if ($consulta->num_rows() > 0) {
			$datos = $consulta->row();
			return $datos;
		}
	}

	// Para sacar el promedio de la tasas por ventas por sesion
	public function get_promedio_tasa_por_sesion($iduser,$nrosesion,$tstampini,$tstampfin) {
		$this->db->select('
			tbfacturas.idfact,
			tbfactgen.nrosess,
			tbfactgen.fechafact,
			tbfactgen.tasaDolar');
		$this->db->join('tbfactgen', 'tbfacturas.nrofact = tbfactgen.nrofact');
		$this->db->where('tbfactgen.tipo_venta !=', 'DESCARGO');
		$this->db->where('tbfactgen.nrosess', $nrosesion);
		$this->db->where('tbfactgen.operador', $iduser);
		$this->db->where('tbfactgen.timestamp <=', $tstampfin);
		$this->db->where('tbfactgen.timestamp >=', $tstampini);
		$query = $this->db->get('tbfacturas');
		// echo $this->db->last_query();
		if ($query->num_rows() > 0) {
			$promedioTasa = 0;
			foreach ($query->result() as $value) {
				$promedioTasa = $value->tasaDolar + $promedioTasa;
			}
			$tasaProm = $promedioTasa / $query->num_rows();
			//echo 'GANANCIA: '.$gain;
			return $tasaProm;
		} else {
			return '0.00';
		}
	}

	// Para sacar las ganancias por ventas por sesion
	public function get_ganancias_por_sesion($iduser,$nrosesion,$tstampini,$tstampfin) {
		$this->db->select('
			tbfacturas.idfact,
			tbfactgen.nrosess,
			tbfactgen.fechafact,
			tbfactgen.nrofact,
			tbfacturas.codprod,
			tbfacturas.cantpedido,
			tbfactgen.tasaDolar,
			tbfacturas.precioProv,
			tbfacturas.precioDetal,
      tbfacturas.stdolVenta');
		$this->db->join('tbfactgen', 'tbfacturas.nrofact = tbfactgen.nrofact');
		$this->db->where('tbfactgen.tipo_venta !=', 'DESCARGO');
		$this->db->where('tbfactgen.nrosess', $nrosesion);
		$this->db->where('tbfactgen.operador', $iduser);
		$this->db->where('tbfactgen.timestamp <=', $tstampfin);
		$this->db->where('tbfactgen.timestamp >=', $tstampini);
		$query = $this->db->get('tbfacturas');
		// echo $this->db->last_query();
		if ($query->num_rows() > 0) {
			$gain = 0;
			$ganancia = 0;
			foreach ($query->result() as $value) {
				//echo "cantpedido: ".$value->cantpedido.'<br>';
				//echo "precioProv: ".$value->precioProv.'<br>';
				//echo "pventaProv: ".$value->precioProv * $value->cantpedido.'<br>';
				//echo "ventEnDola: ".$value->stdolVenta.'<br>';
				$pventaProv = $value->precioProv * $value->cantpedido;
				//$ventEnDola = $value->stdolVenta / $value->cantpedido;
				$ventEnDola = $value->precioDetal * $value->cantpedido;
				if( $ventEnDola > $pventaProv ) :
					$ganancia = ($ventEnDola - $pventaProv);
					//echo "ganancia: ".$ganancia.'<br>';
				else :
					$ganancia = ($pventaProv - $ventEnDola);
					//echo "ganancia: ".$ganancia.'<br>';
				endif;
				//echo '<br>';
				$gain = $gain + $ganancia;
			}
			//echo 'GANANCIA: '.$gain;
			return $gain;
		} else {
			return '0.00';
		}
	}

	// Para sacar cuanto dinero hay en caja en una sesion abierta o nueva
	public function get_dolares_en_caja($idses, $tstampini, $operador, $formapago, $status) {
		// $nrosess, $tstampini,
		$this->db->select('
			tbfactupagos.nrofact,
			tbfactupagos.formapagoval,
			SUM(tbfactupagos.entregado) AS entregado,
			tbsesspos.tstampini,
			tbfactgen.timestamp,
			tbsesspos.status');
		$this->db->join('tbfactgen', 'tbfactgen.nrofact = tbfactupagos.nrofact');
		$this->db->join('tbsesspos', 'tbsesspos.nrosesion = tbfactgen.nrosess');
		$this->db->where('DATE(tbfactgen.timestamp) >=', $tstampini);
		$this->db->where('tbsesspos.idses', $idses);
		$this->db->where('tbfactupagos.formapagoval'.$operador, $formapago);
		$query = $this->db->get('tbfactupagos');
		// echo $this->db->last_query();
		if ($query->num_rows() > 0) {
			$datos = $query->row();
			return $datos;
		} else {
			return 0.00;
		}
	}

	// insertar cliente
	public function insertar_cliente($nace,$cede,$apes,$emae,$tlce,$tlae,$dire,$fec,$ope) {
		// preparar el array
		$data = array (
			'nac'       => $nace,
			'ccedula'   => $cede,
			'apenombre' => $apes,
			'dircli'    => $dire,
			'tlfnocasa' => $tlae,
			'tlfnocel'  => $tlce,
			'ecorreo'   => $emae,
			'fecingreso'=> $fec,
			'status'    => 'ACTIVO',
			'fecha'     => $fec,
			'operador'  => $ope,
			'observacion'=> 'N/A'
		);

			// verificamos si se insertaron los registros
		if ( ! $this->db->insert('tbclientes', $data)){

			return FALSE;

		} else {

			return TRUE;
		}
	}

	// insertar los datos de la factura
	public function insertar_factura($pos_sess,$dia_sess,$hrs_sess,$nrovaucher,$entregado,$formpagoval,$totfact,$subiva,$subtot,$totusd,$tasaDolar,$idcli) {
		//echo '<a href="/modulo/pos">Volver</a>';
		//print("<pre>".print_r($_POST,true)."</pre>");
		// ubico el numero que le voy a colocar al recibo
		$nrofact = $this->Ventas_model->get_nuevo_nro_factura();
		// cuantos productos vienen en la factura
		$cantprd = $this->cart->total_items();

		/* Uso de transacciones->CODEIGNITER para ejecutar varios inserts a la BD's
		de tal manera que si falla alguno de ellos entonces se regresan todas las inserciones hechas */
		$this->db->trans_start();
			// para el timestamp
			$timestamp = $dia_sess.' '.$hrs_sess;
			// registrar el encabezado de la factura
			$datafactgen = array (
				'fechafact'       => $dia_sess,
				'nrosess'         => $pos_sess,
				'nrofact'         => $nrofact,
				'idcli'           => $idcli,
				'idvend'          => 0,
				'pctcomisionfact' => 0,
				'cant_prod'       => $cantprd,
				'netofact'        => $subtot,
				'ivafact'         => $subiva,
				'totalfact'       => $totfact,
				'totusd'          => $totusd,
				'tasaDolar'       => $tasaDolar,
				'condcomercio'    => 1,
				'tipo_venta'      => 'CONTADO',
				'descuento'       => 0,
				'statuspago'      => 'PAGADA',
				'statuscomision'  => 'PAGADA',
				'tasaBsPago'      => $tasaDolar,
	      'diaapi'          => $dia_sess,
	      'horapi'          => $hrs_sess,
				'timestamp'       => $timestamp,
				'operador'        => $this->session->userdata('user_id')
			);
			// print("<pre>datafactgen: ".print_r($datafactgen,true)."</pre>");
			$this->db->insert('tbfactgen', $datafactgen);

			/* UBICO EL ID DE ESTA FACTURA PARA TENER LA REFERENCIA AL MOMENTO DE IMPRIMIR TICKET*/
			$this->db->where('tbfactgen.nrofact',$nrofact);
			$qfct = $this->db->get('tbfactgen');
			if($qfct->num_rows() > 0){
				$fact = $qfct->row();
				// id de la factura para mandar a imprimir ticket
				$ultimo_id_fact = $fact->idfgen;
			}
			//echo $this->db->last_query();
			//print("<pre>datafactgen".print_r($datafactgen,true)."</pre>");

			// registrar pagos realizados
			$i = 0;
			foreach ($nrovaucher as $nvau) :
				// insertar los pagos recibidos para esta factura
				$datapagos = array (
					'nrofact'      => $nrofact,
					'nrovaucher'   => $nrovaucher[$i],
					'entregado'    => $entregado[$i],
					'formapagoval' => $formpagoval[$i],
					'fecfpag'      => $dia_sess,
					'operador'     => $this->session->userdata('user_id')
				);
				$this->db->insert('tbfactupagos', $datapagos);
				//echo $this->db->last_query();
				// print("<pre>datapagos".print_r($datapagos,true)."</pre>");
				$i++;
			endforeach;

			// registrar cada producto en la tabla de productos vendidos
			foreach ($this->cart->contents() as $items => $value) :
				$codpr = $value['options'][0]['codpr'];
				$tipop = $value['options'][0]['tipop'];
				$canti = $value['qty'];
				// como por lo general son varios productos guardo uno por uno
				$datafactura = array (
					'nrofact'    => $nrofact,
					'idinventa'  => $value['id'],
					'codprod'    => $codpr,
					'codbarprod' => $value['options'][0]['codpv'],
					'cantpedido' => $value['qty'],
					'precioProv' => $value['options'][0]['pprov'],
					'precioDetal'=> $value['options'][0]['pdeta'],
					'exento_iva' => $value['options'][0]['exiva'],
					'stotlVenta' => $value['subtotal'],
					'stdolVenta' => $value['options'][0]['pdeta'] * $value['qty'],
					'descriprod' => $value['name'],
					'tipo_prod'  => $tipop,
					'almacen'    => 'PRINCIPAL',
					'statusprod' => 'VENDIDO',
					'fecha'      => $dia_sess,
					'operador'   => $this->session->userdata('user_id')
				);
				$this->db->insert('tbfacturas', $datafactura);
				//echo $this->db->last_query();
				// print("<pre>datafact: ".print_r($datafactura,true)."</pre>");
				// ACTUALIZAR LA EXISTENCIA DE CADA PRODUCTO VENDIDO
				// DEBO VERIFICAR SI EL PRODUCTO ES SENCILLO O ES COMPUESTO
				// SI ES SENCILLO LO DESCUENTO DE LA EXISTENCIA COMO SIEMPRE
				if ( $tipop == 'SENCILLO') :
					// print("<pre>tipoP: ".print_r($tipop,true)."</pre>");
					// ejecuto la consulta sql por cada producto
					$sql = "UPDATE tbproductos set existencia = existencia - ? WHERE codprod = ?";
					$this->db->query($sql, array($canti, $codpr));
				elseif ( $tipop == 'COMPUESTO' ) :
					// print("<pre>tipoP: ".print_r($tipop,true)."</pre>");
					/* SI EL PRODUCTO ES COMPUESTO DEBO BUSCAR CUALES SON SUS SUBPRODUCTOS
					 * Y DESCONTARLOS UNO POR UNO SEGUN LA CANTIDAD QUE SE HAYA REGISTRADO
					 * EN EL SUBPRODUCTO */
					// ubico cuales subproductos conforman el producto
					$subProductos = $this->Productos_model->get_subproductos_by_codprod($codpr);
					// recorro el resultado y al recorrerlo, realizo el descargo del inventario
					if ( $subProductos != 'nada' ) :
						 foreach ($subProductos as $row) :
							$codsprod = $row->codsubprod;
							$cantsprd = $row->cantpedsprd * $canti; // multiplico la cantidad que sera vendida del subproducto por la cantidad que pidio el cliente
							// ejecuto la consulta sql por cada subproducto
							$sql = "UPDATE tbproductos set existencia = existencia - ? WHERE codprod = ?";
							$this->db->query($sql, array($cantsprd, $codsprod));
						 endforeach;
					else :
						// si el producto es de tipo COMPUESTO pero no tiene subproductos lo trato como si fuera SENCILLO
						$sql = "UPDATE tbproductos set existencia = existencia - ? WHERE codprod = ?";
						$this->db->query($sql, array($canti, $codpr));
					endif;
				endif;
			endforeach;

		$this->db->trans_complete();

		// // limpio el carrito
		$this->cart->destroy();

		// si la transaccion no sale bien, codeigniter hace lo propio: revierte las inserciones hechas
		if ($this->db->trans_status() === FALSE) {

			return 'error';

		} else {

			return $ultimo_id_fact;

		}
	}


	/////////////////////////////////////////////////////////////////////////
	// FUNCIONES PARA DEVOLVER PRODUCTOS O DEVOLVER LA FACTURA COMPLETA
	// DEVUELVE UN PRODUCTO
	public function devolver_producto($idfact, $nfact, $idinv, $cantp, $pvpd, $tasa) {
		/* Uso de transacciones->CODEIGNITER para ejecutar varios inserts a la BD's
		de tal manera que si falla alguno de ellos entonces se regresan todas las inserciones hechas */
		$this->db->trans_start();

			/* Actualizo la tabla de productos sumandole las
			 * existencias que han sido cargadas en la factura */
      $sql   = "UPDATE tbproductos SET existencia = existencia + ? WHERE idprod = ?;";
      $this->db->query($sql, array($cantp, $idinv));
      echo 'update1: '.$this->db->last_query().'<br>';

      /* Actualizo tbfactgen para que se descuente lo vendido y
       * los montos de las ganancias se actualicen */
      $restarAlNeto = ($pvpd * $cantp) * $tasa;
      $sql_dos = "
      		UPDATE tbfactgen
      		SET cant_prod = cant_prod - ?,
      		    totusd = totusd - ?,
      		    netofact = netofact - ?,
      		    totalfact = totalfact - ivafact - ?
      		WHERE nrofact = ?;";
      $this->db->query($sql_dos, array($cantp, $pvpd, $restarAlNeto, $restarAlNeto, $nfact));
      echo 'update2: '.$this->db->last_query().'<br>';

      /* Luego, aqui procedo a reversar las cantidades para que
			 * el inventario en BD siempre esté consolidado con la existencia
			 */
			// preparo el array para llevar a cero el producto en la factura
			// para que no intervenga en los reportes de ganancia e inventario
			$prod_devuelto = array (
				"precioProv"  => 0.00000000,
				"precioDetal" => 0.00,
				"stotlVenta"  => 0.00000000,
				"stdolVenta"  => 0.00000000,
				"cantpedido"  => 0.0000,
				"statusprod"  => 'DEVUELTO'
			);
			$this->db->where('idfact',$idfact);
			$this->db->update('tbfacturas',$prod_devuelto);
			//print("<pre>arreglo: ".print_r($new_row,true)."</pre>");

		$this->db->trans_complete();

		// si la transaccion no sale bien, codeigniter hace lo propio: revierte las inserciones hechas
		if ($this->db->trans_status() === FALSE) :

			return 'error';

		else :

			return 'correcto';
		endif;
	}


	// borra (mas bien cambia el status) el registro de la factura
	public function devolver_factura($idfa, $nten) {

		/* Uso de transacciones->CODEIGNITER para ejecutar varios inserts a la BD's
		de tal manera que si falla alguno de ellos entonces se regresan todas las inserciones hechas */
		$this->db->trans_start();

			// Primero verifico que no tenga abonos. Si tiene abonos no se puede borrar
			$this->db->where('nronotaentrega',$nten);
			$query = $this->db->get('tbdepoypagos');
			if ($query->num_rows() > 0) :
				/* Si esta pendiente completamente y SI tiene abonos
				 * entonces NO la podemos borrar */
				return 'con_abonos';
			else :
				/* Luego, si la nota de entrega se puede anular entonces procedo
				 * a reversar el inventario para que el inventario en BD siempre
				 * esté consolidado con la existencia
				 */
				/* Entonces, consigo cuales productos y cual existencia
				 * tienen cargada cada uno de ellos en la nota de entrega */
				$this->db->where('tbfacturas.nronotentr', $nten);
				$this->db->order_by('tbfacturas.idfact', 'desc');
				$resp = $this->db->get('tbfacturas');
				if($resp->num_rows() > 0){
					// envio un resultado para que si no existe en la bd
					// entonces se permita al usuario crear en que esta escribiendo
					foreach ($resp->result() as $row){
						$new_row[] =
							array (
								"idinventa"  => $row->idinventa,
								"cantpedido" => number_format($row->cantpedido,2,'.','')
							);
					}
					//print("<pre>arreglo: ".print_r($new_row,true)."</pre>");
					/* Ya que tengo los datos metidos en un array
					 * entonces lo recorro para actualizar la base de datos
					 * sumandole las existencias que han sido cargadas en la nota*/
					foreach($new_row as $item) :
				    // ACTUALIZAR LA EXISTENCIA DE CADA PRODUCTO VENDIDO
				    $idinv = $item["idinventa"];
	          $exist = $item["cantpedido"];
	          $sql   = "UPDATE tbproductos SET existencia = existencia + ? WHERE idprod = ?;";
	          $this->db->query($sql, array($exist, $idinv));
	          echo 'update1: '.$this->db->last_query().'<br>';
					endforeach;
				}

				/* Si esta pendiente completamente y no tiene abonos
				 * entonces si la podemos borrar */
				$data = array (
					'statuspago' 	=> 'ANULADA',
					'statuscomision'=> 'ANULADA',
				);
				$this->db->where('idfgen',$idfa);
				$this->db->update('tbfactgen',$data);

			endif;

		$this->db->trans_complete();

		// si la transaccion no sale bien, codeigniter hace lo propio: revierte las inserciones hechas
		if ($this->db->trans_status() === FALSE) :

			return 'error';

		else :

			return 'correcto';
		endif;
	}

	////////////////////////////////////////////////////////////////////////
	// Para las consultas sobre las sesiones
	//obtenemos el total de filas para hacer la paginación del buscador
	function busqueda_sesion($buscador,$iduser,$nroses,$fecini,$fecfin) {
		$this->db->select('
			tbfactgen.*,
			tbclientes.nac,
			tbclientes.ccedula,
			tbclientes.apenombre,
			users.first_name,
			users.last_name');
		$this->db->join('users', 'users.id = tbfactgen.operador');
		$this->db->join('tbclientes', 'tbclientes.idnrocli = tbfactgen.idcli');
		$this->db->where('tbfactgen.timestamp <=', $fecfin);
		$this->db->where('tbfactgen.timestamp >=', $fecini);
		$this->db->like('tbfactgen.nrofact', $buscador);
		$this->db->order_by('tbfactgen.idfgen', 'DESC');
		$consulta = $this->db->get("( SELECT * FROM tbfactgen WHERE tbfactgen.nrosess = '$nroses' AND tbfactgen.operador = '$iduser' ) AS tbfactgen ");
		// echo $this->db->last_query();
		return $consulta->num_rows();
	}

	//obtenemos todos los posts a paginar con la función
	//total_posts_paginados pasando lo que buscamos, la cantidad por página y el segmento
	//como parámetros de la misma
	function total_posts_paginados_sesion($idsess,$nroses,$fecini,$fecfin,$iduser,$buscador, $por_pagina, $segmento) {
		$this->db->select('
			tbfactgen.*,
			tbclientes.nac,
			tbclientes.ccedula,
			tbclientes.apenombre,
			users.first_name,
			users.last_name');
		$this->db->join('users', 'users.id = tbfactgen.operador');
		$this->db->join('tbclientes', 'tbclientes.idnrocli = tbfactgen.idcli');
		$this->db->where('tbfactgen.timestamp <=', $fecfin);
		$this->db->where('tbfactgen.timestamp >=', $fecini);
		$this->db->like('tbfactgen.nrofact', $buscador);
		$this->db->order_by('tbfactgen.idfgen', 'DESC');
		$consulta = $this->db->get("( SELECT * FROM tbfactgen WHERE tbfactgen.nrosess = '$nroses' AND tbfactgen.operador = '$iduser' ) AS tbfactgen ", $por_pagina, $segmento);
		// echo $this->db->last_query();
		if ($consulta->num_rows() > 0) {
			foreach ($consulta->result() as $fila) {
				$datos[] = $fila;
			}
			return $datos;
		}
	}

	// Funcion para mostrar totales de productos devueltos en la factura solicitada
	function get_total_productos_devueltos_segun_fact_sesion($nrofact) {
		$this->db->select('
			SUM(tbfacturas.stotlVenta) AS stVentaBoliv,
			SUM(tbfacturas.stdolVenta) AS stVentaDolar,
			tbfacturas.statusprod');
		$this->db->where('tbfacturas.nrofact', $nrofact);
		$consulta = $this->db->get("tbfacturas");
		// echo $this->db->last_query();
		if ($consulta->num_rows() > 0) {
			foreach ($consulta->result() as $fila) {
				$datos[] = $fila;
			}
			return $datos;
		}
	}

	//Funcion para mostrar las formas de pago utilizadas para pagar la factura
	function get_formas_pago_by_factura($nrofac) {
		$this->db->where('tbfactupagos.nrofact', $nrofac);
		$this->db->order_by('tbfactupagos.idfpagos', 'DESC');
		$consulta = $this->db->get('tbfactupagos');
		//echo $this->db->last_query();
		if ($consulta->num_rows() > 0) :
			foreach ($consulta->result() as $fila) {
				$datos[] = $fila;
			}
			return $datos;
		else :
			return 'nada';
		endif;
	}

	////////////////////////////////////////////////////////////////////////
	// Para las consultas sobre los descargos
	//obtenemos el total de filas para hacer la paginación del buscador
	function busqueda_descargo($buscador) {
		$this->db->select('tbfactgen.*, tbclientes.nac, tbclientes.apenombre, tbclientes.ccedula');
		$this->db->join('tbclientes', 'tbfactgen.idcli = tbclientes.idnrocli');
		$this->db->like('tbclientes.apenombre', $buscador);
		$this->db->like('tbclientes.ccedula', $buscador);
		$this->db->like('tbfactgen.idfgen', $buscador);
		$this->db->order_by('tbfactgen.idfgen', 'DESC');
		$consulta = $this->db->get('(SELECT * FROM tbfactgen WHERE tipo_venta != "CONTADO") AS tbfactgen');
		return $consulta->num_rows();
	}

	//obtenemos todos los posts a paginar con la función
	//total_posts_paginados pasando lo que buscamos, la cantidad por página y el segmento
	//como parámetros de la misma
	function total_posts_paginados_descargo($buscador, $por_pagina, $segmento) {
		$this->db->select('tbfactgen.*, tbclientes.nac, tbclientes.apenombre, tbclientes.ccedula');
		$this->db->join('tbclientes', 'tbfactgen.idcli = tbclientes.idnrocli');
		$this->db->like('tbclientes.apenombre', $buscador);
		$this->db->like('tbclientes.ccedula', $buscador);
		$this->db->like('tbfactgen.idfgen', $buscador);
		$this->db->order_by('tbfactgen.idfgen', 'DESC');
		$consulta = $this->db->get('(SELECT * FROM tbfactgen WHERE tipo_venta != "CONTADO") AS tbfactgen', $por_pagina, $segmento);
		//echo $this->db->last_query();
		if ($consulta->num_rows() > 0) {
			foreach ($consulta->result() as $fila) {
				$datos[] = $fila;
			}
			return $datos;
		}
	}

	// para buscar los datos de los servicios tabulados
  function consultar_prod($descrip) {
    $this->db->join('tbcatego', 'tbcatego.idcat = tbproductos.idcategoria', 'left');
    $this->db->like('nomprod',$descrip);
    $this->db->or_like('codprod',$descrip);
    $this->db->or_like('codbarprod',$descrip);
    $this->db->or_like('descrpcorta',$descrip);
    $this->db->order_by('nomprod ASC, existencia ASC');
    $query = $this->db->get('(SELECT * FROM tbproductos WHERE tbproductos.precioDetal != 0 AND tbproductos.status_prod = "ACTIVO") AS tbproductos', 10);
    if($query->num_rows() > 0){
      foreach ($query->result() as $row){
        $new_row[] =
          array (
            'value'        => trim($row->nomprod),
            'cantpedido'   => '',
            'precioDetal'  => trim($row->precioDetal),
            'precioMayor'  => trim($row->precioMayor),
            'idcategoria'  => trim($row->idcategoria),
            'categoria'    => trim($row->categoria),
            'subcategoria' => trim($row->subcategoria),
            'idprod'       => trim($row->idprod),
            'codprod'      => trim($row->codprod),
            'tipoprod'     => trim($row->tipo_prod),
            'codbarprod'   => trim($row->codbarprod),
            'precio_prov'  => trim($row->precio_proveedor),
            'existencia'   => trim($row->existencia),
            'exento_iva'   => trim($row->exento_iva),
          );
      }
      $row_set['suggestions'] = $new_row; //build an array
      echo json_encode($row_set); //format the array into json data
    } else {
      // envio un resultado para que si no existe en la bd
      // entonces se permita al usuario crear en que esta escribiendo
      $new_row = array("SIN RESULTADOS");
      $row_set["suggestions"] = $new_row;
      echo json_encode($row_set); //format the array into json data
    }
  }

  // insertar los datos de la factura
  public function insertar_descargo($idcli,$nacc,$cced,$fven,$nomc,$emai,$dirc,$tlfc,$tlfo,$obsr,$tvent,$idvnd,$pcomv,$ccond,$descu,$fec,$ope) {
    //echo '<a href="/modulo/pos">Volver</a>';
    //print("<pre>".print_r($_POST,true)."</pre>");
    // ubico el numero que le voy a colocar a la factura
    $nrofact = $this->Ventas_model->get_nuevo_nro_factura();
    // cuantos productos vienen en la factura
    $cantprd = $this->cart->total_items();
    // UBicando el iva
    $empresa = $this->Empresa_model->datos_empresa_row();
    if ( $empresa->aplicar_iva == 'Si') :
      $iva = ( $empresa->iva / 100 );
    elseif ( $empresa->aplicar_iva == 'No') :
      $iva = 0;
    endif;
    // Ubicar la tasa dolar
    if ( $empresa->aplicar_tasa == 'Si') :
      $tasaDolar = $empresa->tasa_dolar;
    elseif ( $empresa->aplicar_tasa == 'No') :
      $tasaDolar = 1;
    endif;

    // SI EL CLIENTE ES NUEVO UBICO EL VALOR DE ID PARA AGREGARLO A LA TABLA CORRESPONDIENTE
    if ( $idcli == '') :
      /////////////////////////////////////////////////////////////////
      // consulto si el proveedor esta en la tbproveedores y si no esta lo grabo
      $this->db->select('ccedula');
      $this->db->where('ccedula',$cced);
      $qcli = $this->db->get('tbclientes');
      $cuantos = $qcli->num_rows();
      if ($cuantos <= 0) :
        $dataCli = array (
          'nac'        => $nacc,
          'ccedula'    => $cced,
          'apenombre'  => $nomc,
          'dircli'     => $dirc,
          'tlfnocasa'  => $tlfo,
          'tlfnocel'   => $tlfc,
          'ecorreo'    => $emai,
          'fecingreso' => $fec,
          'status'     => 'ACTIVO',
          'fecha'      => $fec,
          'observacion'=> $obsr,
          'operador'   => $ope
        );
        $this->db->insert('tbclientes',$dataCli);
        //echo 'Prove'.$this->db->last_query();
      endif;

      /* AHORA UBICO EL ID DEL NUEVO PROVEEDOR PARA QUE SE GUARDE
      * EN LA TABLA TBCOMPRGEN*/
      $this->db->where('tbclientes.ccedula',$cced);
      $qcli = $this->db->get('tbclientes');
      if($qcli->num_rows() > 0) :
        $clie  = $qcli->row();
        $idcli = $clie->idnrocli;// AQUI LE DECLARO UN VALOR REAL A LA VARIABLE $idcli QUE VIENE VACIA
      endif;

    endif;

    /* Uso de transacciones->CODEIGNITER para ejecutar varios inserts a la BD's
    de tal manera que si falla alguno de ellos entonces se regresan todas las inserciones hechas */
    $this->db->trans_start();

      // Neto de la compra
      $net_fac = $this->cart->total();
      $tot_iva = $this->cart->total() * $iva;
      $tot_fac = $net_fac + $tot_iva;
      $netofac = $net_fac * $tasaDolar;
      $totalfc = $tot_fac * $tasaDolar;
      // para el timestamp
      $timezone = "America/Caracas";
    	date_default_timezone_set($timezone);
    	$timestamp = new DateTime("now",new DateTimeZone($timezone));
      // Colocar el status del pago segun el tipo de venta
      $stpag = ($tvent == 'CONTADO') ? 'PAGADA' : 'PENDIENTE';
      // Colocar el status del pago de comision segun el tipo de venta
      $stcom = ($tvent == 'DESCARGO') ? 'N/A' : 'PENDIENTE';
      // registrar el encabezado de la factura
      $datafactgen = array (
        'fechafact'       => $fven,
        'nrosess'         => 1,
        'nrofact'         => $nrofact,
        'idcli'           => $idcli,
        'idvend'          => 0,
        'pctcomisionfact' => 0,
        'cant_prod'       => $cantprd,
        'netofact'        => $netofac,
        'ivafact'         => $tot_iva,
        'totalfact'       => $totalfc,
        'totusd'          => $net_fac,
        'tasaDolar'       => $tasaDolar,
        'condcomercio'    => 1,
        'tipo_venta'      => $tvent,
        'descuento'       => 0,
        'statuspago'      => 'PAGADA',
        'statuscomision'  => 'PAGADA',
				'tasaBsPago'      => $tasaDolar,
	      'diaapi'          => $timestamp->format('Y-m-d'),
	      'horapi'          => $timestamp->format('H:i:s'),
				'timestamp'       => $timestamp->format('Y-m-d H:i:s'),
				'operador'        => $this->session->userdata('user_id')
      );
      $this->db->insert('tbfactgen', $datafactgen);

      // /* UBICO EL ID DE ESTA FACTURA PARA TENER LA REFERENCIA AL MOMENTO DE IMPRIMIR TICKET*/
      $this->db->where('tbfactgen.nrofact',$nrofact);
      $qfct = $this->db->get('tbfactgen');
      if($qfct->num_rows() > 0){
        $fact = $qfct->row();
        // id de la factura para madar a imprimir ticket
        $ultimo_id_fact = $fact->idfgen;
      }
      //echo $this->db->last_query();
      //print("<pre>datafactgen".print_r($datafactgen,true)."</pre>");
      ///////////////////////////////////////////////////////////////////////////
      ///
      ///
      ///  OJO: ESTE TIPO DE VENTA NO GENERA GANANCIAS NI SE LE AGREGA FORMA DE
      ///  PAGO ALGUNA. SOLO SIRVE PARA DESCARGAR PRODUCTOS DEL INVENTARIO
      ///
      ///
      ////////////////////////////////////////////////////////////////////////////

      // registrar cada producto en la tabla de productos vendidos
      foreach ($this->cart->contents() as $items => $value) :
        $codpr = $value['options'][0]['codpr'];
        $tipop = $value['options'][0]['tipop'];
        $canti = $value['qty'];
        $stotlventa = $value['subtotal'] * $tasaDolar;
        // como por lo general son varios productos guardo uno por uno
        $datafactura = array (
          'nrofact'    => $nrofact,
          'idinventa'  => $value['id'],
          'codprod'    => $codpr,
          'codbarprod' => $value['options'][0]['codpv'],
          'cantpedido' => $value['qty'],
          'precioProv' => $value['options'][0]['pprov'],
          'precioDetal'=> $value['price'],
          'stotlVenta' => $stotlventa,
          'stdolVenta' => $value['subtotal'],
          'descriprod' => $value['name'],
          'tipo_prod'  => $tipop,
          'exento_iva' => $value['options'][0]['exiva'],
          'almacen'    => 'PRINCIPAL',
          'statusprod' => 'VENDIDO',
          'fecha'      => $timestamp->format('Y-m-d'),
          'operador'   => $this->session->userdata('user_id')
        );
        $this->db->insert('tbfacturas', $datafactura);
        // echo $this->db->last_query();
        //print("<pre>datafact: ".print_r($datafactura,true)."</pre>");
        // ACTUALIZAR LA EXISTENCIA DE CADA PRODUCTO VENDIDO
				// DEBO VERIFICAR SI EL PRODUCTO ES SENCILLO O ES COMPUESTO
				// SI ES SENCILLO LO DESCUENTO DE LA EXISTENCIA COMO SIEMPRE
				if ( $tipop == 'SENCILLO') :
					// ejecuto la consulta sql por cada producto
					$sql = "UPDATE tbproductos set existencia = existencia - ? WHERE codprod = ?";
					$this->db->query($sql, array($canti, $codpr));
				elseif ( $tipop == 'COMPUESTO' ) :
					/* SI EL PRODUCTO ES COMPUESTO DEBO BUSCAR CUALES SON SUS SUBPRODUCTOS
					 * Y DESCONTARLOS UNO POR UNO SEGUN LA CANTIDAD QUE SE HAYA REGISTRADO
					 * EN EL SUBPRODUCTO */
					// ubico cuales subproductos conforman el producto
					$subProductos = $this->Productos_model->get_subproductos_by_codprod($codpr);
					// recorro el resultado y al recorrerlo, realizo el descargo del inventario
						if ( $subProductos != 'nada' ) :
					 		foreach ($subProductos as $row) :
								$codsprod = $row->codsubprod;
								$cantsprd = $row->cantpedsprd * $canti; // multiplico la cantidad que sera vendida del subproducto por la cantidad que pidio el cliente
								// ejecuto la consulta sql por cada subproducto
								$sql = "UPDATE tbproductos set existencia = existencia - ? WHERE codprod = ?";
								$this->db->query($sql, array($cantsprd, $codsprod));
							 endforeach;
						else :
							// si el producto es de tipo COMPUESTO pero no tiene subproductos lo trato como si fuera SENCILLO
							$sql = "UPDATE tbproductos set existencia = existencia - ? WHERE codprod = ?";
							$this->db->query($sql, array($canti, $codpr));
						endif;
				endif;
      endforeach;

    $this->db->trans_complete();

    //limpio el carrito
    $this->cart->destroy();

    // si la transaccion no sale bien, codeigniter hace lo propio: revierte las inserciones hechas
    if ($this->db->trans_status() === FALSE) {

      return 'error';

    } else {

      return 'correcto';

    }
  }


}