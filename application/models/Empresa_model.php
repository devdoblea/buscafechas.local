<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empresa_model extends CI_Model {

  /* Nombres de ls grupos de usuario para
   * validar el tipo de usuario que esta logeado actualmente */
  protected $group1; // grupo de usuario
  protected $group2; // grupo de usuario
  protected $usrid; // id del usuario
  protected $fecha_hoy;
  protected $mes_hoy;
  protected $hoyDia;

  public function __construct() {
    parent::__construct();
    // para redirijir o validar el usuario logeado
    $this->group1    = array('admin','encargado');
    $this->group2    = array('cajeros');
    // id del usuario
    $this->usrid     = $this->session->userdata('user_id');
    // Setear la fecha y la hora para que sea la del SO y no del Server
    // $this->fecha_hoy = ucfirst(setear_fecha(date('Y-m-d'), "a, d"));
    // $this->mes_hoy   = ucfirst(setear_fecha(date('Y-m-d'), "%B de %Y"));
    // Setear la fecha y la hora para que sea la del SO y no del Server
    $this->fecha_hoy = ucfirst(setear_fecha(date('Y-m-d'), "l, d"));
    $this->mes_hoy   = ucfirst(setear_fecha(date('Y-m-d'), ' \d\e F \d\e Y'));
    $this->hoyDia    = fecha_formateada_segun_timezone('America/Caracas', 'Y-m-d');
  }

  function datos() {
    $consulta = $this->db->get('tbempresa');
    if ($consulta->num_rows() > 0) {
      foreach ($consulta->result() as $fila) {
      $datos[] = $fila;
    }
      return $datos;

    } else {

      return 'nada';
    }
  }

  // consultar cliente por id
  function datos_empresa_row() {
    $query = $this->db->get('tbempresa');
    if($query->num_rows() > 0){

      return $query->row();
    } else {

      return array ('empresa' => 'nada');
    }
  }

  // ubicando el tipo de usuario actualmente logueado
  public function get_grupo_user_by_id_user() {
    $tipoUser  = $this->ion_auth->get_users_groups($this->usrid)->result();
    $tipo_user = ( ! empty($tipoUser) ) ? $tipoUser[0]->name : 'NO LOGUEADO';
    return $tipo_user;
  }

  // consultar datos basicos del usuario segun su tipo
  public function get_datauser_y_tipouser_by_id() {
    // Consulto si el usuario es ADMIN, ENCARGADO O CAJERO
    $user = $this->ion_auth->user($this->usrid)->row();
    $tipo_user = $this->get_grupo_user_by_id_user();
    return array(
      'id_usu'   => $user->id,
      'tipo_usu' => $tipo_user,
      'cedula'   => $user->company,
      'nombre'   => $user->first_name,
      'apellido' => $user->last_name
    );
  }

  // sacar el numero del recibo
  function nro_nropresup() {
    $this->db->select('nropresup');
    $consulta = $this->db->get('tbempresa');

    if ($consulta->num_rows() > 0) :
      foreach ($consulta->result() as $fila) :
        $nrotemp = $fila->nropresup;
      endforeach;

      /* sumo un numero mas al que esta en la base de datos para que no se dupliquen
      los contenidos de las ordenes de servicio cuando hay mas de un usuario cargando*/
      $incremento = array ( 'nropresup' => $nrotemp + 1) ;
      $this->db->where('idemp', 1);
      $this->db->update('tbempresa',$incremento);

      // retorno el dato que se saco de la bd antes de aumentarle uno mas
      return $nrotemp;
    endif;
  }

  // sacar el numero de la sesion actual
  function nro_sesion_actual() {

    $this->db->where('idemp', 1);
    $consulta = $this->db->get('tbempresa');
    if ($consulta->num_rows() > 0) :
      foreach ($consulta->result() as $fila) :
        // ubico el dato actual
        $nro_actual = $fila->nrosess;
      endforeach;
      // retorno el dato actual
      return $nro_actual;
    else :
      return 'nada';
    endif;
  }

  // sacar el numero de una nueva sesion o de la siguiente
  function nro_nueva_sesion() {

    $this->db->where('idemp', 1);
    $consulta = $this->db->get('tbempresa');
    if ($consulta->num_rows() > 0) :
      foreach ($consulta->result() as $fila) :
        // ubico el dato actual
        $nro_actual = $fila->nrosess;
      endforeach;

      /* si ya hay una sesion igual pero esta CERRADA entonces creo
       * una sesion nueva pero con el nro de sesion incrementado */
      $nro_nuevo = ($nro_actual + 1);
      $update_nro = array ( 'nrosess' => $nro_nuevo) ;
      $this->db->where('idemp', 1);
      $this->db->update('tbempresa',$update_nro);

      // retorno el dato que se saco de la bd DESPUES de aumentarle uno mas
      return $nro_nuevo;
    else :
      return 'nada';
    endif;
  }

  // sacar el numero de la factura
  function nro_factura() {
    $this->db->select('nrofactura');
    $consulta = $this->db->get('tbempresa');

    if ($consulta->num_rows() > 0) :
      foreach ($consulta->result() as $fila) :
        $nrotemp = $fila->nrofactura;
      endforeach;

      /* sumo un numero mas al que esta en la base de datos para que no se dupliquen
      los contenidos de las ordenes de servicio cuando hay mas de un usuario cargando*/
      $nro_nuevo  = ($nrotemp + 1);
      $update_nro = array ('nrofactura' => $nro_nuevo);
      $this->db->where('idemp', 1);
      $this->db->update('tbempresa',$update_nro);

      // retorno el dato que se saco de la bd antes de aumentarle uno mas
      return $nrotemp;
    endif;
  }

  // sacar el numero de la compra
  function nro_compra() {
    $this->db->select('nrocompra');
    $consulta = $this->db->get('tbempresa');

    if ($consulta->num_rows() > 0) :
      foreach ($consulta->result() as $fila) :
        $nrotemp = $fila->nrocompra;
      endforeach;

      /* sumo un numero mas al que esta en la base de datos para que no se dupliquen
      los contenidos de las ordenes de servicio cuando hay mas de un usuario cargando*/
      $nro_nuevo  = ($nrotemp + 1);
      $update_nro = array ('nrocompra' => $nro_nuevo);
      $this->db->where('idemp', 1);
      $this->db->update('tbempresa',$update_nro);

      // retorno el dato que se saco de la bd antes de aumentarle uno mas
      return $nrotemp;
    endif;
  }

  // conocer la tasa del dolar
  public function get_tasa_dolar() {
    $this->db->select('tasa_dolar');
    $this->db->where('idemp','1');
    $query = $this->db->get('tbempresa');
    if($query->num_rows() > 0){

      $row = $query->row();
      return $row->tasa_dolar;

    } else {

      return '0.00';
    }
  }

  //actualizar la tasa de cambio en la tabla empresa
  public function actualiza_tasacambio($tasa_dolar){
    $data_emp = array (
      'tasa_dolar' => $tasa_dolar
    );
    // actualizar la tasa en la tabla de la empresa
    $this->db->where('idemp', 1);
    if( ! $this->db->update('tbempresa', $data_emp)) {
      return 'error';
    } else {
      return 'correcto';
    };
  }

  // con este se consulta el modelo segun la marca
  public function ciudad_consult($edo_selec) {
    $this->db->distinct();
    $this->db->select('ciudad');
    $this->db->where('estado',$edo_selec);
    //$this->db->group_by('municipio');
    $this->db->order_by('ciudad','asc');
    $ciudad = $this->db->get('tbestados');
      if($ciudad->num_rows() > 0) {
        return $ciudad->result();
      }
  }

  // con este se consulta el modelo segun la marca
  public function municipio_consult($cit_selec) {
    $this->db->distinct();
    $this->db->select('municipio');
    $this->db->where('ciudad',$cit_selec);
    //$this->db->group_by('municipio');
    $this->db->order_by('municipio','asc');
    $ciudad = $this->db->get('tbestados');
      if($ciudad->num_rows() > 0) {
        return $ciudad->result();
      }
  }

  // con este se consulta el modelo segun la marca
  public function parroquia_consult($mun_selec) {
    $this->db->distinct();
    $this->db->select('parroquia');
    $this->db->where('municipio',$mun_selec);
    //$this->db->group_by('municipio');
    $this->db->order_by('parroquia','asc');
    $mun = $this->db->get('tbestados');
      if($mun->num_rows() > 0)  {
        return $mun->result();
      }
  }


  // para ingresar los datos cargados en el formulario ingresarForm_view
  public function insertar_emp($rife,$empr,$fcre,$dire,$edoe,$cdad,$mune,$prqe,$tlca,$tlpr,$tlcc,$adme,$emai,$nrof,$nroc,$mond,$simb,$ivae,$avia,$pinc,$pven,$tsad,$atas,$fec,$ope) {

    $data = array (
      'rifemp'        => $rife,
      'empresa'       => $empr,
      'fcreacion'     => $fcre,
      'dirempresa'    => $dire,
      'estado'        => $edoe,
      'ciudad'        => $cdad,
      'municipio'     => $mune,
      'parroquia'     => $prqe,
      'tlfnocantv'    => $tlca,
      'tlfnofic'      => $tlpr,
      'tlfnocel'      => $tlcc,
      'administrador' => $adme,
      'email'         => $emai,
      'nrofactura'    => $nrof,
      'nrocompra'     => $nroc,
      'moneda'        => $mond,
      'simbol_moneda' => $simb,
      'iva'           => $ivae,
      'tasa_dolar'    => $tsad,
      'aplicar_iva'   => $avia,
      'aplicar_tasa'  => $atas,
      'pctincremento' => $pinc,
      'gananciaxventa'=> $pven,
      'fecha'         => $fec,
      'operador'      => $ope
    );

    $this->db->where('rifemp',$rife);
    $query = $this->db->get('tbempresa');

    if($query->num_rows() == 0 ) {

      if (!$this->db->insert('tbempresa', $data)) {
        return 'error';
       } else {
        return 'correcto';
      }

     } else {

      $this->db->where('rifemp',$rife);
      if (!$this->db->update('tbempresa',$data)) {
        return 'error';
       } else {
        return 'correcto';
      }
    }
  }

  function get_categoria_id($idcat) {
    $this->db->where('idcat',$idcat);
    $query = $this->db->get('tbcatego');
    if($query->num_rows() > 0){

      return $query->row();
    } else {

      return array ('catego' => 'nada');
    }
  }

  // para buscar los tipos de servicio en el tabulador
  function buscar_categorias($catego) {
    $this->db->distinct();
    $this->db->select('categoria');
    $this->db->like('categoria',$catego);
    $this->db->order_by('categoria');
    $query = $this->db->get('tbcatego');
    if($query->num_rows() > 0){
      foreach ($query->result_array() as $row){
        $new_row['id']   = trim($row['idcat']);
        $new_row['text'] = trim($row['categoria']);
        $row_set[] = $new_row; //build an array
      }
      echo json_encode($row_set); //format the array into json data

    } else {
      // envio un resultado para que si no existe en la bd
      // entonces se permita al usuario crear en que esta escribiendo
      $new_row['id']   = "1";
      $new_row['text'] = "SIN RESULTADOS";
      $row_set[] = $new_row;
      echo json_encode($row_set); //format the array into json data
    }
  }

  // para listar estudiante por pfg
  public function listarCatego() {
    // consulto el id en donde se guardó la ultima nomina para pasarsela a la tabla nomina general
    $this->db->order_by('categoria');
    $qnomi = $this->db->get('tbcatego');
    return $qnomi;
  }

  // para grabar los ejes geopoliticos
  public function agregarCat($cat,$sct) {

    $data = array (
      'categoria' => $cat,
      'subcategoria' => $sct
    );

    // verificamos si se insertaron los registros
    if (!$this->db->insert('tbcatego', $data)){
      return 'error';
    } else {
      return 'correcto';
    }
  }

  public function editarCat($idcat,$catego,$subcat) {

    $data = array (
      'categoria' => $catego,
      'subcategoria' => $subcat
    );

    // verificamos si se insertaron los registros
    $this->db->where('idcat',$idcat);
    if (!$this->db->update('tbcatego', $data)){
      return 'error';
    } else {
      return 'correcto';
    }
  }

  // borra articulo uno por uno de la lista de ejes
  public function borraCat($idcat){

    // verificar que esta categoria no haya sido usado ya
    // en la tabla de productos
    $this->db->where('idcategoria',$idcat);
    $query = $this->db->get('tbproductos');
    if($query->num_rows() >= 1 ) {

      return false;
    } else {
      // borro los registros alla en la prenom general
      $this->db->where('idcat',$idcat);
      // si las cuentas son correctas, seguimos
      if ($this->db->delete('tbcatego')) {
        return true;
      } else {
        return false;
      }
    }
  }

  // para listar los metodos de pago
  public function listarMetodos() {
    // consulto el id en donde se guardó la ultima nomina para pasarsela a la tabla nomina general
    $this->db->order_by('idfpag');
    $qnomi = $this->db->get('tbformapago');
    return $qnomi;
  }

  // para grabar metodos de pago
  public function agregarMtd($fpg,$mpg) {

    $data = array (
      'formapago'  => $fpg,
      'monedapago' => $mpg
    );

    // verificamos si se insertaron los registros
    if (!$this->db->insert('tbformapago', $data)){
      return 'error';
    } else {
      return 'correcto';
    }
  }

  function get_metodos_id($idmtds) {
    $this->db->where('idfpag',$idmtds);
    $query = $this->db->get('tbformapago');
    if($query->num_rows() > 0){

      return $query->row();
    } else {

      return array ('metodo' => 'nada');
    }
  }

  public function editarMtd($idfpag,$fpag,$mpag) {

    $data = array (
      'formapago' => $fpag,
      'monedapago' => $mpag
    );

    // verificamos si se insertaron los registros
    $this->db->where('idfpag',$idfpag);
    if (!$this->db->update('tbformapago', $data)){
      return 'error';
    } else {
      return 'correcto';
    }
  }

  // borra articulo uno por uno de la lista de ejes
  public function borraMtd($idfpag){
    // borro los registros alla en la prenom general
    $this->db->where('idfpag',$idfpag);
    // si las cuentas son correctas, seguimos
    if ($this->db->delete('tbformapago')) {
      return true;
    } else {
      return false;
    }
  }

}



